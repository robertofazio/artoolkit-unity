﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<ContentMode,System.Object>
struct ValueCollection_t3121135483;
// System.Collections.Generic.Dictionary`2<ContentMode,System.Object>
struct Dictionary_2_t123108344;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1809641108.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ContentMode,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m157664498_gshared (ValueCollection_t3121135483 * __this, Dictionary_2_t123108344 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m157664498(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3121135483 *, Dictionary_2_t123108344 *, const MethodInfo*))ValueCollection__ctor_m157664498_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ContentMode,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1851314284_gshared (ValueCollection_t3121135483 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1851314284(__this, ___item0, method) ((  void (*) (ValueCollection_t3121135483 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1851314284_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ContentMode,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3016130531_gshared (ValueCollection_t3121135483 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3016130531(__this, method) ((  void (*) (ValueCollection_t3121135483 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3016130531_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ContentMode,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1644213316_gshared (ValueCollection_t3121135483 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1644213316(__this, ___item0, method) ((  bool (*) (ValueCollection_t3121135483 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1644213316_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ContentMode,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m891289515_gshared (ValueCollection_t3121135483 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m891289515(__this, ___item0, method) ((  bool (*) (ValueCollection_t3121135483 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m891289515_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ContentMode,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3434320585_gshared (ValueCollection_t3121135483 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3434320585(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3121135483 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3434320585_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ContentMode,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m3514481981_gshared (ValueCollection_t3121135483 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m3514481981(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3121135483 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3514481981_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<ContentMode,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3959541796_gshared (ValueCollection_t3121135483 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3959541796(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3121135483 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3959541796_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ContentMode,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m413509039_gshared (ValueCollection_t3121135483 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m413509039(__this, method) ((  bool (*) (ValueCollection_t3121135483 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m413509039_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ContentMode,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4195087353_gshared (ValueCollection_t3121135483 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4195087353(__this, method) ((  bool (*) (ValueCollection_t3121135483 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4195087353_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<ContentMode,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m2900300333_gshared (ValueCollection_t3121135483 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m2900300333(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3121135483 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2900300333_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ContentMode,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m219026987_gshared (ValueCollection_t3121135483 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m219026987(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3121135483 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m219026987_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ContentMode,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1809641108  ValueCollection_GetEnumerator_m1960551728_gshared (ValueCollection_t3121135483 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m1960551728(__this, method) ((  Enumerator_t1809641108  (*) (ValueCollection_t3121135483 *, const MethodInfo*))ValueCollection_GetEnumerator_m1960551728_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<ContentMode,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m1203718169_gshared (ValueCollection_t3121135483 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m1203718169(__this, method) ((  int32_t (*) (ValueCollection_t3121135483 *, const MethodInfo*))ValueCollection_get_Count_m1203718169_gshared)(__this, method)
