﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARMarker
struct ARMarker_t1554260723;
// System.String
struct String_t;
// ARPattern[]
struct ARPatternU5BU5D_t59458152;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"

// System.Void ARMarker::.ctor()
extern "C"  void ARMarker__ctor_m1510980660 (ARMarker_t1554260723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMarker::Awake()
extern "C"  void ARMarker_Awake_m3919975599 (ARMarker_t1554260723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMarker::OnEnable()
extern "C"  void ARMarker_OnEnable_m2896502880 (ARMarker_t1554260723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMarker::OnDisable()
extern "C"  void ARMarker_OnDisable_m3224112171 (ARMarker_t1554260723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARMarker::unpackStreamingAssetToCacheDir(System.String)
extern "C"  bool ARMarker_unpackStreamingAssetToCacheDir_m2370257800 (ARMarker_t1554260723 * __this, String_t* ___basename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMarker::Load()
extern "C"  void ARMarker_Load_m2962895258 (ARMarker_t1554260723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMarker::Update()
extern "C"  void ARMarker_Update_m4009774547 (ARMarker_t1554260723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMarker::Unload()
extern "C"  void ARMarker_Unload_m3142779339 (ARMarker_t1554260723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 ARMarker::get_TransformationMatrix()
extern "C"  Matrix4x4_t2933234003  ARMarker_get_TransformationMatrix_m3960336120 (ARMarker_t1554260723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARMarker::get_Visible()
extern "C"  bool ARMarker_get_Visible_m4014965141 (ARMarker_t1554260723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ARPattern[] ARMarker::get_Patterns()
extern "C"  ARPatternU5BU5D_t59458152* ARMarker_get_Patterns_m100568928 (ARMarker_t1554260723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARMarker::get_Filtered()
extern "C"  bool ARMarker_get_Filtered_m2782988530 (ARMarker_t1554260723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMarker::set_Filtered(System.Boolean)
extern "C"  void ARMarker_set_Filtered_m2334347853 (ARMarker_t1554260723 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ARMarker::get_FilterSampleRate()
extern "C"  float ARMarker_get_FilterSampleRate_m2217235765 (ARMarker_t1554260723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMarker::set_FilterSampleRate(System.Single)
extern "C"  void ARMarker_set_FilterSampleRate_m3330025658 (ARMarker_t1554260723 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ARMarker::get_FilterCutoffFreq()
extern "C"  float ARMarker_get_FilterCutoffFreq_m1734975516 (ARMarker_t1554260723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMarker::set_FilterCutoffFreq(System.Single)
extern "C"  void ARMarker_set_FilterCutoffFreq_m2633862823 (ARMarker_t1554260723 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARMarker::get_UseContPoseEstimation()
extern "C"  bool ARMarker_get_UseContPoseEstimation_m153213980 (ARMarker_t1554260723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMarker::set_UseContPoseEstimation(System.Boolean)
extern "C"  void ARMarker_set_UseContPoseEstimation_m2883712299 (ARMarker_t1554260723 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ARMarker::get_NFTScale()
extern "C"  float ARMarker_get_NFTScale_m2738392489 (ARMarker_t1554260723 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMarker::set_NFTScale(System.Single)
extern "C"  void ARMarker_set_NFTScale_m4109098010 (ARMarker_t1554260723 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARMarker::.cctor()
extern "C"  void ARMarker__cctor_m3893367429 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
