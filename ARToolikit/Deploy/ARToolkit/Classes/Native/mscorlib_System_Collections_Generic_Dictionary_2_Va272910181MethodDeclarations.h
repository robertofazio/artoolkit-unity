﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va933139243MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MarkerType,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m2332411352(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t272910181 *, Dictionary_2_t1569850338 *, const MethodInfo*))ValueCollection__ctor_m3048576706_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MarkerType,System.String>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1888928514(__this, ___item0, method) ((  void (*) (ValueCollection_t272910181 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2539228508_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MarkerType,System.String>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3068249923(__this, method) ((  void (*) (ValueCollection_t272910181 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2201883495_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<MarkerType,System.String>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m768058198(__this, ___item0, method) ((  bool (*) (ValueCollection_t272910181 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3607159516_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<MarkerType,System.String>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m486731835(__this, ___item0, method) ((  bool (*) (ValueCollection_t272910181 *, String_t*, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m956745599_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<MarkerType,System.String>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2316053077(__this, method) ((  Il2CppObject* (*) (ValueCollection_t272910181 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1168036017_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MarkerType,System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m789317797(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t272910181 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m385822401_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<MarkerType,System.String>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3799386630(__this, method) ((  Il2CppObject * (*) (ValueCollection_t272910181 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2955445344_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<MarkerType,System.String>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m749739175(__this, method) ((  bool (*) (ValueCollection_t272910181 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m462705435_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<MarkerType,System.String>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m165394057(__this, method) ((  bool (*) (ValueCollection_t272910181 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2465222925_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<MarkerType,System.String>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m132330033(__this, method) ((  Il2CppObject * (*) (ValueCollection_t272910181 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m4289216821_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MarkerType,System.String>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m2738040507(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t272910181 *, StringU5BU5D_t1642385972*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m276100111_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<MarkerType,System.String>::GetEnumerator()
#define ValueCollection_GetEnumerator_m4209582032(__this, method) ((  Enumerator_t3256383102  (*) (ValueCollection_t272910181 *, const MethodInfo*))ValueCollection_GetEnumerator_m2979033658_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<MarkerType,System.String>::get_Count()
#define ValueCollection_get_Count_m1627335021(__this, method) ((  int32_t (*) (ValueCollection_t272910181 *, const MethodInfo*))ValueCollection_get_Count_m746136433_gshared)(__this, method)
