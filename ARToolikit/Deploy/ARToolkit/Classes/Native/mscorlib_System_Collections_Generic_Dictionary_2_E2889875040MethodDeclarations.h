﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3550104102MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MarkerType,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3041374827(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2889875040 *, Dictionary_2_t1569850338 *, const MethodInfo*))Enumerator__ctor_m2973548479_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<MarkerType,System.String>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2849219898(__this, method) ((  Il2CppObject * (*) (Enumerator_t2889875040 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1191993120_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MarkerType,System.String>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1320507556(__this, method) ((  void (*) (Enumerator_t2889875040 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2975504638_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<MarkerType,System.String>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2362651145(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t2889875040 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4253026541_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<MarkerType,System.String>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3897616924(__this, method) ((  Il2CppObject * (*) (Enumerator_t2889875040 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3652883970_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<MarkerType,System.String>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2677285190(__this, method) ((  Il2CppObject * (*) (Enumerator_t2889875040 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3676760432_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<MarkerType,System.String>::MoveNext()
#define Enumerator_MoveNext_m3131315932(__this, method) ((  bool (*) (Enumerator_t2889875040 *, const MethodInfo*))Enumerator_MoveNext_m1917570470_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<MarkerType,System.String>::get_Current()
#define Enumerator_get_Current_m2438855244(__this, method) ((  KeyValuePair_2_t3622162856  (*) (Enumerator_t2889875040 *, const MethodInfo*))Enumerator_get_Current_m3452829462_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<MarkerType,System.String>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1208219903(__this, method) ((  int32_t (*) (Enumerator_t2889875040 *, const MethodInfo*))Enumerator_get_CurrentKey_m4228999067_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<MarkerType,System.String>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m4178425031(__this, method) ((  String_t* (*) (Enumerator_t2889875040 *, const MethodInfo*))Enumerator_get_CurrentValue_m1599100579_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MarkerType,System.String>::Reset()
#define Enumerator_Reset_m3414549761(__this, method) ((  void (*) (Enumerator_t2889875040 *, const MethodInfo*))Enumerator_Reset_m428455253_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MarkerType,System.String>::VerifyState()
#define Enumerator_VerifyState_m544085908(__this, method) ((  void (*) (Enumerator_t2889875040 *, const MethodInfo*))Enumerator_VerifyState_m1352483306_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MarkerType,System.String>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3101182708(__this, method) ((  void (*) (Enumerator_t2889875040 *, const MethodInfo*))Enumerator_VerifyCurrent_m1035604846_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MarkerType,System.String>::Dispose()
#define Enumerator_Dispose_m3825477039(__this, method) ((  void (*) (Enumerator_t2889875040 *, const MethodInfo*))Enumerator_Dispose_m325224547_gshared)(__this, method)
