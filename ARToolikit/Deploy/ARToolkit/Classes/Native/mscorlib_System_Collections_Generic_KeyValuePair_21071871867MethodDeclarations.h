﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21732100929MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<ARController/ARToolKitThresholdMode,System.String>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1424692786(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1071871867 *, int32_t, String_t*, const MethodInfo*))KeyValuePair_2__ctor_m1132841688_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<ARController/ARToolKitThresholdMode,System.String>::get_Key()
#define KeyValuePair_2_get_Key_m2609692440(__this, method) ((  int32_t (*) (KeyValuePair_2_t1071871867 *, const MethodInfo*))KeyValuePair_2_get_Key_m3936769182_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<ARController/ARToolKitThresholdMode,System.String>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3677860243(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1071871867 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m3873007327_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<ARController/ARToolKitThresholdMode,System.String>::get_Value()
#define KeyValuePair_2_get_Value_m3243017240(__this, method) ((  String_t* (*) (KeyValuePair_2_t1071871867 *, const MethodInfo*))KeyValuePair_2_get_Value_m4217972446_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<ARController/ARToolKitThresholdMode,System.String>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3895768371(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1071871867 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Value_m2643820831_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<ARController/ARToolKitThresholdMode,System.String>::ToString()
#define KeyValuePair_2_ToString_m395766313(__this, method) ((  String_t* (*) (KeyValuePair_2_t1071871867 *, const MethodInfo*))KeyValuePair_2_ToString_m1817796925_gshared)(__this, method)
