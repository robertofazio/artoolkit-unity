﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>
struct Dictionary_2_t3974755707;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En999813113.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21732100929.h"
#include "AssemblyU2DCSharp_ARController_ARToolKitThresholdM2161580787.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ARController/ARToolKitThresholdMode,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m550298620_gshared (Enumerator_t999813113 * __this, Dictionary_2_t3974755707 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m550298620(__this, ___dictionary0, method) ((  void (*) (Enumerator_t999813113 *, Dictionary_2_t3974755707 *, const MethodInfo*))Enumerator__ctor_m550298620_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ARController/ARToolKitThresholdMode,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3448596329_gshared (Enumerator_t999813113 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3448596329(__this, method) ((  Il2CppObject * (*) (Enumerator_t999813113 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3448596329_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ARController/ARToolKitThresholdMode,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m456649017_gshared (Enumerator_t999813113 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m456649017(__this, method) ((  void (*) (Enumerator_t999813113 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m456649017_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<ARController/ARToolKitThresholdMode,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3932033974_gshared (Enumerator_t999813113 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3932033974(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t999813113 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3932033974_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ARController/ARToolKitThresholdMode,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2380732015_gshared (Enumerator_t999813113 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2380732015(__this, method) ((  Il2CppObject * (*) (Enumerator_t999813113 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2380732015_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ARController/ARToolKitThresholdMode,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3673366751_gshared (Enumerator_t999813113 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3673366751(__this, method) ((  Il2CppObject * (*) (Enumerator_t999813113 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3673366751_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<ARController/ARToolKitThresholdMode,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m916534521_gshared (Enumerator_t999813113 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m916534521(__this, method) ((  bool (*) (Enumerator_t999813113 *, const MethodInfo*))Enumerator_MoveNext_m916534521_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<ARController/ARToolKitThresholdMode,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t1732100929  Enumerator_get_Current_m400230469_gshared (Enumerator_t999813113 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m400230469(__this, method) ((  KeyValuePair_2_t1732100929  (*) (Enumerator_t999813113 *, const MethodInfo*))Enumerator_get_Current_m400230469_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<ARController/ARToolKitThresholdMode,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m3586788524_gshared (Enumerator_t999813113 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m3586788524(__this, method) ((  int32_t (*) (Enumerator_t999813113 *, const MethodInfo*))Enumerator_get_CurrentKey_m3586788524_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<ARController/ARToolKitThresholdMode,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m1406815020_gshared (Enumerator_t999813113 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1406815020(__this, method) ((  Il2CppObject * (*) (Enumerator_t999813113 *, const MethodInfo*))Enumerator_get_CurrentValue_m1406815020_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ARController/ARToolKitThresholdMode,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m246264030_gshared (Enumerator_t999813113 * __this, const MethodInfo* method);
#define Enumerator_Reset_m246264030(__this, method) ((  void (*) (Enumerator_t999813113 *, const MethodInfo*))Enumerator_Reset_m246264030_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ARController/ARToolKitThresholdMode,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2906625695_gshared (Enumerator_t999813113 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2906625695(__this, method) ((  void (*) (Enumerator_t999813113 *, const MethodInfo*))Enumerator_VerifyState_m2906625695_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ARController/ARToolKitThresholdMode,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m3943498473_gshared (Enumerator_t999813113 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m3943498473(__this, method) ((  void (*) (Enumerator_t999813113 *, const MethodInfo*))Enumerator_VerifyCurrent_m3943498473_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ARController/ARToolKitThresholdMode,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2489208708_gshared (Enumerator_t999813113 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2489208708(__this, method) ((  void (*) (Enumerator_t999813113 *, const MethodInfo*))Enumerator_Dispose_m2489208708_gshared)(__this, method)
