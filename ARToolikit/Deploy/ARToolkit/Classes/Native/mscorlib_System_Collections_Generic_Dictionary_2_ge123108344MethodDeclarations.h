﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<ContentMode,System.Object>
struct Dictionary_2_t123108344;
// System.Collections.Generic.IEqualityComparer`1<ContentMode>
struct IEqualityComparer_1_t2133546308;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<ContentMode,System.Object>[]
struct KeyValuePair_2U5BU5D_t2061717899;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<ContentMode,System.Object>>
struct IEnumerator_1_t3945911985;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/ValueCollection<ContentMode,System.Object>
struct ValueCollection_t3121135483;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22175420862.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_ContentMode2920913530.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1443133046.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m2554471395_gshared (Dictionary_2_t123108344 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m2554471395(__this, method) ((  void (*) (Dictionary_2_t123108344 *, const MethodInfo*))Dictionary_2__ctor_m2554471395_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2220716248_gshared (Dictionary_2_t123108344 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m2220716248(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t123108344 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2220716248_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m1022021828_gshared (Dictionary_2_t123108344 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m1022021828(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t123108344 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m1022021828_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m2533493614_gshared (Dictionary_2_t123108344 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m2533493614(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t123108344 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m2533493614_gshared)(__this, ___info0, ___context1, method)
// System.Object System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m990041611_gshared (Dictionary_2_t123108344 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m990041611(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t123108344 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m990041611_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m176917962_gshared (Dictionary_2_t123108344 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m176917962(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t123108344 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m176917962_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m206792213_gshared (Dictionary_2_t123108344 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m206792213(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t123108344 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m206792213_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m622443190_gshared (Dictionary_2_t123108344 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m622443190(__this, ___key0, method) ((  void (*) (Dictionary_2_t123108344 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m622443190_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2154278335_gshared (Dictionary_2_t123108344 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2154278335(__this, method) ((  bool (*) (Dictionary_2_t123108344 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2154278335_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2497331199_gshared (Dictionary_2_t123108344 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2497331199(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t123108344 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2497331199_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m4032014453_gshared (Dictionary_2_t123108344 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m4032014453(__this, method) ((  bool (*) (Dictionary_2_t123108344 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m4032014453_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2411488812_gshared (Dictionary_2_t123108344 * __this, KeyValuePair_2_t2175420862  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2411488812(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t123108344 *, KeyValuePair_2_t2175420862 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2411488812_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m876704508_gshared (Dictionary_2_t123108344 * __this, KeyValuePair_2_t2175420862  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m876704508(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t123108344 *, KeyValuePair_2_t2175420862 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m876704508_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2836162048_gshared (Dictionary_2_t123108344 * __this, KeyValuePair_2U5BU5D_t2061717899* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2836162048(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t123108344 *, KeyValuePair_2U5BU5D_t2061717899*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2836162048_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1400452235_gshared (Dictionary_2_t123108344 * __this, KeyValuePair_2_t2175420862  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1400452235(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t123108344 *, KeyValuePair_2_t2175420862 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1400452235_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m2478475919_gshared (Dictionary_2_t123108344 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m2478475919(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t123108344 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m2478475919_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2844149642_gshared (Dictionary_2_t123108344 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2844149642(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t123108344 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2844149642_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3033798861_gshared (Dictionary_2_t123108344 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3033798861(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t123108344 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3033798861_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1632365424_gshared (Dictionary_2_t123108344 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1632365424(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t123108344 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1632365424_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m2513837183_gshared (Dictionary_2_t123108344 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m2513837183(__this, method) ((  int32_t (*) (Dictionary_2_t123108344 *, const MethodInfo*))Dictionary_2_get_Count_m2513837183_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m1465143080_gshared (Dictionary_2_t123108344 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m1465143080(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t123108344 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m1465143080_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m4013333755_gshared (Dictionary_2_t123108344 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m4013333755(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t123108344 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m4013333755_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m1547039867_gshared (Dictionary_2_t123108344 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m1547039867(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t123108344 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m1547039867_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m3448120696_gshared (Dictionary_2_t123108344 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m3448120696(__this, ___size0, method) ((  void (*) (Dictionary_2_t123108344 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m3448120696_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m4083499294_gshared (Dictionary_2_t123108344 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m4083499294(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t123108344 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m4083499294_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t2175420862  Dictionary_2_make_pair_m2308935824_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m2308935824(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t2175420862  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m2308935824_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m4152793230_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m4152793230(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m4152793230_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m3994782063_gshared (Dictionary_2_t123108344 * __this, KeyValuePair_2U5BU5D_t2061717899* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m3994782063(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t123108344 *, KeyValuePair_2U5BU5D_t2061717899*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m3994782063_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m1929134253_gshared (Dictionary_2_t123108344 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m1929134253(__this, method) ((  void (*) (Dictionary_2_t123108344 *, const MethodInfo*))Dictionary_2_Resize_m1929134253_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m4133552248_gshared (Dictionary_2_t123108344 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m4133552248(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t123108344 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m4133552248_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m1364447976_gshared (Dictionary_2_t123108344 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m1364447976(__this, method) ((  void (*) (Dictionary_2_t123108344 *, const MethodInfo*))Dictionary_2_Clear_m1364447976_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m1737825232_gshared (Dictionary_2_t123108344 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m1737825232(__this, ___key0, method) ((  bool (*) (Dictionary_2_t123108344 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m1737825232_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m2521995184_gshared (Dictionary_2_t123108344 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m2521995184(__this, ___value0, method) ((  bool (*) (Dictionary_2_t123108344 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m2521995184_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m3187383843_gshared (Dictionary_2_t123108344 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m3187383843(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t123108344 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m3187383843_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m2283779179_gshared (Dictionary_2_t123108344 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m2283779179(__this, ___sender0, method) ((  void (*) (Dictionary_2_t123108344 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2283779179_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m1839481236_gshared (Dictionary_2_t123108344 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m1839481236(__this, ___key0, method) ((  bool (*) (Dictionary_2_t123108344 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m1839481236_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m495214907_gshared (Dictionary_2_t123108344 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m495214907(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t123108344 *, int32_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m495214907_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::get_Values()
extern "C"  ValueCollection_t3121135483 * Dictionary_2_get_Values_m3982547652_gshared (Dictionary_2_t123108344 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m3982547652(__this, method) ((  ValueCollection_t3121135483 * (*) (Dictionary_2_t123108344 *, const MethodInfo*))Dictionary_2_get_Values_m3982547652_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m3065574325_gshared (Dictionary_2_t123108344 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m3065574325(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t123108344 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m3065574325_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m1598583101_gshared (Dictionary_2_t123108344 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1598583101(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t123108344 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1598583101_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m1318605635_gshared (Dictionary_2_t123108344 * __this, KeyValuePair_2_t2175420862  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m1318605635(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t123108344 *, KeyValuePair_2_t2175420862 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1318605635_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1443133046  Dictionary_2_GetEnumerator_m580523248_gshared (Dictionary_2_t123108344 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m580523248(__this, method) ((  Enumerator_t1443133046  (*) (Dictionary_2_t123108344 *, const MethodInfo*))Dictionary_2_GetEnumerator_m580523248_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<ContentMode,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__0_m404489941_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m404489941(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m404489941_gshared)(__this /* static, unused */, ___key0, ___value1, method)
