﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<ContentMode>
struct DefaultComparer_t247876395;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ContentMode2920913530.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<ContentMode>::.ctor()
extern "C"  void DefaultComparer__ctor_m1739260384_gshared (DefaultComparer_t247876395 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1739260384(__this, method) ((  void (*) (DefaultComparer_t247876395 *, const MethodInfo*))DefaultComparer__ctor_m1739260384_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<ContentMode>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2085761917_gshared (DefaultComparer_t247876395 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2085761917(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t247876395 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m2085761917_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<ContentMode>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3785186709_gshared (DefaultComparer_t247876395 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3785186709(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t247876395 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m3785186709_gshared)(__this, ___x0, ___y1, method)
