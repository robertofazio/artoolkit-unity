﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.WebClient
struct WebClient_t1432723993;
// System.Net.IWebProxy
struct IWebProxy_t3916853445;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Uri
struct Uri_t19570940;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Net.WebRequest
struct WebRequest_t1365124353;
// System.IO.Stream
struct Stream_t3255436806;
// System.Net.DownloadProgressChangedEventArgs
struct DownloadProgressChangedEventArgs_t3269688412;
// System.Net.WebResponse
struct WebResponse_t1895226051;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Uri19570940.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_IO_Stream3255436806.h"
#include "System_System_Net_DownloadProgressChangedEventArgs3269688412.h"
#include "System_System_Net_WebRequest1365124353.h"

// System.Void System.Net.WebClient::.ctor()
extern "C"  void WebClient__ctor_m660733025 (WebClient_t1432723993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::.cctor()
extern "C"  void WebClient__cctor_m196213842 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.IWebProxy System.Net.WebClient::get_Proxy()
extern "C"  Il2CppObject * WebClient_get_Proxy_m903524692 (WebClient_t1432723993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.WebClient::get_IsBusy()
extern "C"  bool WebClient_get_IsBusy_m1483534531 (WebClient_t1432723993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::CheckBusy()
extern "C"  void WebClient_CheckBusy_m3268863214 (WebClient_t1432723993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::SetBusy()
extern "C"  void WebClient_SetBusy_m3197543546 (WebClient_t1432723993 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::DownloadData(System.Uri)
extern "C"  ByteU5BU5D_t3397334013* WebClient_DownloadData_m3099526144 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::DownloadDataCore(System.Uri,System.Object)
extern "C"  ByteU5BU5D_t3397334013* WebClient_DownloadDataCore_m3229215021 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, Il2CppObject * ___userToken1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebClient::DownloadString(System.String)
extern "C"  String_t* WebClient_DownloadString_m1757636243 (WebClient_t1432723993 * __this, String_t* ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri System.Net.WebClient::CreateUri(System.String)
extern "C"  Uri_t19570940 * WebClient_CreateUri_m255501139 (WebClient_t1432723993 * __this, String_t* ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebClient::GetQueryString(System.Boolean)
extern "C"  String_t* WebClient_GetQueryString_m1589491618 (WebClient_t1432723993 * __this, bool ___add_qmark0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri System.Net.WebClient::MakeUri(System.String)
extern "C"  Uri_t19570940 * WebClient_MakeUri_m159089413 (WebClient_t1432723993 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.WebClient::SetupRequest(System.Uri)
extern "C"  WebRequest_t1365124353 * WebClient_SetupRequest_m3399367600 (WebClient_t1432723993 * __this, Uri_t19570940 * ___uri0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Net.WebClient::ReadAll(System.IO.Stream,System.Int32,System.Object)
extern "C"  ByteU5BU5D_t3397334013* WebClient_ReadAll_m1147384516 (WebClient_t1432723993 * __this, Stream_t3255436806 * ___stream0, int32_t ___length1, Il2CppObject * ___userToken2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.WebClient::UrlEncode(System.String)
extern "C"  String_t* WebClient_UrlEncode_m2772509467 (WebClient_t1432723993 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebClient::OnDownloadProgressChanged(System.Net.DownloadProgressChangedEventArgs)
extern "C"  void WebClient_OnDownloadProgressChanged_m2431565293 (WebClient_t1432723993 * __this, DownloadProgressChangedEventArgs_t3269688412 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.WebClient::GetWebRequest(System.Uri)
extern "C"  WebRequest_t1365124353 * WebClient_GetWebRequest_m3663064103 (WebClient_t1432723993 * __this, Uri_t19570940 * ___address0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebResponse System.Net.WebClient::GetWebResponse(System.Net.WebRequest)
extern "C"  WebResponse_t1895226051 * WebClient_GetWebResponse_m247274309 (WebClient_t1432723993 * __this, WebRequest_t1365124353 * ___request0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
