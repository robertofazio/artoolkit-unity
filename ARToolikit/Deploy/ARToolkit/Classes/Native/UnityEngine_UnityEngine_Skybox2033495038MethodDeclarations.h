﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Skybox
struct Skybox_t2033495038;
// UnityEngine.Material
struct Material_t193706927;

#include "codegen/il2cpp-codegen.h"

// UnityEngine.Material UnityEngine.Skybox::get_material()
extern "C"  Material_t193706927 * Skybox_get_material_m2038447772 (Skybox_t2033495038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
