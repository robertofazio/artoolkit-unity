﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TimedSelfDestruct
struct TimedSelfDestruct_t4043215813;

#include "codegen/il2cpp-codegen.h"

// System.Void TimedSelfDestruct::.ctor()
extern "C"  void TimedSelfDestruct__ctor_m1407401746 (TimedSelfDestruct_t4043215813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimedSelfDestruct::Start()
extern "C"  void TimedSelfDestruct_Start_m2725227658 (TimedSelfDestruct_t4043215813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimedSelfDestruct::Update()
extern "C"  void TimedSelfDestruct_Update_m869068077 (TimedSelfDestruct_t4043215813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
