﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginFunctions/LogCallback
struct LogCallback_t2143553514;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Single[]
struct SingleU5BU5D_t577127397;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PluginFunctions_LogCallback2143553514.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void ARNativePlugin::arwRegisterLogCallback(PluginFunctions/LogCallback)
extern "C"  void ARNativePlugin_arwRegisterLogCallback_m2470376146 (Il2CppObject * __this /* static, unused */, LogCallback_t2143553514 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePlugin::arwSetLogLevel(System.Int32)
extern "C"  void ARNativePlugin_arwSetLogLevel_m4002955093 (Il2CppObject * __this /* static, unused */, int32_t ___logLevel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePlugin::arwInitialiseAR()
extern "C"  bool ARNativePlugin_arwInitialiseAR_m2286360556 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePlugin::arwInitialiseARWithOptions(System.Int32,System.Int32)
extern "C"  bool ARNativePlugin_arwInitialiseARWithOptions_m2231924264 (Il2CppObject * __this /* static, unused */, int32_t ___pattSize0, int32_t ___pattCountMax1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePlugin::arwGetARToolKitVersion(System.Text.StringBuilder,System.Int32)
extern "C"  bool ARNativePlugin_arwGetARToolKitVersion_m465574986 (Il2CppObject * __this /* static, unused */, StringBuilder_t1221177846 * ___buffer0, int32_t ___length1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARNativePlugin::arwGetError()
extern "C"  int32_t ARNativePlugin_arwGetError_m4256659126 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePlugin::arwShutdownAR()
extern "C"  bool ARNativePlugin_arwShutdownAR_m1555258779 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePlugin::arwStartRunningB(System.String,System.Byte[],System.Int32,System.Single,System.Single)
extern "C"  bool ARNativePlugin_arwStartRunningB_m307445325 (Il2CppObject * __this /* static, unused */, String_t* ___vconf0, ByteU5BU5D_t3397334013* ___cparaBuff1, int32_t ___cparaBuffLen2, float ___nearPlane3, float ___farPlane4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePlugin::arwStartRunningStereoB(System.String,System.Byte[],System.Int32,System.String,System.Byte[],System.Int32,System.Byte[],System.Int32,System.Single,System.Single)
extern "C"  bool ARNativePlugin_arwStartRunningStereoB_m1235119585 (Il2CppObject * __this /* static, unused */, String_t* ___vconfL0, ByteU5BU5D_t3397334013* ___cparaBuffL1, int32_t ___cparaBuffLenL2, String_t* ___vconfR3, ByteU5BU5D_t3397334013* ___cparaBuffR4, int32_t ___cparaBuffLenR5, ByteU5BU5D_t3397334013* ___transL2RBuff6, int32_t ___transL2RBuffLen7, float ___nearPlane8, float ___farPlane9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePlugin::arwIsRunning()
extern "C"  bool ARNativePlugin_arwIsRunning_m3033062181 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePlugin::arwStopRunning()
extern "C"  bool ARNativePlugin_arwStopRunning_m622006595 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePlugin::arwGetProjectionMatrix(System.Single[])
extern "C"  bool ARNativePlugin_arwGetProjectionMatrix_m3243091243 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___matrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePlugin::arwGetProjectionMatrixStereo(System.Single[],System.Single[])
extern "C"  bool ARNativePlugin_arwGetProjectionMatrixStereo_m427191080 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___matrixL0, SingleU5BU5D_t577127397* ___matrixR1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePlugin::arwGetVideoParams(System.Int32&,System.Int32&,System.Int32&,System.Text.StringBuilder,System.Int32)
extern "C"  bool ARNativePlugin_arwGetVideoParams_m982330381 (Il2CppObject * __this /* static, unused */, int32_t* ___width0, int32_t* ___height1, int32_t* ___pixelSize2, StringBuilder_t1221177846 * ___pixelFormatBuffer3, int32_t ___pixelFormatBufferLen4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePlugin::arwGetVideoParamsStereo(System.Int32&,System.Int32&,System.Int32&,System.Text.StringBuilder,System.Int32,System.Int32&,System.Int32&,System.Int32&,System.Text.StringBuilder,System.Int32)
extern "C"  bool ARNativePlugin_arwGetVideoParamsStereo_m4115500785 (Il2CppObject * __this /* static, unused */, int32_t* ___widthL0, int32_t* ___heightL1, int32_t* ___pixelSizeL2, StringBuilder_t1221177846 * ___pixelFormatBufferL3, int32_t ___pixelFormatBufferLenL4, int32_t* ___widthR5, int32_t* ___heightR6, int32_t* ___pixelSizeR7, StringBuilder_t1221177846 * ___pixelFormatBufferR8, int32_t ___pixelFormatBufferLenR9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePlugin::arwCapture()
extern "C"  bool ARNativePlugin_arwCapture_m808157064 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePlugin::arwUpdateAR()
extern "C"  bool ARNativePlugin_arwUpdateAR_m3219639630 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePlugin::arwUpdateTexture(System.IntPtr)
extern "C"  bool ARNativePlugin_arwUpdateTexture_m938831980 (Il2CppObject * __this /* static, unused */, IntPtr_t ___colors0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePlugin::arwUpdateTextureStereo(System.IntPtr,System.IntPtr)
extern "C"  bool ARNativePlugin_arwUpdateTextureStereo_m4292622656 (Il2CppObject * __this /* static, unused */, IntPtr_t ___colorsL0, IntPtr_t ___colorsR1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePlugin::arwUpdateTexture32(System.IntPtr)
extern "C"  bool ARNativePlugin_arwUpdateTexture32_m443923741 (Il2CppObject * __this /* static, unused */, IntPtr_t ___colors320, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePlugin::arwUpdateTexture32Stereo(System.IntPtr,System.IntPtr)
extern "C"  bool ARNativePlugin_arwUpdateTexture32Stereo_m993054649 (Il2CppObject * __this /* static, unused */, IntPtr_t ___colors32L0, IntPtr_t ___colors32R1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePlugin::arwUpdateTextureGL(System.Int32)
extern "C"  bool ARNativePlugin_arwUpdateTextureGL_m2994090928 (Il2CppObject * __this /* static, unused */, int32_t ___textureID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePlugin::arwUpdateTextureGLStereo(System.Int32,System.Int32)
extern "C"  bool ARNativePlugin_arwUpdateTextureGLStereo_m2235959663 (Il2CppObject * __this /* static, unused */, int32_t ___textureID_L0, int32_t ___textureID_R1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePlugin::arwSetUnityRenderEventUpdateTextureGLTextureID(System.Int32)
extern "C"  void ARNativePlugin_arwSetUnityRenderEventUpdateTextureGLTextureID_m1975041325 (Il2CppObject * __this /* static, unused */, int32_t ___textureID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePlugin::arwSetUnityRenderEventUpdateTextureGLStereoTextureIDs(System.Int32,System.Int32)
extern "C"  void ARNativePlugin_arwSetUnityRenderEventUpdateTextureGLStereoTextureIDs_m1459928577 (Il2CppObject * __this /* static, unused */, int32_t ___textureID_L0, int32_t ___textureID_R1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARNativePlugin::arwGetMarkerPatternCount(System.Int32)
extern "C"  int32_t ARNativePlugin_arwGetMarkerPatternCount_m1314450108 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePlugin::arwGetMarkerPatternConfig(System.Int32,System.Int32,System.Single[],System.Single&,System.Single&,System.Int32&,System.Int32&)
extern "C"  bool ARNativePlugin_arwGetMarkerPatternConfig_m4089949209 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___patternID1, SingleU5BU5D_t577127397* ___matrix2, float* ___width3, float* ___height4, int32_t* ___imageSizeX5, int32_t* ___imageSizeY6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePlugin::arwGetMarkerPatternImage(System.Int32,System.Int32,UnityEngine.Color[])
extern "C"  bool ARNativePlugin_arwGetMarkerPatternImage_m769298731 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___patternID1, ColorU5BU5D_t672350442* ___colors2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePlugin::arwGetMarkerOptionBool(System.Int32,System.Int32)
extern "C"  bool ARNativePlugin_arwGetMarkerOptionBool_m2354836317 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePlugin::arwSetMarkerOptionBool(System.Int32,System.Int32,System.Boolean)
extern "C"  void ARNativePlugin_arwSetMarkerOptionBool_m1342859260 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, bool ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARNativePlugin::arwGetMarkerOptionInt(System.Int32,System.Int32)
extern "C"  int32_t ARNativePlugin_arwGetMarkerOptionInt_m1863724448 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePlugin::arwSetMarkerOptionInt(System.Int32,System.Int32,System.Int32)
extern "C"  void ARNativePlugin_arwSetMarkerOptionInt_m1852067113 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, int32_t ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ARNativePlugin::arwGetMarkerOptionFloat(System.Int32,System.Int32)
extern "C"  float ARNativePlugin_arwGetMarkerOptionFloat_m1337828381 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePlugin::arwSetMarkerOptionFloat(System.Int32,System.Int32,System.Single)
extern "C"  void ARNativePlugin_arwSetMarkerOptionFloat_m3494403524 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePlugin::arwSetVideoDebugMode(System.Boolean)
extern "C"  void ARNativePlugin_arwSetVideoDebugMode_m1349895608 (Il2CppObject * __this /* static, unused */, bool ___debug0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePlugin::arwGetVideoDebugMode()
extern "C"  bool ARNativePlugin_arwGetVideoDebugMode_m2178590269 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePlugin::arwSetVideoThreshold(System.Int32)
extern "C"  void ARNativePlugin_arwSetVideoThreshold_m2002595265 (Il2CppObject * __this /* static, unused */, int32_t ___threshold0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARNativePlugin::arwGetVideoThreshold()
extern "C"  int32_t ARNativePlugin_arwGetVideoThreshold_m2841997392 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePlugin::arwSetVideoThresholdMode(System.Int32)
extern "C"  void ARNativePlugin_arwSetVideoThresholdMode_m1677852552 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARNativePlugin::arwGetVideoThresholdMode()
extern "C"  int32_t ARNativePlugin_arwGetVideoThresholdMode_m1626559629 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePlugin::arwSetLabelingMode(System.Int32)
extern "C"  void ARNativePlugin_arwSetLabelingMode_m801205656 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARNativePlugin::arwGetLabelingMode()
extern "C"  int32_t ARNativePlugin_arwGetLabelingMode_m3294800397 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePlugin::arwSetBorderSize(System.Single)
extern "C"  void ARNativePlugin_arwSetBorderSize_m3785932980 (Il2CppObject * __this /* static, unused */, float ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ARNativePlugin::arwGetBorderSize()
extern "C"  float ARNativePlugin_arwGetBorderSize_m1115123341 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePlugin::arwSetPatternDetectionMode(System.Int32)
extern "C"  void ARNativePlugin_arwSetPatternDetectionMode_m2117597091 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARNativePlugin::arwGetPatternDetectionMode()
extern "C"  int32_t ARNativePlugin_arwGetPatternDetectionMode_m1233416524 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePlugin::arwSetMatrixCodeType(System.Int32)
extern "C"  void ARNativePlugin_arwSetMatrixCodeType_m675665857 (Il2CppObject * __this /* static, unused */, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARNativePlugin::arwGetMatrixCodeType()
extern "C"  int32_t ARNativePlugin_arwGetMatrixCodeType_m176152962 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePlugin::arwSetImageProcMode(System.Int32)
extern "C"  void ARNativePlugin_arwSetImageProcMode_m302197153 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARNativePlugin::arwGetImageProcMode()
extern "C"  int32_t ARNativePlugin_arwGetImageProcMode_m676231894 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePlugin::arwSetNFTMultiMode(System.Boolean)
extern "C"  void ARNativePlugin_arwSetNFTMultiMode_m733908047 (Il2CppObject * __this /* static, unused */, bool ___on0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePlugin::arwGetNFTMultiMode()
extern "C"  bool ARNativePlugin_arwGetNFTMultiMode_m1076663180 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARNativePlugin::arwAddMarker(System.String)
extern "C"  int32_t ARNativePlugin_arwAddMarker_m1259082831 (Il2CppObject * __this /* static, unused */, String_t* ___cfg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePlugin::arwRemoveMarker(System.Int32)
extern "C"  bool ARNativePlugin_arwRemoveMarker_m78809961 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARNativePlugin::arwRemoveAllMarkers()
extern "C"  int32_t ARNativePlugin_arwRemoveAllMarkers_m3455426882 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePlugin::arwQueryMarkerVisibility(System.Int32)
extern "C"  bool ARNativePlugin_arwQueryMarkerVisibility_m3920274723 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePlugin::arwQueryMarkerTransformation(System.Int32,System.Single[])
extern "C"  bool ARNativePlugin_arwQueryMarkerTransformation_m3853733251 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, SingleU5BU5D_t577127397* ___matrix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePlugin::arwQueryMarkerTransformationStereo(System.Int32,System.Single[],System.Single[])
extern "C"  bool ARNativePlugin_arwQueryMarkerTransformationStereo_m3906452060 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, SingleU5BU5D_t577127397* ___matrixL1, SingleU5BU5D_t577127397* ___matrixR2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePlugin::arwLoadOpticalParams(System.String,System.Byte[],System.Int32,System.Single&,System.Single&,System.Single[],System.Single[])
extern "C"  bool ARNativePlugin_arwLoadOpticalParams_m1652819160 (Il2CppObject * __this /* static, unused */, String_t* ___optical_param_name0, ByteU5BU5D_t3397334013* ___optical_param_buff1, int32_t ___optical_param_buffLen2, float* ___fovy_p3, float* ___aspect_p4, SingleU5BU5D_t577127397* ___m5, SingleU5BU5D_t577127397* ___p6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
