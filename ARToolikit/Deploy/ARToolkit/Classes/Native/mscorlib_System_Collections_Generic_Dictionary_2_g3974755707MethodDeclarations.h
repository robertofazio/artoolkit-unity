﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>
struct Dictionary_2_t3974755707;
// System.Collections.Generic.IEqualityComparer`1<ARController/ARToolKitThresholdMode>
struct IEqualityComparer_1_t1374213565;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<ARController/ARToolKitThresholdMode,System.Object>[]
struct KeyValuePair_2U5BU5D_t819931804;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<ARController/ARToolKitThresholdMode,System.Object>>
struct IEnumerator_1_t3502592052;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/ValueCollection<ARController/ARToolKitThresholdMode,System.Object>
struct ValueCollection_t2677815550;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21732100929.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_ARController_ARToolKitThresholdM2161580787.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En999813113.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m4210699770_gshared (Dictionary_2_t3974755707 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m4210699770(__this, method) ((  void (*) (Dictionary_2_t3974755707 *, const MethodInfo*))Dictionary_2__ctor_m4210699770_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1668610479_gshared (Dictionary_2_t3974755707 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m1668610479(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t3974755707 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1668610479_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m4253698733_gshared (Dictionary_2_t3974755707 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m4253698733(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t3974755707 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m4253698733_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m196225687_gshared (Dictionary_2_t3974755707 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m196225687(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3974755707 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m196225687_gshared)(__this, ___info0, ___context1, method)
// System.Object System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m3616125538_gshared (Dictionary_2_t3974755707 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m3616125538(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3974755707 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m3616125538_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m2382523583_gshared (Dictionary_2_t3974755707 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m2382523583(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3974755707 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m2382523583_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m1399973568_gshared (Dictionary_2_t3974755707 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m1399973568(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3974755707 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1399973568_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m2116753067_gshared (Dictionary_2_t3974755707 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m2116753067(__this, ___key0, method) ((  void (*) (Dictionary_2_t3974755707 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2116753067_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3460677314_gshared (Dictionary_2_t3974755707 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3460677314(__this, method) ((  bool (*) (Dictionary_2_t3974755707 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3460677314_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1696777738_gshared (Dictionary_2_t3974755707 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1696777738(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3974755707 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m1696777738_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3659732696_gshared (Dictionary_2_t3974755707 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3659732696(__this, method) ((  bool (*) (Dictionary_2_t3974755707 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3659732696_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m91707831_gshared (Dictionary_2_t3974755707 * __this, KeyValuePair_2_t1732100929  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m91707831(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t3974755707 *, KeyValuePair_2_t1732100929 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m91707831_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2153589209_gshared (Dictionary_2_t3974755707 * __this, KeyValuePair_2_t1732100929  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2153589209(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3974755707 *, KeyValuePair_2_t1732100929 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2153589209_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1270915243_gshared (Dictionary_2_t3974755707 * __this, KeyValuePair_2U5BU5D_t819931804* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1270915243(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3974755707 *, KeyValuePair_2U5BU5D_t819931804*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1270915243_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3249920142_gshared (Dictionary_2_t3974755707 * __this, KeyValuePair_2_t1732100929  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3249920142(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3974755707 *, KeyValuePair_2_t1732100929 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3249920142_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m2474888602_gshared (Dictionary_2_t3974755707 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m2474888602(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3974755707 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m2474888602_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2470160909_gshared (Dictionary_2_t3974755707 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2470160909(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3974755707 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m2470160909_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m907688880_gshared (Dictionary_2_t3974755707 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m907688880(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3974755707 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m907688880_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1771544691_gshared (Dictionary_2_t3974755707 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1771544691(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3974755707 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1771544691_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m3783655170_gshared (Dictionary_2_t3974755707 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m3783655170(__this, method) ((  int32_t (*) (Dictionary_2_t3974755707 *, const MethodInfo*))Dictionary_2_get_Count_m3783655170_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m24555435_gshared (Dictionary_2_t3974755707 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m24555435(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3974755707 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m24555435_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m3876567088_gshared (Dictionary_2_t3974755707 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m3876567088(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3974755707 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m3876567088_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m3489271398_gshared (Dictionary_2_t3974755707 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m3489271398(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t3974755707 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m3489271398_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m4183202349_gshared (Dictionary_2_t3974755707 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m4183202349(__this, ___size0, method) ((  void (*) (Dictionary_2_t3974755707 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m4183202349_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m4170834099_gshared (Dictionary_2_t3974755707 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m4170834099(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3974755707 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m4170834099_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t1732100929  Dictionary_2_make_pair_m84129933_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m84129933(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t1732100929  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m84129933_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m741635157_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m741635157(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m741635157_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m3867174436_gshared (Dictionary_2_t3974755707 * __this, KeyValuePair_2U5BU5D_t819931804* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m3867174436(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3974755707 *, KeyValuePair_2U5BU5D_t819931804*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m3867174436_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m1013047352_gshared (Dictionary_2_t3974755707 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m1013047352(__this, method) ((  void (*) (Dictionary_2_t3974755707 *, const MethodInfo*))Dictionary_2_Resize_m1013047352_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m139277441_gshared (Dictionary_2_t3974755707 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m139277441(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3974755707 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m139277441_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m1560574333_gshared (Dictionary_2_t3974755707 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m1560574333(__this, method) ((  void (*) (Dictionary_2_t3974755707 *, const MethodInfo*))Dictionary_2_Clear_m1560574333_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m1572032371_gshared (Dictionary_2_t3974755707 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m1572032371(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3974755707 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m1572032371_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m725613651_gshared (Dictionary_2_t3974755707 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m725613651(__this, ___value0, method) ((  bool (*) (Dictionary_2_t3974755707 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m725613651_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m587002190_gshared (Dictionary_2_t3974755707 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m587002190(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3974755707 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m587002190_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m900610016_gshared (Dictionary_2_t3974755707 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m900610016(__this, ___sender0, method) ((  void (*) (Dictionary_2_t3974755707 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m900610016_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m3313933617_gshared (Dictionary_2_t3974755707 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m3313933617(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3974755707 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m3313933617_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m2150245780_gshared (Dictionary_2_t3974755707 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m2150245780(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t3974755707 *, int32_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m2150245780_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::get_Values()
extern "C"  ValueCollection_t2677815550 * Dictionary_2_get_Values_m3446542311_gshared (Dictionary_2_t3974755707 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m3446542311(__this, method) ((  ValueCollection_t2677815550 * (*) (Dictionary_2_t3974755707 *, const MethodInfo*))Dictionary_2_get_Values_m3446542311_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m2863440466_gshared (Dictionary_2_t3974755707 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m2863440466(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t3974755707 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m2863440466_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m1918541754_gshared (Dictionary_2_t3974755707 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1918541754(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t3974755707 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1918541754_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m3267394844_gshared (Dictionary_2_t3974755707 * __this, KeyValuePair_2_t1732100929  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m3267394844(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t3974755707 *, KeyValuePair_2_t1732100929 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m3267394844_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::GetEnumerator()
extern "C"  Enumerator_t999813113  Dictionary_2_GetEnumerator_m2991035609_gshared (Dictionary_2_t3974755707 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m2991035609(__this, method) ((  Enumerator_t999813113  (*) (Dictionary_2_t3974755707 *, const MethodInfo*))Dictionary_2_GetEnumerator_m2991035609_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__0_m3851201074_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m3851201074(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m3851201074_gshared)(__this /* static, unused */, ___key0, ___value1, method)
