﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3779665792.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_ContentMode2920913530.h"

// System.Void System.Array/InternalEnumerator`1<ContentMode>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4286116161_gshared (InternalEnumerator_1_t3779665792 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m4286116161(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3779665792 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m4286116161_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<ContentMode>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1155429645_gshared (InternalEnumerator_1_t3779665792 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1155429645(__this, method) ((  void (*) (InternalEnumerator_1_t3779665792 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1155429645_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<ContentMode>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m246749201_gshared (InternalEnumerator_1_t3779665792 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m246749201(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3779665792 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m246749201_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<ContentMode>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1297509862_gshared (InternalEnumerator_1_t3779665792 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1297509862(__this, method) ((  void (*) (InternalEnumerator_1_t3779665792 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1297509862_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<ContentMode>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3699766293_gshared (InternalEnumerator_1_t3779665792 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3699766293(__this, method) ((  bool (*) (InternalEnumerator_1_t3779665792 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3699766293_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<ContentMode>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m2158644774_gshared (InternalEnumerator_1_t3779665792 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2158644774(__this, method) ((  int32_t (*) (InternalEnumerator_1_t3779665792 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2158644774_gshared)(__this, method)
