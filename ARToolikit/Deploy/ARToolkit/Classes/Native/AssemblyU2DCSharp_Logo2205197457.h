﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Logo
struct  Logo_t2205197457  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Logo::speedRotate
	float ___speedRotate_2;
	// System.Single Logo::startScale
	float ___startScale_3;
	// System.Single Logo::desireScale
	float ___desireScale_4;
	// System.Single Logo::desirePos
	float ___desirePos_5;
	// System.Single Logo::lerpScale
	float ___lerpScale_6;
	// System.Single Logo::lerpPos
	float ___lerpPos_7;
	// System.Single Logo::t
	float ___t_8;

public:
	inline static int32_t get_offset_of_speedRotate_2() { return static_cast<int32_t>(offsetof(Logo_t2205197457, ___speedRotate_2)); }
	inline float get_speedRotate_2() const { return ___speedRotate_2; }
	inline float* get_address_of_speedRotate_2() { return &___speedRotate_2; }
	inline void set_speedRotate_2(float value)
	{
		___speedRotate_2 = value;
	}

	inline static int32_t get_offset_of_startScale_3() { return static_cast<int32_t>(offsetof(Logo_t2205197457, ___startScale_3)); }
	inline float get_startScale_3() const { return ___startScale_3; }
	inline float* get_address_of_startScale_3() { return &___startScale_3; }
	inline void set_startScale_3(float value)
	{
		___startScale_3 = value;
	}

	inline static int32_t get_offset_of_desireScale_4() { return static_cast<int32_t>(offsetof(Logo_t2205197457, ___desireScale_4)); }
	inline float get_desireScale_4() const { return ___desireScale_4; }
	inline float* get_address_of_desireScale_4() { return &___desireScale_4; }
	inline void set_desireScale_4(float value)
	{
		___desireScale_4 = value;
	}

	inline static int32_t get_offset_of_desirePos_5() { return static_cast<int32_t>(offsetof(Logo_t2205197457, ___desirePos_5)); }
	inline float get_desirePos_5() const { return ___desirePos_5; }
	inline float* get_address_of_desirePos_5() { return &___desirePos_5; }
	inline void set_desirePos_5(float value)
	{
		___desirePos_5 = value;
	}

	inline static int32_t get_offset_of_lerpScale_6() { return static_cast<int32_t>(offsetof(Logo_t2205197457, ___lerpScale_6)); }
	inline float get_lerpScale_6() const { return ___lerpScale_6; }
	inline float* get_address_of_lerpScale_6() { return &___lerpScale_6; }
	inline void set_lerpScale_6(float value)
	{
		___lerpScale_6 = value;
	}

	inline static int32_t get_offset_of_lerpPos_7() { return static_cast<int32_t>(offsetof(Logo_t2205197457, ___lerpPos_7)); }
	inline float get_lerpPos_7() const { return ___lerpPos_7; }
	inline float* get_address_of_lerpPos_7() { return &___lerpPos_7; }
	inline void set_lerpPos_7(float value)
	{
		___lerpPos_7 = value;
	}

	inline static int32_t get_offset_of_t_8() { return static_cast<int32_t>(offsetof(Logo_t2205197457, ___t_8)); }
	inline float get_t_8() const { return ___t_8; }
	inline float* get_address_of_t_8() { return &___t_8; }
	inline void set_t_8(float value)
	{
		___t_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
