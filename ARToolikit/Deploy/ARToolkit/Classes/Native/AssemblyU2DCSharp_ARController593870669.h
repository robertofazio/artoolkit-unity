﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action`1<System.String>
struct Action_1_t1831019615;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// UnityEngine.Color32[]
struct Color32U5BU5D_t30278651;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Camera
struct Camera_t189460977;
// System.Collections.Generic.Dictionary`2<ContentMode,System.String>
struct Dictionary_2_t3757846578;
// System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.String>
struct Dictionary_2_t3314526645;
// UnityEngine.GUIStyle[]
struct GUIStyleU5BU5D_t2497716199;
// PluginFunctions/LogCallback
struct LogCallback_t2143553514;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"
#include "AssemblyU2DCSharp_ContentAlign2517740362.h"
#include "AssemblyU2DCSharp_ContentMode2920913530.h"
#include "AssemblyU2DCSharp_ARController_ARToolKitThresholdM2161580787.h"
#include "AssemblyU2DCSharp_ARController_ARToolKitLabelingMod925999490.h"
#include "AssemblyU2DCSharp_ARController_ARToolKitPatternDet1396884775.h"
#include "AssemblyU2DCSharp_ARController_ARToolKitMatrixCode3871848761.h"
#include "AssemblyU2DCSharp_ARController_ARToolKitImageProcM1927279569.h"
#include "AssemblyU2DCSharp_ARController_AR_LOG_LEVEL1976762171.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARController
struct  ARController_t593870669  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean ARController::bFound
	bool ___bFound_2;
	// System.Boolean ARController::UseNativeGLTexturingIfAvailable
	bool ___UseNativeGLTexturingIfAvailable_7;
	// System.Boolean ARController::AllowNonRGBVideo
	bool ___AllowNonRGBVideo_8;
	// System.Boolean ARController::QuitOnEscOrBack
	bool ___QuitOnEscOrBack_9;
	// System.Boolean ARController::AutoStartAR
	bool ___AutoStartAR_10;
	// System.String ARController::_version
	String_t* ____version_11;
	// System.Boolean ARController::_running
	bool ____running_12;
	// System.Boolean ARController::_runOnUnpause
	bool ____runOnUnpause_13;
	// System.Boolean ARController::_sceneConfiguredForVideo
	bool ____sceneConfiguredForVideo_14;
	// System.Boolean ARController::_sceneConfiguredForVideoWaitingMessageLogged
	bool ____sceneConfiguredForVideoWaitingMessageLogged_15;
	// System.Boolean ARController::_useNativeGLTexturing
	bool ____useNativeGLTexturing_16;
	// System.Boolean ARController::_useColor32
	bool ____useColor32_17;
	// System.String ARController::videoCParamName0
	String_t* ___videoCParamName0_18;
	// System.String ARController::videoConfigurationWindows0
	String_t* ___videoConfigurationWindows0_19;
	// System.String ARController::videoConfigurationMacOSX0
	String_t* ___videoConfigurationMacOSX0_20;
	// System.String ARController::videoConfigurationiOS0
	String_t* ___videoConfigurationiOS0_21;
	// System.String ARController::videoConfigurationAndroid0
	String_t* ___videoConfigurationAndroid0_22;
	// System.String ARController::videoConfigurationWindowsStore0
	String_t* ___videoConfigurationWindowsStore0_23;
	// System.String ARController::videoConfigurationLinux0
	String_t* ___videoConfigurationLinux0_24;
	// System.Int32 ARController::BackgroundLayer0
	int32_t ___BackgroundLayer0_25;
	// System.Int32 ARController::_videoWidth0
	int32_t ____videoWidth0_26;
	// System.Int32 ARController::_videoHeight0
	int32_t ____videoHeight0_27;
	// System.Int32 ARController::_videoPixelSize0
	int32_t ____videoPixelSize0_28;
	// System.String ARController::_videoPixelFormatString0
	String_t* ____videoPixelFormatString0_29;
	// UnityEngine.Matrix4x4 ARController::_videoProjectionMatrix0
	Matrix4x4_t2933234003  ____videoProjectionMatrix0_30;
	// UnityEngine.GameObject ARController::_videoBackgroundMeshGO0
	GameObject_t1756533147 * ____videoBackgroundMeshGO0_31;
	// UnityEngine.Color[] ARController::_videoColorArray0
	ColorU5BU5D_t672350442* ____videoColorArray0_32;
	// UnityEngine.Color32[] ARController::_videoColor32Array0
	Color32U5BU5D_t30278651* ____videoColor32Array0_33;
	// UnityEngine.Texture2D ARController::_videoTexture0
	Texture2D_t3542995729 * ____videoTexture0_34;
	// UnityEngine.Material ARController::_videoMaterial0
	Material_t193706927 * ____videoMaterial0_35;
	// System.Boolean ARController::VideoIsStereo
	bool ___VideoIsStereo_36;
	// System.String ARController::transL2RName
	String_t* ___transL2RName_37;
	// System.String ARController::videoCParamName1
	String_t* ___videoCParamName1_38;
	// System.String ARController::videoConfigurationWindows1
	String_t* ___videoConfigurationWindows1_39;
	// System.String ARController::videoConfigurationMacOSX1
	String_t* ___videoConfigurationMacOSX1_40;
	// System.String ARController::videoConfigurationiOS1
	String_t* ___videoConfigurationiOS1_41;
	// System.String ARController::videoConfigurationAndroid1
	String_t* ___videoConfigurationAndroid1_42;
	// System.String ARController::videoConfigurationWindowsStore1
	String_t* ___videoConfigurationWindowsStore1_43;
	// System.String ARController::videoConfigurationLinux1
	String_t* ___videoConfigurationLinux1_44;
	// System.Int32 ARController::BackgroundLayer1
	int32_t ___BackgroundLayer1_45;
	// System.Int32 ARController::_videoWidth1
	int32_t ____videoWidth1_46;
	// System.Int32 ARController::_videoHeight1
	int32_t ____videoHeight1_47;
	// System.Int32 ARController::_videoPixelSize1
	int32_t ____videoPixelSize1_48;
	// System.String ARController::_videoPixelFormatString1
	String_t* ____videoPixelFormatString1_49;
	// UnityEngine.Matrix4x4 ARController::_videoProjectionMatrix1
	Matrix4x4_t2933234003  ____videoProjectionMatrix1_50;
	// UnityEngine.GameObject ARController::_videoBackgroundMeshGO1
	GameObject_t1756533147 * ____videoBackgroundMeshGO1_51;
	// UnityEngine.Color[] ARController::_videoColorArray1
	ColorU5BU5D_t672350442* ____videoColorArray1_52;
	// UnityEngine.Color32[] ARController::_videoColor32Array1
	Color32U5BU5D_t30278651* ____videoColor32Array1_53;
	// UnityEngine.Texture2D ARController::_videoTexture1
	Texture2D_t3542995729 * ____videoTexture1_54;
	// UnityEngine.Material ARController::_videoMaterial1
	Material_t193706927 * ____videoMaterial1_55;
	// UnityEngine.Camera ARController::clearCamera
	Camera_t189460977 * ___clearCamera_56;
	// UnityEngine.GameObject ARController::_videoBackgroundCameraGO0
	GameObject_t1756533147 * ____videoBackgroundCameraGO0_57;
	// UnityEngine.Camera ARController::_videoBackgroundCamera0
	Camera_t189460977 * ____videoBackgroundCamera0_58;
	// UnityEngine.GameObject ARController::_videoBackgroundCameraGO1
	GameObject_t1756533147 * ____videoBackgroundCameraGO1_59;
	// UnityEngine.Camera ARController::_videoBackgroundCamera1
	Camera_t189460977 * ____videoBackgroundCamera1_60;
	// System.Single ARController::NearPlane
	float ___NearPlane_61;
	// System.Single ARController::FarPlane
	float ___FarPlane_62;
	// System.Boolean ARController::ContentRotate90
	bool ___ContentRotate90_63;
	// System.Boolean ARController::ContentFlipH
	bool ___ContentFlipH_64;
	// System.Boolean ARController::ContentFlipV
	bool ___ContentFlipV_65;
	// ContentAlign ARController::ContentAlign
	int32_t ___ContentAlign_66;
	// System.Int32 ARController::frameCounter
	int32_t ___frameCounter_68;
	// System.Single ARController::timeCounter
	float ___timeCounter_69;
	// System.Single ARController::lastFramerate
	float ___lastFramerate_70;
	// System.Single ARController::refreshTime
	float ___refreshTime_71;
	// ContentMode ARController::currentContentMode
	int32_t ___currentContentMode_73;
	// ARController/ARToolKitThresholdMode ARController::currentThresholdMode
	int32_t ___currentThresholdMode_74;
	// System.Int32 ARController::currentThreshold
	int32_t ___currentThreshold_75;
	// ARController/ARToolKitLabelingMode ARController::currentLabelingMode
	int32_t ___currentLabelingMode_76;
	// System.Int32 ARController::currentTemplateSize
	int32_t ___currentTemplateSize_77;
	// System.Int32 ARController::currentTemplateCountMax
	int32_t ___currentTemplateCountMax_78;
	// System.Single ARController::currentBorderSize
	float ___currentBorderSize_79;
	// ARController/ARToolKitPatternDetectionMode ARController::currentPatternDetectionMode
	int32_t ___currentPatternDetectionMode_80;
	// ARController/ARToolKitMatrixCodeType ARController::currentMatrixCodeType
	int32_t ___currentMatrixCodeType_81;
	// ARController/ARToolKitImageProcMode ARController::currentImageProcMode
	int32_t ___currentImageProcMode_82;
	// System.Boolean ARController::currentUseVideoBackground
	bool ___currentUseVideoBackground_83;
	// System.Boolean ARController::currentNFTMultiMode
	bool ___currentNFTMultiMode_84;
	// ARController/AR_LOG_LEVEL ARController::currentLogLevel
	int32_t ___currentLogLevel_85;
	// UnityEngine.GUIStyle[] ARController::style
	GUIStyleU5BU5D_t2497716199* ___style_86;
	// System.Boolean ARController::guiSetup
	bool ___guiSetup_87;
	// System.Boolean ARController::showGUIErrorDialog
	bool ___showGUIErrorDialog_88;
	// System.String ARController::showGUIErrorDialogContent
	String_t* ___showGUIErrorDialogContent_89;
	// UnityEngine.Rect ARController::showGUIErrorDialogWinRect
	Rect_t3681755626  ___showGUIErrorDialogWinRect_90;
	// System.Boolean ARController::showGUIDebug
	bool ___showGUIDebug_91;
	// System.Boolean ARController::showGUIDebugInfo
	bool ___showGUIDebugInfo_92;
	// System.Boolean ARController::showGUIDebugLogConsole
	bool ___showGUIDebugLogConsole_93;
	// UnityEngine.Vector2 ARController::scrollPosition
	Vector2_t2243707579  ___scrollPosition_94;

public:
	inline static int32_t get_offset_of_bFound_2() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___bFound_2)); }
	inline bool get_bFound_2() const { return ___bFound_2; }
	inline bool* get_address_of_bFound_2() { return &___bFound_2; }
	inline void set_bFound_2(bool value)
	{
		___bFound_2 = value;
	}

	inline static int32_t get_offset_of_UseNativeGLTexturingIfAvailable_7() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___UseNativeGLTexturingIfAvailable_7)); }
	inline bool get_UseNativeGLTexturingIfAvailable_7() const { return ___UseNativeGLTexturingIfAvailable_7; }
	inline bool* get_address_of_UseNativeGLTexturingIfAvailable_7() { return &___UseNativeGLTexturingIfAvailable_7; }
	inline void set_UseNativeGLTexturingIfAvailable_7(bool value)
	{
		___UseNativeGLTexturingIfAvailable_7 = value;
	}

	inline static int32_t get_offset_of_AllowNonRGBVideo_8() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___AllowNonRGBVideo_8)); }
	inline bool get_AllowNonRGBVideo_8() const { return ___AllowNonRGBVideo_8; }
	inline bool* get_address_of_AllowNonRGBVideo_8() { return &___AllowNonRGBVideo_8; }
	inline void set_AllowNonRGBVideo_8(bool value)
	{
		___AllowNonRGBVideo_8 = value;
	}

	inline static int32_t get_offset_of_QuitOnEscOrBack_9() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___QuitOnEscOrBack_9)); }
	inline bool get_QuitOnEscOrBack_9() const { return ___QuitOnEscOrBack_9; }
	inline bool* get_address_of_QuitOnEscOrBack_9() { return &___QuitOnEscOrBack_9; }
	inline void set_QuitOnEscOrBack_9(bool value)
	{
		___QuitOnEscOrBack_9 = value;
	}

	inline static int32_t get_offset_of_AutoStartAR_10() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___AutoStartAR_10)); }
	inline bool get_AutoStartAR_10() const { return ___AutoStartAR_10; }
	inline bool* get_address_of_AutoStartAR_10() { return &___AutoStartAR_10; }
	inline void set_AutoStartAR_10(bool value)
	{
		___AutoStartAR_10 = value;
	}

	inline static int32_t get_offset_of__version_11() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____version_11)); }
	inline String_t* get__version_11() const { return ____version_11; }
	inline String_t** get_address_of__version_11() { return &____version_11; }
	inline void set__version_11(String_t* value)
	{
		____version_11 = value;
		Il2CppCodeGenWriteBarrier(&____version_11, value);
	}

	inline static int32_t get_offset_of__running_12() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____running_12)); }
	inline bool get__running_12() const { return ____running_12; }
	inline bool* get_address_of__running_12() { return &____running_12; }
	inline void set__running_12(bool value)
	{
		____running_12 = value;
	}

	inline static int32_t get_offset_of__runOnUnpause_13() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____runOnUnpause_13)); }
	inline bool get__runOnUnpause_13() const { return ____runOnUnpause_13; }
	inline bool* get_address_of__runOnUnpause_13() { return &____runOnUnpause_13; }
	inline void set__runOnUnpause_13(bool value)
	{
		____runOnUnpause_13 = value;
	}

	inline static int32_t get_offset_of__sceneConfiguredForVideo_14() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____sceneConfiguredForVideo_14)); }
	inline bool get__sceneConfiguredForVideo_14() const { return ____sceneConfiguredForVideo_14; }
	inline bool* get_address_of__sceneConfiguredForVideo_14() { return &____sceneConfiguredForVideo_14; }
	inline void set__sceneConfiguredForVideo_14(bool value)
	{
		____sceneConfiguredForVideo_14 = value;
	}

	inline static int32_t get_offset_of__sceneConfiguredForVideoWaitingMessageLogged_15() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____sceneConfiguredForVideoWaitingMessageLogged_15)); }
	inline bool get__sceneConfiguredForVideoWaitingMessageLogged_15() const { return ____sceneConfiguredForVideoWaitingMessageLogged_15; }
	inline bool* get_address_of__sceneConfiguredForVideoWaitingMessageLogged_15() { return &____sceneConfiguredForVideoWaitingMessageLogged_15; }
	inline void set__sceneConfiguredForVideoWaitingMessageLogged_15(bool value)
	{
		____sceneConfiguredForVideoWaitingMessageLogged_15 = value;
	}

	inline static int32_t get_offset_of__useNativeGLTexturing_16() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____useNativeGLTexturing_16)); }
	inline bool get__useNativeGLTexturing_16() const { return ____useNativeGLTexturing_16; }
	inline bool* get_address_of__useNativeGLTexturing_16() { return &____useNativeGLTexturing_16; }
	inline void set__useNativeGLTexturing_16(bool value)
	{
		____useNativeGLTexturing_16 = value;
	}

	inline static int32_t get_offset_of__useColor32_17() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____useColor32_17)); }
	inline bool get__useColor32_17() const { return ____useColor32_17; }
	inline bool* get_address_of__useColor32_17() { return &____useColor32_17; }
	inline void set__useColor32_17(bool value)
	{
		____useColor32_17 = value;
	}

	inline static int32_t get_offset_of_videoCParamName0_18() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___videoCParamName0_18)); }
	inline String_t* get_videoCParamName0_18() const { return ___videoCParamName0_18; }
	inline String_t** get_address_of_videoCParamName0_18() { return &___videoCParamName0_18; }
	inline void set_videoCParamName0_18(String_t* value)
	{
		___videoCParamName0_18 = value;
		Il2CppCodeGenWriteBarrier(&___videoCParamName0_18, value);
	}

	inline static int32_t get_offset_of_videoConfigurationWindows0_19() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___videoConfigurationWindows0_19)); }
	inline String_t* get_videoConfigurationWindows0_19() const { return ___videoConfigurationWindows0_19; }
	inline String_t** get_address_of_videoConfigurationWindows0_19() { return &___videoConfigurationWindows0_19; }
	inline void set_videoConfigurationWindows0_19(String_t* value)
	{
		___videoConfigurationWindows0_19 = value;
		Il2CppCodeGenWriteBarrier(&___videoConfigurationWindows0_19, value);
	}

	inline static int32_t get_offset_of_videoConfigurationMacOSX0_20() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___videoConfigurationMacOSX0_20)); }
	inline String_t* get_videoConfigurationMacOSX0_20() const { return ___videoConfigurationMacOSX0_20; }
	inline String_t** get_address_of_videoConfigurationMacOSX0_20() { return &___videoConfigurationMacOSX0_20; }
	inline void set_videoConfigurationMacOSX0_20(String_t* value)
	{
		___videoConfigurationMacOSX0_20 = value;
		Il2CppCodeGenWriteBarrier(&___videoConfigurationMacOSX0_20, value);
	}

	inline static int32_t get_offset_of_videoConfigurationiOS0_21() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___videoConfigurationiOS0_21)); }
	inline String_t* get_videoConfigurationiOS0_21() const { return ___videoConfigurationiOS0_21; }
	inline String_t** get_address_of_videoConfigurationiOS0_21() { return &___videoConfigurationiOS0_21; }
	inline void set_videoConfigurationiOS0_21(String_t* value)
	{
		___videoConfigurationiOS0_21 = value;
		Il2CppCodeGenWriteBarrier(&___videoConfigurationiOS0_21, value);
	}

	inline static int32_t get_offset_of_videoConfigurationAndroid0_22() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___videoConfigurationAndroid0_22)); }
	inline String_t* get_videoConfigurationAndroid0_22() const { return ___videoConfigurationAndroid0_22; }
	inline String_t** get_address_of_videoConfigurationAndroid0_22() { return &___videoConfigurationAndroid0_22; }
	inline void set_videoConfigurationAndroid0_22(String_t* value)
	{
		___videoConfigurationAndroid0_22 = value;
		Il2CppCodeGenWriteBarrier(&___videoConfigurationAndroid0_22, value);
	}

	inline static int32_t get_offset_of_videoConfigurationWindowsStore0_23() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___videoConfigurationWindowsStore0_23)); }
	inline String_t* get_videoConfigurationWindowsStore0_23() const { return ___videoConfigurationWindowsStore0_23; }
	inline String_t** get_address_of_videoConfigurationWindowsStore0_23() { return &___videoConfigurationWindowsStore0_23; }
	inline void set_videoConfigurationWindowsStore0_23(String_t* value)
	{
		___videoConfigurationWindowsStore0_23 = value;
		Il2CppCodeGenWriteBarrier(&___videoConfigurationWindowsStore0_23, value);
	}

	inline static int32_t get_offset_of_videoConfigurationLinux0_24() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___videoConfigurationLinux0_24)); }
	inline String_t* get_videoConfigurationLinux0_24() const { return ___videoConfigurationLinux0_24; }
	inline String_t** get_address_of_videoConfigurationLinux0_24() { return &___videoConfigurationLinux0_24; }
	inline void set_videoConfigurationLinux0_24(String_t* value)
	{
		___videoConfigurationLinux0_24 = value;
		Il2CppCodeGenWriteBarrier(&___videoConfigurationLinux0_24, value);
	}

	inline static int32_t get_offset_of_BackgroundLayer0_25() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___BackgroundLayer0_25)); }
	inline int32_t get_BackgroundLayer0_25() const { return ___BackgroundLayer0_25; }
	inline int32_t* get_address_of_BackgroundLayer0_25() { return &___BackgroundLayer0_25; }
	inline void set_BackgroundLayer0_25(int32_t value)
	{
		___BackgroundLayer0_25 = value;
	}

	inline static int32_t get_offset_of__videoWidth0_26() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____videoWidth0_26)); }
	inline int32_t get__videoWidth0_26() const { return ____videoWidth0_26; }
	inline int32_t* get_address_of__videoWidth0_26() { return &____videoWidth0_26; }
	inline void set__videoWidth0_26(int32_t value)
	{
		____videoWidth0_26 = value;
	}

	inline static int32_t get_offset_of__videoHeight0_27() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____videoHeight0_27)); }
	inline int32_t get__videoHeight0_27() const { return ____videoHeight0_27; }
	inline int32_t* get_address_of__videoHeight0_27() { return &____videoHeight0_27; }
	inline void set__videoHeight0_27(int32_t value)
	{
		____videoHeight0_27 = value;
	}

	inline static int32_t get_offset_of__videoPixelSize0_28() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____videoPixelSize0_28)); }
	inline int32_t get__videoPixelSize0_28() const { return ____videoPixelSize0_28; }
	inline int32_t* get_address_of__videoPixelSize0_28() { return &____videoPixelSize0_28; }
	inline void set__videoPixelSize0_28(int32_t value)
	{
		____videoPixelSize0_28 = value;
	}

	inline static int32_t get_offset_of__videoPixelFormatString0_29() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____videoPixelFormatString0_29)); }
	inline String_t* get__videoPixelFormatString0_29() const { return ____videoPixelFormatString0_29; }
	inline String_t** get_address_of__videoPixelFormatString0_29() { return &____videoPixelFormatString0_29; }
	inline void set__videoPixelFormatString0_29(String_t* value)
	{
		____videoPixelFormatString0_29 = value;
		Il2CppCodeGenWriteBarrier(&____videoPixelFormatString0_29, value);
	}

	inline static int32_t get_offset_of__videoProjectionMatrix0_30() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____videoProjectionMatrix0_30)); }
	inline Matrix4x4_t2933234003  get__videoProjectionMatrix0_30() const { return ____videoProjectionMatrix0_30; }
	inline Matrix4x4_t2933234003 * get_address_of__videoProjectionMatrix0_30() { return &____videoProjectionMatrix0_30; }
	inline void set__videoProjectionMatrix0_30(Matrix4x4_t2933234003  value)
	{
		____videoProjectionMatrix0_30 = value;
	}

	inline static int32_t get_offset_of__videoBackgroundMeshGO0_31() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____videoBackgroundMeshGO0_31)); }
	inline GameObject_t1756533147 * get__videoBackgroundMeshGO0_31() const { return ____videoBackgroundMeshGO0_31; }
	inline GameObject_t1756533147 ** get_address_of__videoBackgroundMeshGO0_31() { return &____videoBackgroundMeshGO0_31; }
	inline void set__videoBackgroundMeshGO0_31(GameObject_t1756533147 * value)
	{
		____videoBackgroundMeshGO0_31 = value;
		Il2CppCodeGenWriteBarrier(&____videoBackgroundMeshGO0_31, value);
	}

	inline static int32_t get_offset_of__videoColorArray0_32() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____videoColorArray0_32)); }
	inline ColorU5BU5D_t672350442* get__videoColorArray0_32() const { return ____videoColorArray0_32; }
	inline ColorU5BU5D_t672350442** get_address_of__videoColorArray0_32() { return &____videoColorArray0_32; }
	inline void set__videoColorArray0_32(ColorU5BU5D_t672350442* value)
	{
		____videoColorArray0_32 = value;
		Il2CppCodeGenWriteBarrier(&____videoColorArray0_32, value);
	}

	inline static int32_t get_offset_of__videoColor32Array0_33() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____videoColor32Array0_33)); }
	inline Color32U5BU5D_t30278651* get__videoColor32Array0_33() const { return ____videoColor32Array0_33; }
	inline Color32U5BU5D_t30278651** get_address_of__videoColor32Array0_33() { return &____videoColor32Array0_33; }
	inline void set__videoColor32Array0_33(Color32U5BU5D_t30278651* value)
	{
		____videoColor32Array0_33 = value;
		Il2CppCodeGenWriteBarrier(&____videoColor32Array0_33, value);
	}

	inline static int32_t get_offset_of__videoTexture0_34() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____videoTexture0_34)); }
	inline Texture2D_t3542995729 * get__videoTexture0_34() const { return ____videoTexture0_34; }
	inline Texture2D_t3542995729 ** get_address_of__videoTexture0_34() { return &____videoTexture0_34; }
	inline void set__videoTexture0_34(Texture2D_t3542995729 * value)
	{
		____videoTexture0_34 = value;
		Il2CppCodeGenWriteBarrier(&____videoTexture0_34, value);
	}

	inline static int32_t get_offset_of__videoMaterial0_35() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____videoMaterial0_35)); }
	inline Material_t193706927 * get__videoMaterial0_35() const { return ____videoMaterial0_35; }
	inline Material_t193706927 ** get_address_of__videoMaterial0_35() { return &____videoMaterial0_35; }
	inline void set__videoMaterial0_35(Material_t193706927 * value)
	{
		____videoMaterial0_35 = value;
		Il2CppCodeGenWriteBarrier(&____videoMaterial0_35, value);
	}

	inline static int32_t get_offset_of_VideoIsStereo_36() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___VideoIsStereo_36)); }
	inline bool get_VideoIsStereo_36() const { return ___VideoIsStereo_36; }
	inline bool* get_address_of_VideoIsStereo_36() { return &___VideoIsStereo_36; }
	inline void set_VideoIsStereo_36(bool value)
	{
		___VideoIsStereo_36 = value;
	}

	inline static int32_t get_offset_of_transL2RName_37() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___transL2RName_37)); }
	inline String_t* get_transL2RName_37() const { return ___transL2RName_37; }
	inline String_t** get_address_of_transL2RName_37() { return &___transL2RName_37; }
	inline void set_transL2RName_37(String_t* value)
	{
		___transL2RName_37 = value;
		Il2CppCodeGenWriteBarrier(&___transL2RName_37, value);
	}

	inline static int32_t get_offset_of_videoCParamName1_38() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___videoCParamName1_38)); }
	inline String_t* get_videoCParamName1_38() const { return ___videoCParamName1_38; }
	inline String_t** get_address_of_videoCParamName1_38() { return &___videoCParamName1_38; }
	inline void set_videoCParamName1_38(String_t* value)
	{
		___videoCParamName1_38 = value;
		Il2CppCodeGenWriteBarrier(&___videoCParamName1_38, value);
	}

	inline static int32_t get_offset_of_videoConfigurationWindows1_39() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___videoConfigurationWindows1_39)); }
	inline String_t* get_videoConfigurationWindows1_39() const { return ___videoConfigurationWindows1_39; }
	inline String_t** get_address_of_videoConfigurationWindows1_39() { return &___videoConfigurationWindows1_39; }
	inline void set_videoConfigurationWindows1_39(String_t* value)
	{
		___videoConfigurationWindows1_39 = value;
		Il2CppCodeGenWriteBarrier(&___videoConfigurationWindows1_39, value);
	}

	inline static int32_t get_offset_of_videoConfigurationMacOSX1_40() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___videoConfigurationMacOSX1_40)); }
	inline String_t* get_videoConfigurationMacOSX1_40() const { return ___videoConfigurationMacOSX1_40; }
	inline String_t** get_address_of_videoConfigurationMacOSX1_40() { return &___videoConfigurationMacOSX1_40; }
	inline void set_videoConfigurationMacOSX1_40(String_t* value)
	{
		___videoConfigurationMacOSX1_40 = value;
		Il2CppCodeGenWriteBarrier(&___videoConfigurationMacOSX1_40, value);
	}

	inline static int32_t get_offset_of_videoConfigurationiOS1_41() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___videoConfigurationiOS1_41)); }
	inline String_t* get_videoConfigurationiOS1_41() const { return ___videoConfigurationiOS1_41; }
	inline String_t** get_address_of_videoConfigurationiOS1_41() { return &___videoConfigurationiOS1_41; }
	inline void set_videoConfigurationiOS1_41(String_t* value)
	{
		___videoConfigurationiOS1_41 = value;
		Il2CppCodeGenWriteBarrier(&___videoConfigurationiOS1_41, value);
	}

	inline static int32_t get_offset_of_videoConfigurationAndroid1_42() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___videoConfigurationAndroid1_42)); }
	inline String_t* get_videoConfigurationAndroid1_42() const { return ___videoConfigurationAndroid1_42; }
	inline String_t** get_address_of_videoConfigurationAndroid1_42() { return &___videoConfigurationAndroid1_42; }
	inline void set_videoConfigurationAndroid1_42(String_t* value)
	{
		___videoConfigurationAndroid1_42 = value;
		Il2CppCodeGenWriteBarrier(&___videoConfigurationAndroid1_42, value);
	}

	inline static int32_t get_offset_of_videoConfigurationWindowsStore1_43() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___videoConfigurationWindowsStore1_43)); }
	inline String_t* get_videoConfigurationWindowsStore1_43() const { return ___videoConfigurationWindowsStore1_43; }
	inline String_t** get_address_of_videoConfigurationWindowsStore1_43() { return &___videoConfigurationWindowsStore1_43; }
	inline void set_videoConfigurationWindowsStore1_43(String_t* value)
	{
		___videoConfigurationWindowsStore1_43 = value;
		Il2CppCodeGenWriteBarrier(&___videoConfigurationWindowsStore1_43, value);
	}

	inline static int32_t get_offset_of_videoConfigurationLinux1_44() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___videoConfigurationLinux1_44)); }
	inline String_t* get_videoConfigurationLinux1_44() const { return ___videoConfigurationLinux1_44; }
	inline String_t** get_address_of_videoConfigurationLinux1_44() { return &___videoConfigurationLinux1_44; }
	inline void set_videoConfigurationLinux1_44(String_t* value)
	{
		___videoConfigurationLinux1_44 = value;
		Il2CppCodeGenWriteBarrier(&___videoConfigurationLinux1_44, value);
	}

	inline static int32_t get_offset_of_BackgroundLayer1_45() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___BackgroundLayer1_45)); }
	inline int32_t get_BackgroundLayer1_45() const { return ___BackgroundLayer1_45; }
	inline int32_t* get_address_of_BackgroundLayer1_45() { return &___BackgroundLayer1_45; }
	inline void set_BackgroundLayer1_45(int32_t value)
	{
		___BackgroundLayer1_45 = value;
	}

	inline static int32_t get_offset_of__videoWidth1_46() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____videoWidth1_46)); }
	inline int32_t get__videoWidth1_46() const { return ____videoWidth1_46; }
	inline int32_t* get_address_of__videoWidth1_46() { return &____videoWidth1_46; }
	inline void set__videoWidth1_46(int32_t value)
	{
		____videoWidth1_46 = value;
	}

	inline static int32_t get_offset_of__videoHeight1_47() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____videoHeight1_47)); }
	inline int32_t get__videoHeight1_47() const { return ____videoHeight1_47; }
	inline int32_t* get_address_of__videoHeight1_47() { return &____videoHeight1_47; }
	inline void set__videoHeight1_47(int32_t value)
	{
		____videoHeight1_47 = value;
	}

	inline static int32_t get_offset_of__videoPixelSize1_48() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____videoPixelSize1_48)); }
	inline int32_t get__videoPixelSize1_48() const { return ____videoPixelSize1_48; }
	inline int32_t* get_address_of__videoPixelSize1_48() { return &____videoPixelSize1_48; }
	inline void set__videoPixelSize1_48(int32_t value)
	{
		____videoPixelSize1_48 = value;
	}

	inline static int32_t get_offset_of__videoPixelFormatString1_49() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____videoPixelFormatString1_49)); }
	inline String_t* get__videoPixelFormatString1_49() const { return ____videoPixelFormatString1_49; }
	inline String_t** get_address_of__videoPixelFormatString1_49() { return &____videoPixelFormatString1_49; }
	inline void set__videoPixelFormatString1_49(String_t* value)
	{
		____videoPixelFormatString1_49 = value;
		Il2CppCodeGenWriteBarrier(&____videoPixelFormatString1_49, value);
	}

	inline static int32_t get_offset_of__videoProjectionMatrix1_50() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____videoProjectionMatrix1_50)); }
	inline Matrix4x4_t2933234003  get__videoProjectionMatrix1_50() const { return ____videoProjectionMatrix1_50; }
	inline Matrix4x4_t2933234003 * get_address_of__videoProjectionMatrix1_50() { return &____videoProjectionMatrix1_50; }
	inline void set__videoProjectionMatrix1_50(Matrix4x4_t2933234003  value)
	{
		____videoProjectionMatrix1_50 = value;
	}

	inline static int32_t get_offset_of__videoBackgroundMeshGO1_51() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____videoBackgroundMeshGO1_51)); }
	inline GameObject_t1756533147 * get__videoBackgroundMeshGO1_51() const { return ____videoBackgroundMeshGO1_51; }
	inline GameObject_t1756533147 ** get_address_of__videoBackgroundMeshGO1_51() { return &____videoBackgroundMeshGO1_51; }
	inline void set__videoBackgroundMeshGO1_51(GameObject_t1756533147 * value)
	{
		____videoBackgroundMeshGO1_51 = value;
		Il2CppCodeGenWriteBarrier(&____videoBackgroundMeshGO1_51, value);
	}

	inline static int32_t get_offset_of__videoColorArray1_52() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____videoColorArray1_52)); }
	inline ColorU5BU5D_t672350442* get__videoColorArray1_52() const { return ____videoColorArray1_52; }
	inline ColorU5BU5D_t672350442** get_address_of__videoColorArray1_52() { return &____videoColorArray1_52; }
	inline void set__videoColorArray1_52(ColorU5BU5D_t672350442* value)
	{
		____videoColorArray1_52 = value;
		Il2CppCodeGenWriteBarrier(&____videoColorArray1_52, value);
	}

	inline static int32_t get_offset_of__videoColor32Array1_53() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____videoColor32Array1_53)); }
	inline Color32U5BU5D_t30278651* get__videoColor32Array1_53() const { return ____videoColor32Array1_53; }
	inline Color32U5BU5D_t30278651** get_address_of__videoColor32Array1_53() { return &____videoColor32Array1_53; }
	inline void set__videoColor32Array1_53(Color32U5BU5D_t30278651* value)
	{
		____videoColor32Array1_53 = value;
		Il2CppCodeGenWriteBarrier(&____videoColor32Array1_53, value);
	}

	inline static int32_t get_offset_of__videoTexture1_54() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____videoTexture1_54)); }
	inline Texture2D_t3542995729 * get__videoTexture1_54() const { return ____videoTexture1_54; }
	inline Texture2D_t3542995729 ** get_address_of__videoTexture1_54() { return &____videoTexture1_54; }
	inline void set__videoTexture1_54(Texture2D_t3542995729 * value)
	{
		____videoTexture1_54 = value;
		Il2CppCodeGenWriteBarrier(&____videoTexture1_54, value);
	}

	inline static int32_t get_offset_of__videoMaterial1_55() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____videoMaterial1_55)); }
	inline Material_t193706927 * get__videoMaterial1_55() const { return ____videoMaterial1_55; }
	inline Material_t193706927 ** get_address_of__videoMaterial1_55() { return &____videoMaterial1_55; }
	inline void set__videoMaterial1_55(Material_t193706927 * value)
	{
		____videoMaterial1_55 = value;
		Il2CppCodeGenWriteBarrier(&____videoMaterial1_55, value);
	}

	inline static int32_t get_offset_of_clearCamera_56() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___clearCamera_56)); }
	inline Camera_t189460977 * get_clearCamera_56() const { return ___clearCamera_56; }
	inline Camera_t189460977 ** get_address_of_clearCamera_56() { return &___clearCamera_56; }
	inline void set_clearCamera_56(Camera_t189460977 * value)
	{
		___clearCamera_56 = value;
		Il2CppCodeGenWriteBarrier(&___clearCamera_56, value);
	}

	inline static int32_t get_offset_of__videoBackgroundCameraGO0_57() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____videoBackgroundCameraGO0_57)); }
	inline GameObject_t1756533147 * get__videoBackgroundCameraGO0_57() const { return ____videoBackgroundCameraGO0_57; }
	inline GameObject_t1756533147 ** get_address_of__videoBackgroundCameraGO0_57() { return &____videoBackgroundCameraGO0_57; }
	inline void set__videoBackgroundCameraGO0_57(GameObject_t1756533147 * value)
	{
		____videoBackgroundCameraGO0_57 = value;
		Il2CppCodeGenWriteBarrier(&____videoBackgroundCameraGO0_57, value);
	}

	inline static int32_t get_offset_of__videoBackgroundCamera0_58() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____videoBackgroundCamera0_58)); }
	inline Camera_t189460977 * get__videoBackgroundCamera0_58() const { return ____videoBackgroundCamera0_58; }
	inline Camera_t189460977 ** get_address_of__videoBackgroundCamera0_58() { return &____videoBackgroundCamera0_58; }
	inline void set__videoBackgroundCamera0_58(Camera_t189460977 * value)
	{
		____videoBackgroundCamera0_58 = value;
		Il2CppCodeGenWriteBarrier(&____videoBackgroundCamera0_58, value);
	}

	inline static int32_t get_offset_of__videoBackgroundCameraGO1_59() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____videoBackgroundCameraGO1_59)); }
	inline GameObject_t1756533147 * get__videoBackgroundCameraGO1_59() const { return ____videoBackgroundCameraGO1_59; }
	inline GameObject_t1756533147 ** get_address_of__videoBackgroundCameraGO1_59() { return &____videoBackgroundCameraGO1_59; }
	inline void set__videoBackgroundCameraGO1_59(GameObject_t1756533147 * value)
	{
		____videoBackgroundCameraGO1_59 = value;
		Il2CppCodeGenWriteBarrier(&____videoBackgroundCameraGO1_59, value);
	}

	inline static int32_t get_offset_of__videoBackgroundCamera1_60() { return static_cast<int32_t>(offsetof(ARController_t593870669, ____videoBackgroundCamera1_60)); }
	inline Camera_t189460977 * get__videoBackgroundCamera1_60() const { return ____videoBackgroundCamera1_60; }
	inline Camera_t189460977 ** get_address_of__videoBackgroundCamera1_60() { return &____videoBackgroundCamera1_60; }
	inline void set__videoBackgroundCamera1_60(Camera_t189460977 * value)
	{
		____videoBackgroundCamera1_60 = value;
		Il2CppCodeGenWriteBarrier(&____videoBackgroundCamera1_60, value);
	}

	inline static int32_t get_offset_of_NearPlane_61() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___NearPlane_61)); }
	inline float get_NearPlane_61() const { return ___NearPlane_61; }
	inline float* get_address_of_NearPlane_61() { return &___NearPlane_61; }
	inline void set_NearPlane_61(float value)
	{
		___NearPlane_61 = value;
	}

	inline static int32_t get_offset_of_FarPlane_62() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___FarPlane_62)); }
	inline float get_FarPlane_62() const { return ___FarPlane_62; }
	inline float* get_address_of_FarPlane_62() { return &___FarPlane_62; }
	inline void set_FarPlane_62(float value)
	{
		___FarPlane_62 = value;
	}

	inline static int32_t get_offset_of_ContentRotate90_63() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___ContentRotate90_63)); }
	inline bool get_ContentRotate90_63() const { return ___ContentRotate90_63; }
	inline bool* get_address_of_ContentRotate90_63() { return &___ContentRotate90_63; }
	inline void set_ContentRotate90_63(bool value)
	{
		___ContentRotate90_63 = value;
	}

	inline static int32_t get_offset_of_ContentFlipH_64() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___ContentFlipH_64)); }
	inline bool get_ContentFlipH_64() const { return ___ContentFlipH_64; }
	inline bool* get_address_of_ContentFlipH_64() { return &___ContentFlipH_64; }
	inline void set_ContentFlipH_64(bool value)
	{
		___ContentFlipH_64 = value;
	}

	inline static int32_t get_offset_of_ContentFlipV_65() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___ContentFlipV_65)); }
	inline bool get_ContentFlipV_65() const { return ___ContentFlipV_65; }
	inline bool* get_address_of_ContentFlipV_65() { return &___ContentFlipV_65; }
	inline void set_ContentFlipV_65(bool value)
	{
		___ContentFlipV_65 = value;
	}

	inline static int32_t get_offset_of_ContentAlign_66() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___ContentAlign_66)); }
	inline int32_t get_ContentAlign_66() const { return ___ContentAlign_66; }
	inline int32_t* get_address_of_ContentAlign_66() { return &___ContentAlign_66; }
	inline void set_ContentAlign_66(int32_t value)
	{
		___ContentAlign_66 = value;
	}

	inline static int32_t get_offset_of_frameCounter_68() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___frameCounter_68)); }
	inline int32_t get_frameCounter_68() const { return ___frameCounter_68; }
	inline int32_t* get_address_of_frameCounter_68() { return &___frameCounter_68; }
	inline void set_frameCounter_68(int32_t value)
	{
		___frameCounter_68 = value;
	}

	inline static int32_t get_offset_of_timeCounter_69() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___timeCounter_69)); }
	inline float get_timeCounter_69() const { return ___timeCounter_69; }
	inline float* get_address_of_timeCounter_69() { return &___timeCounter_69; }
	inline void set_timeCounter_69(float value)
	{
		___timeCounter_69 = value;
	}

	inline static int32_t get_offset_of_lastFramerate_70() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___lastFramerate_70)); }
	inline float get_lastFramerate_70() const { return ___lastFramerate_70; }
	inline float* get_address_of_lastFramerate_70() { return &___lastFramerate_70; }
	inline void set_lastFramerate_70(float value)
	{
		___lastFramerate_70 = value;
	}

	inline static int32_t get_offset_of_refreshTime_71() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___refreshTime_71)); }
	inline float get_refreshTime_71() const { return ___refreshTime_71; }
	inline float* get_address_of_refreshTime_71() { return &___refreshTime_71; }
	inline void set_refreshTime_71(float value)
	{
		___refreshTime_71 = value;
	}

	inline static int32_t get_offset_of_currentContentMode_73() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___currentContentMode_73)); }
	inline int32_t get_currentContentMode_73() const { return ___currentContentMode_73; }
	inline int32_t* get_address_of_currentContentMode_73() { return &___currentContentMode_73; }
	inline void set_currentContentMode_73(int32_t value)
	{
		___currentContentMode_73 = value;
	}

	inline static int32_t get_offset_of_currentThresholdMode_74() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___currentThresholdMode_74)); }
	inline int32_t get_currentThresholdMode_74() const { return ___currentThresholdMode_74; }
	inline int32_t* get_address_of_currentThresholdMode_74() { return &___currentThresholdMode_74; }
	inline void set_currentThresholdMode_74(int32_t value)
	{
		___currentThresholdMode_74 = value;
	}

	inline static int32_t get_offset_of_currentThreshold_75() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___currentThreshold_75)); }
	inline int32_t get_currentThreshold_75() const { return ___currentThreshold_75; }
	inline int32_t* get_address_of_currentThreshold_75() { return &___currentThreshold_75; }
	inline void set_currentThreshold_75(int32_t value)
	{
		___currentThreshold_75 = value;
	}

	inline static int32_t get_offset_of_currentLabelingMode_76() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___currentLabelingMode_76)); }
	inline int32_t get_currentLabelingMode_76() const { return ___currentLabelingMode_76; }
	inline int32_t* get_address_of_currentLabelingMode_76() { return &___currentLabelingMode_76; }
	inline void set_currentLabelingMode_76(int32_t value)
	{
		___currentLabelingMode_76 = value;
	}

	inline static int32_t get_offset_of_currentTemplateSize_77() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___currentTemplateSize_77)); }
	inline int32_t get_currentTemplateSize_77() const { return ___currentTemplateSize_77; }
	inline int32_t* get_address_of_currentTemplateSize_77() { return &___currentTemplateSize_77; }
	inline void set_currentTemplateSize_77(int32_t value)
	{
		___currentTemplateSize_77 = value;
	}

	inline static int32_t get_offset_of_currentTemplateCountMax_78() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___currentTemplateCountMax_78)); }
	inline int32_t get_currentTemplateCountMax_78() const { return ___currentTemplateCountMax_78; }
	inline int32_t* get_address_of_currentTemplateCountMax_78() { return &___currentTemplateCountMax_78; }
	inline void set_currentTemplateCountMax_78(int32_t value)
	{
		___currentTemplateCountMax_78 = value;
	}

	inline static int32_t get_offset_of_currentBorderSize_79() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___currentBorderSize_79)); }
	inline float get_currentBorderSize_79() const { return ___currentBorderSize_79; }
	inline float* get_address_of_currentBorderSize_79() { return &___currentBorderSize_79; }
	inline void set_currentBorderSize_79(float value)
	{
		___currentBorderSize_79 = value;
	}

	inline static int32_t get_offset_of_currentPatternDetectionMode_80() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___currentPatternDetectionMode_80)); }
	inline int32_t get_currentPatternDetectionMode_80() const { return ___currentPatternDetectionMode_80; }
	inline int32_t* get_address_of_currentPatternDetectionMode_80() { return &___currentPatternDetectionMode_80; }
	inline void set_currentPatternDetectionMode_80(int32_t value)
	{
		___currentPatternDetectionMode_80 = value;
	}

	inline static int32_t get_offset_of_currentMatrixCodeType_81() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___currentMatrixCodeType_81)); }
	inline int32_t get_currentMatrixCodeType_81() const { return ___currentMatrixCodeType_81; }
	inline int32_t* get_address_of_currentMatrixCodeType_81() { return &___currentMatrixCodeType_81; }
	inline void set_currentMatrixCodeType_81(int32_t value)
	{
		___currentMatrixCodeType_81 = value;
	}

	inline static int32_t get_offset_of_currentImageProcMode_82() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___currentImageProcMode_82)); }
	inline int32_t get_currentImageProcMode_82() const { return ___currentImageProcMode_82; }
	inline int32_t* get_address_of_currentImageProcMode_82() { return &___currentImageProcMode_82; }
	inline void set_currentImageProcMode_82(int32_t value)
	{
		___currentImageProcMode_82 = value;
	}

	inline static int32_t get_offset_of_currentUseVideoBackground_83() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___currentUseVideoBackground_83)); }
	inline bool get_currentUseVideoBackground_83() const { return ___currentUseVideoBackground_83; }
	inline bool* get_address_of_currentUseVideoBackground_83() { return &___currentUseVideoBackground_83; }
	inline void set_currentUseVideoBackground_83(bool value)
	{
		___currentUseVideoBackground_83 = value;
	}

	inline static int32_t get_offset_of_currentNFTMultiMode_84() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___currentNFTMultiMode_84)); }
	inline bool get_currentNFTMultiMode_84() const { return ___currentNFTMultiMode_84; }
	inline bool* get_address_of_currentNFTMultiMode_84() { return &___currentNFTMultiMode_84; }
	inline void set_currentNFTMultiMode_84(bool value)
	{
		___currentNFTMultiMode_84 = value;
	}

	inline static int32_t get_offset_of_currentLogLevel_85() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___currentLogLevel_85)); }
	inline int32_t get_currentLogLevel_85() const { return ___currentLogLevel_85; }
	inline int32_t* get_address_of_currentLogLevel_85() { return &___currentLogLevel_85; }
	inline void set_currentLogLevel_85(int32_t value)
	{
		___currentLogLevel_85 = value;
	}

	inline static int32_t get_offset_of_style_86() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___style_86)); }
	inline GUIStyleU5BU5D_t2497716199* get_style_86() const { return ___style_86; }
	inline GUIStyleU5BU5D_t2497716199** get_address_of_style_86() { return &___style_86; }
	inline void set_style_86(GUIStyleU5BU5D_t2497716199* value)
	{
		___style_86 = value;
		Il2CppCodeGenWriteBarrier(&___style_86, value);
	}

	inline static int32_t get_offset_of_guiSetup_87() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___guiSetup_87)); }
	inline bool get_guiSetup_87() const { return ___guiSetup_87; }
	inline bool* get_address_of_guiSetup_87() { return &___guiSetup_87; }
	inline void set_guiSetup_87(bool value)
	{
		___guiSetup_87 = value;
	}

	inline static int32_t get_offset_of_showGUIErrorDialog_88() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___showGUIErrorDialog_88)); }
	inline bool get_showGUIErrorDialog_88() const { return ___showGUIErrorDialog_88; }
	inline bool* get_address_of_showGUIErrorDialog_88() { return &___showGUIErrorDialog_88; }
	inline void set_showGUIErrorDialog_88(bool value)
	{
		___showGUIErrorDialog_88 = value;
	}

	inline static int32_t get_offset_of_showGUIErrorDialogContent_89() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___showGUIErrorDialogContent_89)); }
	inline String_t* get_showGUIErrorDialogContent_89() const { return ___showGUIErrorDialogContent_89; }
	inline String_t** get_address_of_showGUIErrorDialogContent_89() { return &___showGUIErrorDialogContent_89; }
	inline void set_showGUIErrorDialogContent_89(String_t* value)
	{
		___showGUIErrorDialogContent_89 = value;
		Il2CppCodeGenWriteBarrier(&___showGUIErrorDialogContent_89, value);
	}

	inline static int32_t get_offset_of_showGUIErrorDialogWinRect_90() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___showGUIErrorDialogWinRect_90)); }
	inline Rect_t3681755626  get_showGUIErrorDialogWinRect_90() const { return ___showGUIErrorDialogWinRect_90; }
	inline Rect_t3681755626 * get_address_of_showGUIErrorDialogWinRect_90() { return &___showGUIErrorDialogWinRect_90; }
	inline void set_showGUIErrorDialogWinRect_90(Rect_t3681755626  value)
	{
		___showGUIErrorDialogWinRect_90 = value;
	}

	inline static int32_t get_offset_of_showGUIDebug_91() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___showGUIDebug_91)); }
	inline bool get_showGUIDebug_91() const { return ___showGUIDebug_91; }
	inline bool* get_address_of_showGUIDebug_91() { return &___showGUIDebug_91; }
	inline void set_showGUIDebug_91(bool value)
	{
		___showGUIDebug_91 = value;
	}

	inline static int32_t get_offset_of_showGUIDebugInfo_92() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___showGUIDebugInfo_92)); }
	inline bool get_showGUIDebugInfo_92() const { return ___showGUIDebugInfo_92; }
	inline bool* get_address_of_showGUIDebugInfo_92() { return &___showGUIDebugInfo_92; }
	inline void set_showGUIDebugInfo_92(bool value)
	{
		___showGUIDebugInfo_92 = value;
	}

	inline static int32_t get_offset_of_showGUIDebugLogConsole_93() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___showGUIDebugLogConsole_93)); }
	inline bool get_showGUIDebugLogConsole_93() const { return ___showGUIDebugLogConsole_93; }
	inline bool* get_address_of_showGUIDebugLogConsole_93() { return &___showGUIDebugLogConsole_93; }
	inline void set_showGUIDebugLogConsole_93(bool value)
	{
		___showGUIDebugLogConsole_93 = value;
	}

	inline static int32_t get_offset_of_scrollPosition_94() { return static_cast<int32_t>(offsetof(ARController_t593870669, ___scrollPosition_94)); }
	inline Vector2_t2243707579  get_scrollPosition_94() const { return ___scrollPosition_94; }
	inline Vector2_t2243707579 * get_address_of_scrollPosition_94() { return &___scrollPosition_94; }
	inline void set_scrollPosition_94(Vector2_t2243707579  value)
	{
		___scrollPosition_94 = value;
	}
};

struct ARController_t593870669_StaticFields
{
public:
	// System.Action`1<System.String> ARController::<logCallback>k__BackingField
	Action_1_t1831019615 * ___U3ClogCallbackU3Ek__BackingField_3;
	// System.Collections.Generic.List`1<System.String> ARController::logMessages
	List_1_t1398341365 * ___logMessages_4;
	// System.Collections.Generic.Dictionary`2<ContentMode,System.String> ARController::ContentModeNames
	Dictionary_2_t3757846578 * ___ContentModeNames_67;
	// System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.String> ARController::ThresholdModeDescriptions
	Dictionary_2_t3314526645 * ___ThresholdModeDescriptions_72;
	// PluginFunctions/LogCallback ARController::<>f__mg$cache0
	LogCallback_t2143553514 * ___U3CU3Ef__mgU24cache0_95;
	// PluginFunctions/LogCallback ARController::<>f__mg$cache1
	LogCallback_t2143553514 * ___U3CU3Ef__mgU24cache1_96;

public:
	inline static int32_t get_offset_of_U3ClogCallbackU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ARController_t593870669_StaticFields, ___U3ClogCallbackU3Ek__BackingField_3)); }
	inline Action_1_t1831019615 * get_U3ClogCallbackU3Ek__BackingField_3() const { return ___U3ClogCallbackU3Ek__BackingField_3; }
	inline Action_1_t1831019615 ** get_address_of_U3ClogCallbackU3Ek__BackingField_3() { return &___U3ClogCallbackU3Ek__BackingField_3; }
	inline void set_U3ClogCallbackU3Ek__BackingField_3(Action_1_t1831019615 * value)
	{
		___U3ClogCallbackU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3ClogCallbackU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_logMessages_4() { return static_cast<int32_t>(offsetof(ARController_t593870669_StaticFields, ___logMessages_4)); }
	inline List_1_t1398341365 * get_logMessages_4() const { return ___logMessages_4; }
	inline List_1_t1398341365 ** get_address_of_logMessages_4() { return &___logMessages_4; }
	inline void set_logMessages_4(List_1_t1398341365 * value)
	{
		___logMessages_4 = value;
		Il2CppCodeGenWriteBarrier(&___logMessages_4, value);
	}

	inline static int32_t get_offset_of_ContentModeNames_67() { return static_cast<int32_t>(offsetof(ARController_t593870669_StaticFields, ___ContentModeNames_67)); }
	inline Dictionary_2_t3757846578 * get_ContentModeNames_67() const { return ___ContentModeNames_67; }
	inline Dictionary_2_t3757846578 ** get_address_of_ContentModeNames_67() { return &___ContentModeNames_67; }
	inline void set_ContentModeNames_67(Dictionary_2_t3757846578 * value)
	{
		___ContentModeNames_67 = value;
		Il2CppCodeGenWriteBarrier(&___ContentModeNames_67, value);
	}

	inline static int32_t get_offset_of_ThresholdModeDescriptions_72() { return static_cast<int32_t>(offsetof(ARController_t593870669_StaticFields, ___ThresholdModeDescriptions_72)); }
	inline Dictionary_2_t3314526645 * get_ThresholdModeDescriptions_72() const { return ___ThresholdModeDescriptions_72; }
	inline Dictionary_2_t3314526645 ** get_address_of_ThresholdModeDescriptions_72() { return &___ThresholdModeDescriptions_72; }
	inline void set_ThresholdModeDescriptions_72(Dictionary_2_t3314526645 * value)
	{
		___ThresholdModeDescriptions_72 = value;
		Il2CppCodeGenWriteBarrier(&___ThresholdModeDescriptions_72, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_95() { return static_cast<int32_t>(offsetof(ARController_t593870669_StaticFields, ___U3CU3Ef__mgU24cache0_95)); }
	inline LogCallback_t2143553514 * get_U3CU3Ef__mgU24cache0_95() const { return ___U3CU3Ef__mgU24cache0_95; }
	inline LogCallback_t2143553514 ** get_address_of_U3CU3Ef__mgU24cache0_95() { return &___U3CU3Ef__mgU24cache0_95; }
	inline void set_U3CU3Ef__mgU24cache0_95(LogCallback_t2143553514 * value)
	{
		___U3CU3Ef__mgU24cache0_95 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache0_95, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_96() { return static_cast<int32_t>(offsetof(ARController_t593870669_StaticFields, ___U3CU3Ef__mgU24cache1_96)); }
	inline LogCallback_t2143553514 * get_U3CU3Ef__mgU24cache1_96() const { return ___U3CU3Ef__mgU24cache1_96; }
	inline LogCallback_t2143553514 ** get_address_of_U3CU3Ef__mgU24cache1_96() { return &___U3CU3Ef__mgU24cache1_96; }
	inline void set_U3CU3Ef__mgU24cache1_96(LogCallback_t2143553514 * value)
	{
		___U3CU3Ef__mgU24cache1_96 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__mgU24cache1_96, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
