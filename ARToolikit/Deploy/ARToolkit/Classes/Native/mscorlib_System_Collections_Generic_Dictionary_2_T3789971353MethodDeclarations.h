﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T1467252219MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<ContentMode,System.String,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m3523396173(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t3789971353 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m4089255057_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<ContentMode,System.String,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m2969784169(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t3789971353 *, int32_t, String_t*, const MethodInfo*))Transform_1_Invoke_m3172528453_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<ContentMode,System.String,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m2336881270(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t3789971353 *, int32_t, String_t*, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m1232101724_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<ContentMode,System.String,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m3846832775(__this, ___result0, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t3789971353 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m3609252291_gshared)(__this, ___result0, method)
