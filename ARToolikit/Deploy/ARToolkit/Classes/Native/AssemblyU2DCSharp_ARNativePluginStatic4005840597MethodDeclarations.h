﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginFunctions/LogCallback
struct LogCallback_t2143553514;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Single[]
struct SingleU5BU5D_t577127397;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PluginFunctions_LogCallback2143553514.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void ARNativePluginStatic::aruRequestCamera()
extern "C"  void ARNativePluginStatic_aruRequestCamera_m4285056894 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePluginStatic::arwRegisterLogCallback(PluginFunctions/LogCallback)
extern "C"  void ARNativePluginStatic_arwRegisterLogCallback_m2696023928 (Il2CppObject * __this /* static, unused */, LogCallback_t2143553514 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePluginStatic::arwSetLogLevel(System.Int32)
extern "C"  void ARNativePluginStatic_arwSetLogLevel_m3738585053 (Il2CppObject * __this /* static, unused */, int32_t ___logLevel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePluginStatic::arwInitialiseAR()
extern "C"  bool ARNativePluginStatic_arwInitialiseAR_m3292125414 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePluginStatic::arwInitialiseARWithOptions(System.Int32,System.Int32)
extern "C"  bool ARNativePluginStatic_arwInitialiseARWithOptions_m4273179010 (Il2CppObject * __this /* static, unused */, int32_t ___pattSize0, int32_t ___pattCountMax1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePluginStatic::arwGetARToolKitVersion(System.Text.StringBuilder,System.Int32)
extern "C"  bool ARNativePluginStatic_arwGetARToolKitVersion_m3283193640 (Il2CppObject * __this /* static, unused */, StringBuilder_t1221177846 * ___buffer0, int32_t ___length1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARNativePluginStatic::arwGetError()
extern "C"  int32_t ARNativePluginStatic_arwGetError_m1131212540 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePluginStatic::arwShutdownAR()
extern "C"  bool ARNativePluginStatic_arwShutdownAR_m3238479991 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePluginStatic::arwStartRunningB(System.String,System.Byte[],System.Int32,System.Single,System.Single)
extern "C"  bool ARNativePluginStatic_arwStartRunningB_m1273829661 (Il2CppObject * __this /* static, unused */, String_t* ___vconf0, ByteU5BU5D_t3397334013* ___cparaBuff1, int32_t ___cparaBuffLen2, float ___nearPlane3, float ___farPlane4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePluginStatic::arwStartRunningStereoB(System.String,System.Byte[],System.Int32,System.String,System.Byte[],System.Int32,System.Byte[],System.Int32,System.Single,System.Single)
extern "C"  bool ARNativePluginStatic_arwStartRunningStereoB_m4213855497 (Il2CppObject * __this /* static, unused */, String_t* ___vconfL0, ByteU5BU5D_t3397334013* ___cparaBuffL1, int32_t ___cparaBuffLenL2, String_t* ___vconfR3, ByteU5BU5D_t3397334013* ___cparaBuffR4, int32_t ___cparaBuffLenR5, ByteU5BU5D_t3397334013* ___transL2RBuff6, int32_t ___transL2RBuffLen7, float ___nearPlane8, float ___farPlane9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePluginStatic::arwIsRunning()
extern "C"  bool ARNativePluginStatic_arwIsRunning_m3525939861 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePluginStatic::arwStopRunning()
extern "C"  bool ARNativePluginStatic_arwStopRunning_m3225889991 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePluginStatic::arwGetProjectionMatrix(System.Single[])
extern "C"  bool ARNativePluginStatic_arwGetProjectionMatrix_m1707584415 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___matrix0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePluginStatic::arwGetProjectionMatrixStereo(System.Single[],System.Single[])
extern "C"  bool ARNativePluginStatic_arwGetProjectionMatrixStereo_m1572723186 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___matrixL0, SingleU5BU5D_t577127397* ___matrixR1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePluginStatic::arwGetVideoParams(System.Int32&,System.Int32&,System.Int32&,System.Text.StringBuilder,System.Int32)
extern "C"  bool ARNativePluginStatic_arwGetVideoParams_m804941317 (Il2CppObject * __this /* static, unused */, int32_t* ___width0, int32_t* ___height1, int32_t* ___pixelSize2, StringBuilder_t1221177846 * ___pixelFormatBuffer3, int32_t ___pixelFormatBufferLen4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePluginStatic::arwGetVideoParamsStereo(System.Int32&,System.Int32&,System.Int32&,System.Text.StringBuilder,System.Int32,System.Int32&,System.Int32&,System.Int32&,System.Text.StringBuilder,System.Int32)
extern "C"  bool ARNativePluginStatic_arwGetVideoParamsStereo_m3904472601 (Il2CppObject * __this /* static, unused */, int32_t* ___widthL0, int32_t* ___heightL1, int32_t* ___pixelSizeL2, StringBuilder_t1221177846 * ___pixelFormatBufferL3, int32_t ___pixelFormatBufferLenL4, int32_t* ___widthR5, int32_t* ___heightR6, int32_t* ___pixelSizeR7, StringBuilder_t1221177846 * ___pixelFormatBufferR8, int32_t ___pixelFormatBufferLenR9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePluginStatic::arwCapture()
extern "C"  bool ARNativePluginStatic_arwCapture_m3910463490 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePluginStatic::arwUpdateAR()
extern "C"  bool ARNativePluginStatic_arwUpdateAR_m1910658788 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePluginStatic::arwUpdateTexture(System.IntPtr)
extern "C"  bool ARNativePluginStatic_arwUpdateTexture_m1158379942 (Il2CppObject * __this /* static, unused */, IntPtr_t ___colors0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePluginStatic::arwUpdateTextureStereo(System.IntPtr,System.IntPtr)
extern "C"  bool ARNativePluginStatic_arwUpdateTextureStereo_m693222146 (Il2CppObject * __this /* static, unused */, IntPtr_t ___colorsL0, IntPtr_t ___colorsR1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePluginStatic::arwUpdateTexture32(System.IntPtr)
extern "C"  bool ARNativePluginStatic_arwUpdateTexture32_m185359189 (Il2CppObject * __this /* static, unused */, IntPtr_t ___colors320, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePluginStatic::arwUpdateTexture32Stereo(System.IntPtr,System.IntPtr)
extern "C"  bool ARNativePluginStatic_arwUpdateTexture32Stereo_m813074921 (Il2CppObject * __this /* static, unused */, IntPtr_t ___colors32L0, IntPtr_t ___colors32R1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePluginStatic::arwUpdateTextureGL(System.Int32)
extern "C"  bool ARNativePluginStatic_arwUpdateTextureGL_m562093018 (Il2CppObject * __this /* static, unused */, int32_t ___textureID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePluginStatic::arwUpdateTextureGLStereo(System.Int32,System.Int32)
extern "C"  bool ARNativePluginStatic_arwUpdateTextureGLStereo_m3176194339 (Il2CppObject * __this /* static, unused */, int32_t ___textureID_L0, int32_t ___textureID_R1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePluginStatic::arwSetUnityRenderEventUpdateTextureGLTextureID(System.Int32)
extern "C"  void ARNativePluginStatic_arwSetUnityRenderEventUpdateTextureGLTextureID_m3208451437 (Il2CppObject * __this /* static, unused */, int32_t ___textureID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePluginStatic::arwSetUnityRenderEventUpdateTextureGLStereoTextureIDs(System.Int32,System.Int32)
extern "C"  void ARNativePluginStatic_arwSetUnityRenderEventUpdateTextureGLStereoTextureIDs_m2332439057 (Il2CppObject * __this /* static, unused */, int32_t ___textureID_L0, int32_t ___textureID_R1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARNativePluginStatic::arwGetMarkerPatternCount(System.Int32)
extern "C"  int32_t ARNativePluginStatic_arwGetMarkerPatternCount_m2534201878 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePluginStatic::arwGetMarkerPatternConfig(System.Int32,System.Int32,System.Single[],System.Single&,System.Single&,System.Int32&,System.Int32&)
extern "C"  bool ARNativePluginStatic_arwGetMarkerPatternConfig_m978360953 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___patternID1, SingleU5BU5D_t577127397* ___matrix2, float* ___width3, float* ___height4, int32_t* ___imageSizeX5, int32_t* ___imageSizeY6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePluginStatic::arwGetMarkerPatternImage(System.Int32,System.Int32,UnityEngine.Color[])
extern "C"  bool ARNativePluginStatic_arwGetMarkerPatternImage_m2239266479 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___patternID1, ColorU5BU5D_t672350442* ___colors2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePluginStatic::arwGetMarkerOptionBool(System.Int32,System.Int32)
extern "C"  bool ARNativePluginStatic_arwGetMarkerOptionBool_m535783117 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePluginStatic::arwSetMarkerOptionBool(System.Int32,System.Int32,System.Boolean)
extern "C"  void ARNativePluginStatic_arwSetMarkerOptionBool_m2682973062 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, bool ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARNativePluginStatic::arwGetMarkerOptionInt(System.Int32,System.Int32)
extern "C"  int32_t ARNativePluginStatic_arwGetMarkerOptionInt_m4131469642 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePluginStatic::arwSetMarkerOptionInt(System.Int32,System.Int32,System.Int32)
extern "C"  void ARNativePluginStatic_arwSetMarkerOptionInt_m4268478505 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, int32_t ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ARNativePluginStatic::arwGetMarkerOptionFloat(System.Int32,System.Int32)
extern "C"  float ARNativePluginStatic_arwGetMarkerOptionFloat_m2202747965 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePluginStatic::arwSetMarkerOptionFloat(System.Int32,System.Int32,System.Single)
extern "C"  void ARNativePluginStatic_arwSetMarkerOptionFloat_m591864046 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, int32_t ___option1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePluginStatic::arwSetVideoDebugMode(System.Boolean)
extern "C"  void ARNativePluginStatic_arwSetVideoDebugMode_m2725814322 (Il2CppObject * __this /* static, unused */, bool ___debug0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePluginStatic::arwGetVideoDebugMode()
extern "C"  bool ARNativePluginStatic_arwGetVideoDebugMode_m4059797661 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePluginStatic::arwSetVideoThreshold(System.Int32)
extern "C"  void ARNativePluginStatic_arwSetVideoThreshold_m2572009209 (Il2CppObject * __this /* static, unused */, int32_t ___threshold0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARNativePluginStatic::arwGetVideoThreshold()
extern "C"  int32_t ARNativePluginStatic_arwGetVideoThreshold_m2851652890 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePluginStatic::arwSetVideoThresholdMode(System.Int32)
extern "C"  void ARNativePluginStatic_arwSetVideoThresholdMode_m2501069538 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARNativePluginStatic::arwGetVideoThresholdMode()
extern "C"  int32_t ARNativePluginStatic_arwGetVideoThresholdMode_m3923466573 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePluginStatic::arwSetLabelingMode(System.Int32)
extern "C"  void ARNativePluginStatic_arwSetLabelingMode_m1320145026 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARNativePluginStatic::arwGetLabelingMode()
extern "C"  int32_t ARNativePluginStatic_arwGetLabelingMode_m2403718909 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePluginStatic::arwSetBorderSize(System.Single)
extern "C"  void ARNativePluginStatic_arwSetBorderSize_m3985598462 (Il2CppObject * __this /* static, unused */, float ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ARNativePluginStatic::arwGetBorderSize()
extern "C"  float ARNativePluginStatic_arwGetBorderSize_m402710309 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePluginStatic::arwSetPatternDetectionMode(System.Int32)
extern "C"  void ARNativePluginStatic_arwSetPatternDetectionMode_m981787463 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARNativePluginStatic::arwGetPatternDetectionMode()
extern "C"  int32_t ARNativePluginStatic_arwGetPatternDetectionMode_m2165003022 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePluginStatic::arwSetMatrixCodeType(System.Int32)
extern "C"  void ARNativePluginStatic_arwSetMatrixCodeType_m1755799257 (Il2CppObject * __this /* static, unused */, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARNativePluginStatic::arwGetMatrixCodeType()
extern "C"  int32_t ARNativePluginStatic_arwGetMatrixCodeType_m1128356520 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePluginStatic::arwSetImageProcMode(System.Int32)
extern "C"  void ARNativePluginStatic_arwSetImageProcMode_m1469690497 (Il2CppObject * __this /* static, unused */, int32_t ___mode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARNativePluginStatic::arwGetImageProcMode()
extern "C"  int32_t ARNativePluginStatic_arwGetImageProcMode_m855251164 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARNativePluginStatic::arwSetNFTMultiMode(System.Boolean)
extern "C"  void ARNativePluginStatic_arwSetNFTMultiMode_m4123606051 (Il2CppObject * __this /* static, unused */, bool ___on0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePluginStatic::arwGetNFTMultiMode()
extern "C"  bool ARNativePluginStatic_arwGetNFTMultiMode_m44684326 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARNativePluginStatic::arwAddMarker(System.String)
extern "C"  int32_t ARNativePluginStatic_arwAddMarker_m4198366323 (Il2CppObject * __this /* static, unused */, String_t* ___cfg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePluginStatic::arwRemoveMarker(System.Int32)
extern "C"  bool ARNativePluginStatic_arwRemoveMarker_m3792023969 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ARNativePluginStatic::arwRemoveAllMarkers()
extern "C"  int32_t ARNativePluginStatic_arwRemoveAllMarkers_m1950709840 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePluginStatic::arwQueryMarkerVisibility(System.Int32)
extern "C"  bool ARNativePluginStatic_arwQueryMarkerVisibility_m3506306455 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePluginStatic::arwQueryMarkerTransformation(System.Int32,System.Single[])
extern "C"  bool ARNativePluginStatic_arwQueryMarkerTransformation_m1782821311 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, SingleU5BU5D_t577127397* ___matrix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePluginStatic::arwQueryMarkerTransformationStereo(System.Int32,System.Single[],System.Single[])
extern "C"  bool ARNativePluginStatic_arwQueryMarkerTransformationStereo_m2434214006 (Il2CppObject * __this /* static, unused */, int32_t ___markerID0, SingleU5BU5D_t577127397* ___matrixL1, SingleU5BU5D_t577127397* ___matrixR2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARNativePluginStatic::arwLoadOpticalParams(System.String,System.Byte[],System.Int32,System.Single&,System.Single&,System.Single[],System.Single[])
extern "C"  bool ARNativePluginStatic_arwLoadOpticalParams_m1348665490 (Il2CppObject * __this /* static, unused */, String_t* ___optical_param_name0, ByteU5BU5D_t3397334013* ___optical_param_buff1, int32_t ___optical_param_buffLen2, float* ___fovy_p3, float* ___aspect_p4, SingleU5BU5D_t577127397* ___m5, SingleU5BU5D_t577127397* ___p6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
