﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Collections_Generic_EqualityCompare735216058.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<ARController/ARToolKitThresholdMode>
struct  DefaultComparer_t3783510948  : public EqualityComparer_1_t735216058
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
