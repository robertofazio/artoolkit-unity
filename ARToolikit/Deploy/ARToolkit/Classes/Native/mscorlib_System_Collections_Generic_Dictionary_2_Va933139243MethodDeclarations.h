﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<MarkerType,System.Object>
struct ValueCollection_t933139243;
// System.Collections.Generic.Dictionary`2<MarkerType,System.Object>
struct Dictionary_2_t2230079400;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3916612164.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MarkerType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m3048576706_gshared (ValueCollection_t933139243 * __this, Dictionary_2_t2230079400 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m3048576706(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t933139243 *, Dictionary_2_t2230079400 *, const MethodInfo*))ValueCollection__ctor_m3048576706_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MarkerType,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2539228508_gshared (ValueCollection_t933139243 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2539228508(__this, ___item0, method) ((  void (*) (ValueCollection_t933139243 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2539228508_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MarkerType,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2201883495_gshared (ValueCollection_t933139243 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2201883495(__this, method) ((  void (*) (ValueCollection_t933139243 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2201883495_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<MarkerType,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3607159516_gshared (ValueCollection_t933139243 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3607159516(__this, ___item0, method) ((  bool (*) (ValueCollection_t933139243 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3607159516_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<MarkerType,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m956745599_gshared (ValueCollection_t933139243 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m956745599(__this, ___item0, method) ((  bool (*) (ValueCollection_t933139243 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m956745599_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<MarkerType,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1168036017_gshared (ValueCollection_t933139243 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1168036017(__this, method) ((  Il2CppObject* (*) (ValueCollection_t933139243 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1168036017_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MarkerType,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m385822401_gshared (ValueCollection_t933139243 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m385822401(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t933139243 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m385822401_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<MarkerType,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2955445344_gshared (ValueCollection_t933139243 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2955445344(__this, method) ((  Il2CppObject * (*) (ValueCollection_t933139243 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2955445344_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<MarkerType,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m462705435_gshared (ValueCollection_t933139243 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m462705435(__this, method) ((  bool (*) (ValueCollection_t933139243 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m462705435_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<MarkerType,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2465222925_gshared (ValueCollection_t933139243 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2465222925(__this, method) ((  bool (*) (ValueCollection_t933139243 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2465222925_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<MarkerType,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m4289216821_gshared (ValueCollection_t933139243 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m4289216821(__this, method) ((  Il2CppObject * (*) (ValueCollection_t933139243 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m4289216821_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MarkerType,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m276100111_gshared (ValueCollection_t933139243 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m276100111(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t933139243 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m276100111_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<MarkerType,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3916612164  ValueCollection_GetEnumerator_m2979033658_gshared (ValueCollection_t933139243 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m2979033658(__this, method) ((  Enumerator_t3916612164  (*) (ValueCollection_t933139243 *, const MethodInfo*))ValueCollection_GetEnumerator_m2979033658_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<MarkerType,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m746136433_gshared (ValueCollection_t933139243 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m746136433(__this, method) ((  int32_t (*) (ValueCollection_t933139243 *, const MethodInfo*))ValueCollection_get_Count_m746136433_gshared)(__this, method)
