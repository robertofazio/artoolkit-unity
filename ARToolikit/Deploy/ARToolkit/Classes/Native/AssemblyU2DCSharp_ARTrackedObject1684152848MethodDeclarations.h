﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARTrackedObject
struct ARTrackedObject_t1684152848;
// System.String
struct String_t;
// ARMarker
struct ARMarker_t1554260723;
// AROrigin
struct AROrigin_t3335349585;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void ARTrackedObject::.ctor()
extern "C"  void ARTrackedObject__ctor_m2314920655 (ARTrackedObject_t1684152848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ARTrackedObject::get_MarkerTag()
extern "C"  String_t* ARTrackedObject_get_MarkerTag_m2984654219 (ARTrackedObject_t1684152848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARTrackedObject::set_MarkerTag(System.String)
extern "C"  void ARTrackedObject_set_MarkerTag_m3948804028 (ARTrackedObject_t1684152848 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ARMarker ARTrackedObject::GetMarker()
extern "C"  ARMarker_t1554260723 * ARTrackedObject_GetMarker_m343782273 (ARTrackedObject_t1684152848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AROrigin ARTrackedObject::GetOrigin()
extern "C"  AROrigin_t3335349585 * ARTrackedObject_GetOrigin_m1492473453 (ARTrackedObject_t1684152848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARTrackedObject::Start()
extern "C"  void ARTrackedObject_Start_m1585862763 (ARTrackedObject_t1684152848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARTrackedObject::LateUpdate()
extern "C"  void ARTrackedObject_LateUpdate_m3261816006 (ARTrackedObject_t1684152848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
