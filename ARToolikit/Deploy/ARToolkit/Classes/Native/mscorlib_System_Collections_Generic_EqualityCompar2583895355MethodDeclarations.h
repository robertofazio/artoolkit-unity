﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<MarkerType>
struct DefaultComparer_t2583895355;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MarkerType961965194.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<MarkerType>::.ctor()
extern "C"  void DefaultComparer__ctor_m2068413746_gshared (DefaultComparer_t2583895355 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2068413746(__this, method) ((  void (*) (DefaultComparer_t2583895355 *, const MethodInfo*))DefaultComparer__ctor_m2068413746_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<MarkerType>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2734874969_gshared (DefaultComparer_t2583895355 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2734874969(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2583895355 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m2734874969_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<MarkerType>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m3249162677_gshared (DefaultComparer_t2583895355 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m3249162677(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2583895355 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m3249162677_gshared)(__this, ___x0, ___y1, method)
