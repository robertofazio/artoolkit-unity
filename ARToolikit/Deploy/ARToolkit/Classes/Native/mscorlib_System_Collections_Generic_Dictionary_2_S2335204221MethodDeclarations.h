﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<MarkerType,System.Object>
struct ShimEnumerator_t2335204221;
// System.Collections.Generic.Dictionary`2<MarkerType,System.Object>
struct Dictionary_2_t2230079400;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<MarkerType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m3276796494_gshared (ShimEnumerator_t2335204221 * __this, Dictionary_2_t2230079400 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m3276796494(__this, ___host0, method) ((  void (*) (ShimEnumerator_t2335204221 *, Dictionary_2_t2230079400 *, const MethodInfo*))ShimEnumerator__ctor_m3276796494_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<MarkerType,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2213914541_gshared (ShimEnumerator_t2335204221 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m2213914541(__this, method) ((  bool (*) (ShimEnumerator_t2335204221 *, const MethodInfo*))ShimEnumerator_MoveNext_m2213914541_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<MarkerType,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m2588510689_gshared (ShimEnumerator_t2335204221 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m2588510689(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t2335204221 *, const MethodInfo*))ShimEnumerator_get_Entry_m2588510689_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<MarkerType,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2939037670_gshared (ShimEnumerator_t2335204221 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m2939037670(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2335204221 *, const MethodInfo*))ShimEnumerator_get_Key_m2939037670_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<MarkerType,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2266640668_gshared (ShimEnumerator_t2335204221 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m2266640668(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2335204221 *, const MethodInfo*))ShimEnumerator_get_Value_m2266640668_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<MarkerType,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1935885916_gshared (ShimEnumerator_t2335204221 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1935885916(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2335204221 *, const MethodInfo*))ShimEnumerator_get_Current_m1935885916_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<MarkerType,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m3750688792_gshared (ShimEnumerator_t2335204221 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m3750688792(__this, method) ((  void (*) (ShimEnumerator_t2335204221 *, const MethodInfo*))ShimEnumerator_Reset_m3750688792_gshared)(__this, method)
