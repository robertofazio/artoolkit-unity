﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<ContentMode,System.Object>
struct ShimEnumerator_t228233165;
// System.Collections.Generic.Dictionary`2<ContentMode,System.Object>
struct Dictionary_2_t123108344;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ContentMode,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m3545435970_gshared (ShimEnumerator_t228233165 * __this, Dictionary_2_t123108344 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m3545435970(__this, ___host0, method) ((  void (*) (ShimEnumerator_t228233165 *, Dictionary_2_t123108344 *, const MethodInfo*))ShimEnumerator__ctor_m3545435970_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<ContentMode,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m677860581_gshared (ShimEnumerator_t228233165 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m677860581(__this, method) ((  bool (*) (ShimEnumerator_t228233165 *, const MethodInfo*))ShimEnumerator_MoveNext_m677860581_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<ContentMode,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m1302996289_gshared (ShimEnumerator_t228233165 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m1302996289(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t228233165 *, const MethodInfo*))ShimEnumerator_get_Entry_m1302996289_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ContentMode,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m539852012_gshared (ShimEnumerator_t228233165 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m539852012(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t228233165 *, const MethodInfo*))ShimEnumerator_get_Key_m539852012_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ContentMode,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2094777198_gshared (ShimEnumerator_t228233165 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m2094777198(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t228233165 *, const MethodInfo*))ShimEnumerator_get_Value_m2094777198_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ContentMode,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2723035478_gshared (ShimEnumerator_t228233165 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m2723035478(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t228233165 *, const MethodInfo*))ShimEnumerator_get_Current_m2723035478_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ContentMode,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m2170141832_gshared (ShimEnumerator_t228233165 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m2170141832(__this, method) ((  void (*) (ShimEnumerator_t228233165 *, const MethodInfo*))ShimEnumerator_Reset_m2170141832_gshared)(__this, method)
