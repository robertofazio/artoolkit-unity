﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3020333049.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_ARController_ARToolKitThresholdM2161580787.h"

// System.Void System.Array/InternalEnumerator`1<ARController/ARToolKitThresholdMode>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m858549430_gshared (InternalEnumerator_1_t3020333049 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m858549430(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3020333049 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m858549430_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<ARController/ARToolKitThresholdMode>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1970642198_gshared (InternalEnumerator_1_t3020333049 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1970642198(__this, method) ((  void (*) (InternalEnumerator_1_t3020333049 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1970642198_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<ARController/ARToolKitThresholdMode>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3488191080_gshared (InternalEnumerator_1_t3020333049 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3488191080(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3020333049 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3488191080_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<ARController/ARToolKitThresholdMode>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1989802747_gshared (InternalEnumerator_1_t3020333049 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1989802747(__this, method) ((  void (*) (InternalEnumerator_1_t3020333049 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1989802747_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<ARController/ARToolKitThresholdMode>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3789848238_gshared (InternalEnumerator_1_t3020333049 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3789848238(__this, method) ((  bool (*) (InternalEnumerator_1_t3020333049 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3789848238_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<ARController/ARToolKitThresholdMode>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m928632463_gshared (InternalEnumerator_1_t3020333049 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m928632463(__this, method) ((  int32_t (*) (InternalEnumerator_1_t3020333049 *, const MethodInfo*))InternalEnumerator_1_get_Current_m928632463_gshared)(__this, method)
