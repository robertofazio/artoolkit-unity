﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARPattern
struct ARPattern_t3906840165;

#include "codegen/il2cpp-codegen.h"

// System.Void ARPattern::.ctor(System.Int32,System.Int32)
extern "C"  void ARPattern__ctor_m3048001238 (ARPattern_t3906840165 * __this, int32_t ___markerID0, int32_t ___patternID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
