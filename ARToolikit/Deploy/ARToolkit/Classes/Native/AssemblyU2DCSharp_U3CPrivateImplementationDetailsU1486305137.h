﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3762068664.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305143  : public Il2CppObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-7CDC43E92004F6C21BFB0E695E5C42C85155527E
	U24ArrayTypeU3D24_t762068664  ___U24fieldU2D7CDC43E92004F6C21BFB0E695E5C42C85155527E_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D7CDC43E92004F6C21BFB0E695E5C42C85155527E_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields, ___U24fieldU2D7CDC43E92004F6C21BFB0E695E5C42C85155527E_0)); }
	inline U24ArrayTypeU3D24_t762068664  get_U24fieldU2D7CDC43E92004F6C21BFB0E695E5C42C85155527E_0() const { return ___U24fieldU2D7CDC43E92004F6C21BFB0E695E5C42C85155527E_0; }
	inline U24ArrayTypeU3D24_t762068664 * get_address_of_U24fieldU2D7CDC43E92004F6C21BFB0E695E5C42C85155527E_0() { return &___U24fieldU2D7CDC43E92004F6C21BFB0E695E5C42C85155527E_0; }
	inline void set_U24fieldU2D7CDC43E92004F6C21BFB0E695E5C42C85155527E_0(U24ArrayTypeU3D24_t762068664  value)
	{
		___U24fieldU2D7CDC43E92004F6C21BFB0E695E5C42C85155527E_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
