﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Object>
struct U3CCreateReverseIteratorU3Ec__IteratorF_1_t2747922618;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Object>::.ctor()
extern "C"  void U3CCreateReverseIteratorU3Ec__IteratorF_1__ctor_m761817807_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t2747922618 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1__ctor_m761817807(__this, method) ((  void (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t2747922618 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1__ctor_m761817807_gshared)(__this, method)
// TSource System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  Il2CppObject * U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m1798043730_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t2747922618 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m1798043730(__this, method) ((  Il2CppObject * (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t2747922618 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m1798043730_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_IEnumerator_get_Current_m58256953_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t2747922618 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_IEnumerator_get_Current_m58256953(__this, method) ((  Il2CppObject * (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t2747922618 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_IEnumerator_get_Current_m58256953_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_IEnumerable_GetEnumerator_m1024292074_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t2747922618 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_IEnumerable_GetEnumerator_m1024292074(__this, method) ((  Il2CppObject * (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t2747922618 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_IEnumerable_GetEnumerator_m1024292074_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m1488125331_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t2747922618 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m1488125331(__this, method) ((  Il2CppObject* (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t2747922618 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m1488125331_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Object>::MoveNext()
extern "C"  bool U3CCreateReverseIteratorU3Ec__IteratorF_1_MoveNext_m3725727129_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t2747922618 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1_MoveNext_m3725727129(__this, method) ((  bool (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t2747922618 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1_MoveNext_m3725727129_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Object>::Dispose()
extern "C"  void U3CCreateReverseIteratorU3Ec__IteratorF_1_Dispose_m2793227986_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t2747922618 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1_Dispose_m2793227986(__this, method) ((  void (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t2747922618 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1_Dispose_m2793227986_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateReverseIterator>c__IteratorF`1<System.Object>::Reset()
extern "C"  void U3CCreateReverseIteratorU3Ec__IteratorF_1_Reset_m564646036_gshared (U3CCreateReverseIteratorU3Ec__IteratorF_1_t2747922618 * __this, const MethodInfo* method);
#define U3CCreateReverseIteratorU3Ec__IteratorF_1_Reset_m564646036(__this, method) ((  void (*) (U3CCreateReverseIteratorU3Ec__IteratorF_1_t2747922618 *, const MethodInfo*))U3CCreateReverseIteratorU3Ec__IteratorF_1_Reset_m564646036_gshared)(__this, method)
