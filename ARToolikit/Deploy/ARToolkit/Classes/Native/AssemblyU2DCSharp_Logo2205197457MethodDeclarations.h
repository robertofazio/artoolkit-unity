﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Logo
struct Logo_t2205197457;

#include "codegen/il2cpp-codegen.h"

// System.Void Logo::.ctor()
extern "C"  void Logo__ctor_m2042197794 (Logo_t2205197457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Logo::Start()
extern "C"  void Logo_Start_m1689820146 (Logo_t2205197457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Logo::Update()
extern "C"  void Logo_Update_m4152945649 (Logo_t2205197457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Logo::Reset()
extern "C"  void Logo_Reset_m359214433 (Logo_t2205197457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
