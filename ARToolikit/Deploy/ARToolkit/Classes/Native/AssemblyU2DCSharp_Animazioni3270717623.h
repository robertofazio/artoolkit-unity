﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Animazioni
struct  Animazioni_t3270717623  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Animazioni::animationWin
	GameObject_t1756533147 * ___animationWin_2;
	// UnityEngine.GameObject Animazioni::animationNotWin
	GameObject_t1756533147 * ___animationNotWin_3;
	// UnityEngine.GameObject Animazioni::fireworks
	GameObject_t1756533147 * ___fireworks_4;
	// UnityEngine.GameObject Animazioni::win
	GameObject_t1756533147 * ___win_5;
	// UnityEngine.GameObject Animazioni::notWin
	GameObject_t1756533147 * ___notWin_6;
	// UnityEngine.UI.Text Animazioni::info
	Text_t356221433 * ___info_7;

public:
	inline static int32_t get_offset_of_animationWin_2() { return static_cast<int32_t>(offsetof(Animazioni_t3270717623, ___animationWin_2)); }
	inline GameObject_t1756533147 * get_animationWin_2() const { return ___animationWin_2; }
	inline GameObject_t1756533147 ** get_address_of_animationWin_2() { return &___animationWin_2; }
	inline void set_animationWin_2(GameObject_t1756533147 * value)
	{
		___animationWin_2 = value;
		Il2CppCodeGenWriteBarrier(&___animationWin_2, value);
	}

	inline static int32_t get_offset_of_animationNotWin_3() { return static_cast<int32_t>(offsetof(Animazioni_t3270717623, ___animationNotWin_3)); }
	inline GameObject_t1756533147 * get_animationNotWin_3() const { return ___animationNotWin_3; }
	inline GameObject_t1756533147 ** get_address_of_animationNotWin_3() { return &___animationNotWin_3; }
	inline void set_animationNotWin_3(GameObject_t1756533147 * value)
	{
		___animationNotWin_3 = value;
		Il2CppCodeGenWriteBarrier(&___animationNotWin_3, value);
	}

	inline static int32_t get_offset_of_fireworks_4() { return static_cast<int32_t>(offsetof(Animazioni_t3270717623, ___fireworks_4)); }
	inline GameObject_t1756533147 * get_fireworks_4() const { return ___fireworks_4; }
	inline GameObject_t1756533147 ** get_address_of_fireworks_4() { return &___fireworks_4; }
	inline void set_fireworks_4(GameObject_t1756533147 * value)
	{
		___fireworks_4 = value;
		Il2CppCodeGenWriteBarrier(&___fireworks_4, value);
	}

	inline static int32_t get_offset_of_win_5() { return static_cast<int32_t>(offsetof(Animazioni_t3270717623, ___win_5)); }
	inline GameObject_t1756533147 * get_win_5() const { return ___win_5; }
	inline GameObject_t1756533147 ** get_address_of_win_5() { return &___win_5; }
	inline void set_win_5(GameObject_t1756533147 * value)
	{
		___win_5 = value;
		Il2CppCodeGenWriteBarrier(&___win_5, value);
	}

	inline static int32_t get_offset_of_notWin_6() { return static_cast<int32_t>(offsetof(Animazioni_t3270717623, ___notWin_6)); }
	inline GameObject_t1756533147 * get_notWin_6() const { return ___notWin_6; }
	inline GameObject_t1756533147 ** get_address_of_notWin_6() { return &___notWin_6; }
	inline void set_notWin_6(GameObject_t1756533147 * value)
	{
		___notWin_6 = value;
		Il2CppCodeGenWriteBarrier(&___notWin_6, value);
	}

	inline static int32_t get_offset_of_info_7() { return static_cast<int32_t>(offsetof(Animazioni_t3270717623, ___info_7)); }
	inline Text_t356221433 * get_info_7() const { return ___info_7; }
	inline Text_t356221433 ** get_address_of_info_7() { return &___info_7; }
	inline void set_info_7(Text_t356221433 * value)
	{
		___info_7 = value;
		Il2CppCodeGenWriteBarrier(&___info_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
