﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<ARController/ARToolKitThresholdMode,System.Object>
struct ShimEnumerator_t4079880528;
// System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>
struct Dictionary_2_t3974755707;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ARController/ARToolKitThresholdMode,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m439907647_gshared (ShimEnumerator_t4079880528 * __this, Dictionary_2_t3974755707 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m439907647(__this, ___host0, method) ((  void (*) (ShimEnumerator_t4079880528 *, Dictionary_2_t3974755707 *, const MethodInfo*))ShimEnumerator__ctor_m439907647_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<ARController/ARToolKitThresholdMode,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1802676218_gshared (ShimEnumerator_t4079880528 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1802676218(__this, method) ((  bool (*) (ShimEnumerator_t4079880528 *, const MethodInfo*))ShimEnumerator_MoveNext_m1802676218_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<ARController/ARToolKitThresholdMode,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m3443690_gshared (ShimEnumerator_t4079880528 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m3443690(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t4079880528 *, const MethodInfo*))ShimEnumerator_get_Entry_m3443690_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ARController/ARToolKitThresholdMode,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1543295439_gshared (ShimEnumerator_t4079880528 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m1543295439(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t4079880528 *, const MethodInfo*))ShimEnumerator_get_Key_m1543295439_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ARController/ARToolKitThresholdMode,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2011471335_gshared (ShimEnumerator_t4079880528 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m2011471335(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t4079880528 *, const MethodInfo*))ShimEnumerator_get_Value_m2011471335_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<ARController/ARToolKitThresholdMode,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m4106908349_gshared (ShimEnumerator_t4079880528 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m4106908349(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t4079880528 *, const MethodInfo*))ShimEnumerator_get_Current_m4106908349_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<ARController/ARToolKitThresholdMode,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m956201857_gshared (ShimEnumerator_t4079880528 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m956201857(__this, method) ((  void (*) (ShimEnumerator_t4079880528 *, const MethodInfo*))ShimEnumerator_Reset_m956201857_gshared)(__this, method)
