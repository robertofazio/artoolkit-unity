﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Net;
using System;

public class Main : MonoBehaviour 
{
	public enum AppState
	{
		STATE_START,
		STATE_AR,
		STATE_RECAP
	};
	public AppState curState;

	public WebClient web = new WebClient();
	const string sLose = "non hai vinto";

	public string stored;
	public static string reply;
	public InputField codice;
	public InputField totale;
	public GameObject panel;
	public GameObject panelPostPlay;
	public GameObject panelRecap;
	public Text txtResult;
	public Text txtPremi;
	//public GameObject goButAvanti;
	private string playerID;
	public static bool bWin = false;

	public int curPlay = 0;
	public int numPlays = 1;
	public string[] gameResults = {};
	public static bool bFound = false;

	private ARController myARController;
	private ARTrackedObject myARTtrackedObject;

	public static Main main
	{ 
		private get;
		set;
	}

	void Awake()
	{
		//goButAvanti = GameObject.Find ("ButAvanti");
		//goButAvanti.SetActive (false);
		panelPostPlay.SetActive(false);

		main = this;
		myARController = main.GetComponent<ARController>();
		myARTtrackedObject = main.GetComponent<ARTrackedObject> ();

	}

	void Start () 
	{
		goStart ();
	}
	
	void Update () 
	{
		if (curState == AppState.STATE_AR) {
			if (!bFound) {
				if (myARController.bFound) {
					//new!
					onMarkerFound();
					//goButAvanti.SetActive(true);
				}
			} else {
				if (!myARController.bFound) {
					//lost
				}
			}
			bFound = myARController.bFound;
		}

	}

	public void goStart(){
		curState = AppState.STATE_START;
		bWin = false;
		txtPremi.text = "";
		codice.text = "";
		totale.text = "";
		curPlay = 0;
		numPlays = 1;
		bFound = false;
		myARController.StopAR ();
		panel.SetActive (true);
		//goButAvanti.SetActive (false);
		panelPostPlay.SetActive(false);
		panelRecap.SetActive (false);
		//Debug.LogError ("Entering START "+curPlay+"/"+numPlays);
	}

	public void goNextPlay(){
		if (curPlay < numPlays) {

			curState = AppState.STATE_AR;
			curPlay++;
			checkWin ();
			bFound = false;
			panel.SetActive (false);
			panelRecap.SetActive (false);
			//goButAvanti.SetActive (false);
			panelPostPlay.SetActive(false);
			myARController.StartAR ();
		//Debug.LogError ("Entering PLAY "+curPlay+"/"+numPlays);
			//MarkerScene.gameObject.GetComponentInChildren<Transform>().gameObject.SetActive (false);

		} else {
			//goStart ();
			goRecap();
		}
	}

	public void goRecap(){
		curState = AppState.STATE_RECAP;
		makeRecapText ();
		myARController.StopAR ();
		panel.SetActive (false);
		panelPostPlay.SetActive (false);
		panelRecap.SetActive (true);
	}

	public void setNumPlays(int _n){
		curPlay = 0;
		numPlays = _n;
	}

	public void onClickAvanti(){
		//if (curPlay < numPlays) {
			myARController.StopAR ();
			goNextPlay ();
		//} else {
		//	goStart ();
		//}

	}

	public void onMarkerFound(){
		panelPostPlay.SetActive(true);
		if (gameResults.Length >= (curPlay - 1)) {
			txtResult.text = gameResults [curPlay - 1];
		}
	}

	// event button UI
	public void GetCode()
	{
		const string baseUrl = "https://justws.developing.it/api.php/pacchi/getPlay";
		string curUrl = baseUrl + "/" + codice.text + "/" + totale.text;
		reply = web.DownloadString (curUrl);
		Debug.LogError (reply);
		gameResults = parseUserWebReply (reply);
		setNumPlays (gameResults.Length);
		if (gameResults.Length > 0) {
			goNextPlay ();
		} else {
			goRecap ();
		}
	}

	public string[] parseUserWebReply(string data){
		string s = "";
		//let's remove the wrapping brackets
		if (data.Length > 2) {
			s = data.Substring (1, data.Length - 2);
			//Debug.LogError (s);
		} else {
			Debug.LogError ("WebService reply must be longer than 2 chars");
		}
		//csv style split
		char[] commaSep = { ',' };
		string[] ss = s.Split(commaSep);
		for (int i = 0; i < ss.Length; i++) {
			//every element has brackets and quotes
			if (ss [i].Length > 4) {
				ss [i] = ss [i].Substring (2,ss[i].Length-4);
				//Debug.LogError (ss[i]);
			} else {
				Debug.LogError ("Webservice elements must have [\"string\"] format");
				ss [i] = sLose;
			}
		}
		return ss;
	}

	void checkWin(){
		
		if (gameResults.Length >= (curPlay - 1)) {
			bWin = (gameResults[curPlay-1]!=sLose);
		} else {
			bWin = false;

		}

	}

	void makeRecapText(){
		txtPremi.text = "";
		string s = "";
		for (int i = 0; i < gameResults.Length; i++) {
			s += gameResults[i] + "\n";
		}
		txtPremi.text = s;
	}

	void hideModels(){
		
	}
}
