﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22175420862MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<ContentMode,System.String>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m65768605(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1515191800 *, int32_t, String_t*, const MethodInfo*))KeyValuePair_2__ctor_m2727937921_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<ContentMode,System.String>::get_Key()
#define KeyValuePair_2_get_Key_m2709225787(__this, method) ((  int32_t (*) (KeyValuePair_2_t1515191800 *, const MethodInfo*))KeyValuePair_2_get_Key_m1256702711_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<ContentMode,System.String>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3952411018(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1515191800 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m3305375956_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<ContentMode,System.String>::get_Value()
#define KeyValuePair_2_get_Value_m1944434971(__this, method) ((  String_t* (*) (KeyValuePair_2_t1515191800 *, const MethodInfo*))KeyValuePair_2_get_Value_m2708364631_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<ContentMode,System.String>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1503793194(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1515191800 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Value_m2787780628_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<ContentMode,System.String>::ToString()
#define KeyValuePair_2_ToString_m2671088338(__this, method) ((  String_t* (*) (KeyValuePair_2_t1515191800 *, const MethodInfo*))KeyValuePair_2_ToString_m1193705512_gshared)(__this, method)
