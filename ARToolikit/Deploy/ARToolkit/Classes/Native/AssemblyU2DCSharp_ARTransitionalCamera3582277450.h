﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "AssemblyU2DCSharp_ARTrackedCamera3950879424.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ARTransitionalCamera
struct  ARTransitionalCamera_t3582277450  : public ARTrackedCamera_t3950879424
{
public:
	// UnityEngine.Vector3 ARTransitionalCamera::vrTargetPosition
	Vector3_t2243707580  ___vrTargetPosition_25;
	// UnityEngine.Quaternion ARTransitionalCamera::vrTargetRotation
	Quaternion_t4030073918  ___vrTargetRotation_26;
	// UnityEngine.GameObject ARTransitionalCamera::targetObject
	GameObject_t1756533147 * ___targetObject_27;
	// System.Single ARTransitionalCamera::transitionAmount
	float ___transitionAmount_28;
	// System.Single ARTransitionalCamera::movementRate
	float ___movementRate_29;
	// System.Single ARTransitionalCamera::vrObserverAzimuth
	float ___vrObserverAzimuth_30;
	// System.Single ARTransitionalCamera::vrObserverElevation
	float ___vrObserverElevation_31;
	// UnityEngine.Vector3 ARTransitionalCamera::vrObserverOffset
	Vector3_t2243707580  ___vrObserverOffset_32;
	// System.Boolean ARTransitionalCamera::automaticTransition
	bool ___automaticTransition_33;
	// System.Single ARTransitionalCamera::automaticTransitionDistance
	float ___automaticTransitionDistance_34;

public:
	inline static int32_t get_offset_of_vrTargetPosition_25() { return static_cast<int32_t>(offsetof(ARTransitionalCamera_t3582277450, ___vrTargetPosition_25)); }
	inline Vector3_t2243707580  get_vrTargetPosition_25() const { return ___vrTargetPosition_25; }
	inline Vector3_t2243707580 * get_address_of_vrTargetPosition_25() { return &___vrTargetPosition_25; }
	inline void set_vrTargetPosition_25(Vector3_t2243707580  value)
	{
		___vrTargetPosition_25 = value;
	}

	inline static int32_t get_offset_of_vrTargetRotation_26() { return static_cast<int32_t>(offsetof(ARTransitionalCamera_t3582277450, ___vrTargetRotation_26)); }
	inline Quaternion_t4030073918  get_vrTargetRotation_26() const { return ___vrTargetRotation_26; }
	inline Quaternion_t4030073918 * get_address_of_vrTargetRotation_26() { return &___vrTargetRotation_26; }
	inline void set_vrTargetRotation_26(Quaternion_t4030073918  value)
	{
		___vrTargetRotation_26 = value;
	}

	inline static int32_t get_offset_of_targetObject_27() { return static_cast<int32_t>(offsetof(ARTransitionalCamera_t3582277450, ___targetObject_27)); }
	inline GameObject_t1756533147 * get_targetObject_27() const { return ___targetObject_27; }
	inline GameObject_t1756533147 ** get_address_of_targetObject_27() { return &___targetObject_27; }
	inline void set_targetObject_27(GameObject_t1756533147 * value)
	{
		___targetObject_27 = value;
		Il2CppCodeGenWriteBarrier(&___targetObject_27, value);
	}

	inline static int32_t get_offset_of_transitionAmount_28() { return static_cast<int32_t>(offsetof(ARTransitionalCamera_t3582277450, ___transitionAmount_28)); }
	inline float get_transitionAmount_28() const { return ___transitionAmount_28; }
	inline float* get_address_of_transitionAmount_28() { return &___transitionAmount_28; }
	inline void set_transitionAmount_28(float value)
	{
		___transitionAmount_28 = value;
	}

	inline static int32_t get_offset_of_movementRate_29() { return static_cast<int32_t>(offsetof(ARTransitionalCamera_t3582277450, ___movementRate_29)); }
	inline float get_movementRate_29() const { return ___movementRate_29; }
	inline float* get_address_of_movementRate_29() { return &___movementRate_29; }
	inline void set_movementRate_29(float value)
	{
		___movementRate_29 = value;
	}

	inline static int32_t get_offset_of_vrObserverAzimuth_30() { return static_cast<int32_t>(offsetof(ARTransitionalCamera_t3582277450, ___vrObserverAzimuth_30)); }
	inline float get_vrObserverAzimuth_30() const { return ___vrObserverAzimuth_30; }
	inline float* get_address_of_vrObserverAzimuth_30() { return &___vrObserverAzimuth_30; }
	inline void set_vrObserverAzimuth_30(float value)
	{
		___vrObserverAzimuth_30 = value;
	}

	inline static int32_t get_offset_of_vrObserverElevation_31() { return static_cast<int32_t>(offsetof(ARTransitionalCamera_t3582277450, ___vrObserverElevation_31)); }
	inline float get_vrObserverElevation_31() const { return ___vrObserverElevation_31; }
	inline float* get_address_of_vrObserverElevation_31() { return &___vrObserverElevation_31; }
	inline void set_vrObserverElevation_31(float value)
	{
		___vrObserverElevation_31 = value;
	}

	inline static int32_t get_offset_of_vrObserverOffset_32() { return static_cast<int32_t>(offsetof(ARTransitionalCamera_t3582277450, ___vrObserverOffset_32)); }
	inline Vector3_t2243707580  get_vrObserverOffset_32() const { return ___vrObserverOffset_32; }
	inline Vector3_t2243707580 * get_address_of_vrObserverOffset_32() { return &___vrObserverOffset_32; }
	inline void set_vrObserverOffset_32(Vector3_t2243707580  value)
	{
		___vrObserverOffset_32 = value;
	}

	inline static int32_t get_offset_of_automaticTransition_33() { return static_cast<int32_t>(offsetof(ARTransitionalCamera_t3582277450, ___automaticTransition_33)); }
	inline bool get_automaticTransition_33() const { return ___automaticTransition_33; }
	inline bool* get_address_of_automaticTransition_33() { return &___automaticTransition_33; }
	inline void set_automaticTransition_33(bool value)
	{
		___automaticTransition_33 = value;
	}

	inline static int32_t get_offset_of_automaticTransitionDistance_34() { return static_cast<int32_t>(offsetof(ARTransitionalCamera_t3582277450, ___automaticTransitionDistance_34)); }
	inline float get_automaticTransitionDistance_34() const { return ___automaticTransitionDistance_34; }
	inline float* get_address_of_automaticTransitionDistance_34() { return &___automaticTransitionDistance_34; }
	inline void set_automaticTransitionDistance_34(float value)
	{
		___automaticTransitionDistance_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
