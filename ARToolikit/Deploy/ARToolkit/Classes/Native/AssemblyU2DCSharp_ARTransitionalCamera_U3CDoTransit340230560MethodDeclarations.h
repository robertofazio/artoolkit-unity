﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ARTransitionalCamera/<DoTransition>c__Iterator0
struct U3CDoTransitionU3Ec__Iterator0_t340230560;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ARTransitionalCamera/<DoTransition>c__Iterator0::.ctor()
extern "C"  void U3CDoTransitionU3Ec__Iterator0__ctor_m3676305879 (U3CDoTransitionU3Ec__Iterator0_t340230560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ARTransitionalCamera/<DoTransition>c__Iterator0::MoveNext()
extern "C"  bool U3CDoTransitionU3Ec__Iterator0_MoveNext_m995060305 (U3CDoTransitionU3Ec__Iterator0_t340230560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ARTransitionalCamera/<DoTransition>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDoTransitionU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3081689489 (U3CDoTransitionU3Ec__Iterator0_t340230560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ARTransitionalCamera/<DoTransition>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDoTransitionU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m100713465 (U3CDoTransitionU3Ec__Iterator0_t340230560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARTransitionalCamera/<DoTransition>c__Iterator0::Dispose()
extern "C"  void U3CDoTransitionU3Ec__Iterator0_Dispose_m3270519898 (U3CDoTransitionU3Ec__Iterator0_t340230560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARTransitionalCamera/<DoTransition>c__Iterator0::Reset()
extern "C"  void U3CDoTransitionU3Ec__Iterator0_Reset_m3338190028 (U3CDoTransitionU3Ec__Iterator0_t340230560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
