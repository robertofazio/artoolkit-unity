﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Animazioni : MonoBehaviour 
{
	public GameObject animationWin;
	public GameObject animationNotWin;
	public GameObject fireworks;
	private GameObject win;
	private GameObject notWin;
	public Text info;

	public void CheckWinner()
	{
		if(Main.bWin)
		{
			win = Instantiate(animationWin, transform.position, Quaternion.identity) as GameObject;
			win.transform.parent = this.transform;
			GameObject fireWorks = Instantiate (fireworks, transform.position, Quaternion.identity) as GameObject;
			fireWorks.transform.parent = win.transform;
			win.SetActive (false);
			animationNotWin.SetActive(false);
			Main.bWin = false;
			//info.text = win.name + " " + win.activeSelf;
		}
		else if(!Main.bWin)
		{
			notWin = Instantiate(animationNotWin, transform.position, Quaternion.identity) as GameObject;
			notWin.transform.parent = this.transform;
			notWin.SetActive (false);
			//info.text = notWin.name + " " + notWin.activeSelf;
			animationWin.SetActive(false);
		}
	}
}
