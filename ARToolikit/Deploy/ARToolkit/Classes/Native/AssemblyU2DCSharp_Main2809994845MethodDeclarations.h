﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Main
struct Main_t2809994845;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Main2809994845.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Main::.ctor()
extern "C"  void Main__ctor_m325986520 (Main_t2809994845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Main Main::get_main()
extern "C"  Main_t2809994845 * Main_get_main_m2962309062 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main::set_main(Main)
extern "C"  void Main_set_main_m1382543681 (Il2CppObject * __this /* static, unused */, Main_t2809994845 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main::Awake()
extern "C"  void Main_Awake_m955704701 (Main_t2809994845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main::Start()
extern "C"  void Main_Start_m2091519880 (Main_t2809994845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main::Update()
extern "C"  void Main_Update_m2467676297 (Main_t2809994845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main::goStart()
extern "C"  void Main_goStart_m2958511914 (Main_t2809994845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main::goNextPlay()
extern "C"  void Main_goNextPlay_m2503892891 (Main_t2809994845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main::goRecap()
extern "C"  void Main_goRecap_m414710553 (Main_t2809994845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main::setNumPlays(System.Int32)
extern "C"  void Main_setNumPlays_m3669947674 (Main_t2809994845 * __this, int32_t ____n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main::onClickAvanti()
extern "C"  void Main_onClickAvanti_m543164064 (Main_t2809994845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main::onMarkerFound()
extern "C"  void Main_onMarkerFound_m2678331393 (Main_t2809994845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main::GetCode()
extern "C"  void Main_GetCode_m2363977499 (Main_t2809994845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Main::parseUserWebReply(System.String)
extern "C"  StringU5BU5D_t1642385972* Main_parseUserWebReply_m4126468891 (Main_t2809994845 * __this, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main::checkWin()
extern "C"  void Main_checkWin_m3851975444 (Main_t2809994845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main::makeRecapText()
extern "C"  void Main_makeRecapText_m687811546 (Main_t2809994845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main::hideModels()
extern "C"  void Main_hideModels_m1537942704 (Main_t2809994845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Main::.cctor()
extern "C"  void Main__cctor_m2288302007 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
