﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<ARController/ARToolKitThresholdMode,System.Object>
struct ValueCollection_t2677815550;
// System.Collections.Generic.Dictionary`2<ARController/ARToolKitThresholdMode,System.Object>
struct Dictionary_2_t3974755707;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1366321175.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ARController/ARToolKitThresholdMode,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m2980136029_gshared (ValueCollection_t2677815550 * __this, Dictionary_2_t3974755707 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m2980136029(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2677815550 *, Dictionary_2_t3974755707 *, const MethodInfo*))ValueCollection__ctor_m2980136029_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ARController/ARToolKitThresholdMode,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2829838359_gshared (ValueCollection_t2677815550 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2829838359(__this, ___item0, method) ((  void (*) (ValueCollection_t2677815550 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2829838359_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ARController/ARToolKitThresholdMode,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1940960952_gshared (ValueCollection_t2677815550 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1940960952(__this, method) ((  void (*) (ValueCollection_t2677815550 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1940960952_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ARController/ARToolKitThresholdMode,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1947576125_gshared (ValueCollection_t2677815550 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1947576125(__this, ___item0, method) ((  bool (*) (ValueCollection_t2677815550 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1947576125_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ARController/ARToolKitThresholdMode,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1913987922_gshared (ValueCollection_t2677815550 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1913987922(__this, ___item0, method) ((  bool (*) (ValueCollection_t2677815550 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1913987922_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ARController/ARToolKitThresholdMode,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2934412752_gshared (ValueCollection_t2677815550 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2934412752(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2677815550 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2934412752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ARController/ARToolKitThresholdMode,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m3344617736_gshared (ValueCollection_t2677815550 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m3344617736(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2677815550 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3344617736_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<ARController/ARToolKitThresholdMode,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3663875303_gshared (ValueCollection_t2677815550 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3663875303(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2677815550 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3663875303_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ARController/ARToolKitThresholdMode,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3860584182_gshared (ValueCollection_t2677815550 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3860584182(__this, method) ((  bool (*) (ValueCollection_t2677815550 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3860584182_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<ARController/ARToolKitThresholdMode,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1004418236_gshared (ValueCollection_t2677815550 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1004418236(__this, method) ((  bool (*) (ValueCollection_t2677815550 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1004418236_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<ARController/ARToolKitThresholdMode,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m1281476600_gshared (ValueCollection_t2677815550 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1281476600(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2677815550 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1281476600_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<ARController/ARToolKitThresholdMode,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m3814159732_gshared (ValueCollection_t2677815550 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m3814159732(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2677815550 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m3814159732_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<ARController/ARToolKitThresholdMode,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1366321175  ValueCollection_GetEnumerator_m1655154457_gshared (ValueCollection_t2677815550 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m1655154457(__this, method) ((  Enumerator_t1366321175  (*) (ValueCollection_t2677815550 *, const MethodInfo*))ValueCollection_GetEnumerator_m1655154457_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<ARController/ARToolKitThresholdMode,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m1967611356_gshared (ValueCollection_t2677815550 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m1967611356(__this, method) ((  int32_t (*) (ValueCollection_t2677815550 *, const MethodInfo*))ValueCollection_get_Count_m1967611356_gshared)(__this, method)
