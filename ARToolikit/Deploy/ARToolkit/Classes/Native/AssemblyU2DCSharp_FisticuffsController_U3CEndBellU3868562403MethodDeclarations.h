﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FisticuffsController/<EndBell>c__Iterator1
struct U3CEndBellU3Ec__Iterator1_t868562403;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FisticuffsController/<EndBell>c__Iterator1::.ctor()
extern "C"  void U3CEndBellU3Ec__Iterator1__ctor_m572293778 (U3CEndBellU3Ec__Iterator1_t868562403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FisticuffsController/<EndBell>c__Iterator1::MoveNext()
extern "C"  bool U3CEndBellU3Ec__Iterator1_MoveNext_m372940338 (U3CEndBellU3Ec__Iterator1_t868562403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FisticuffsController/<EndBell>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CEndBellU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m626065106 (U3CEndBellU3Ec__Iterator1_t868562403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FisticuffsController/<EndBell>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CEndBellU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m466185690 (U3CEndBellU3Ec__Iterator1_t868562403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FisticuffsController/<EndBell>c__Iterator1::Dispose()
extern "C"  void U3CEndBellU3Ec__Iterator1_Dispose_m2051336465 (U3CEndBellU3Ec__Iterator1_t868562403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FisticuffsController/<EndBell>c__Iterator1::Reset()
extern "C"  void U3CEndBellU3Ec__Iterator1_Reset_m4036382267 (U3CEndBellU3Ec__Iterator1_t868562403 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
