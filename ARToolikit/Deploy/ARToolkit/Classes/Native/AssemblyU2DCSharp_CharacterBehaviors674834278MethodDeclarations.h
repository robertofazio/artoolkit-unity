﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CharacterBehaviors
struct CharacterBehaviors_t674834278;

#include "codegen/il2cpp-codegen.h"

// System.Void CharacterBehaviors::.ctor()
extern "C"  void CharacterBehaviors__ctor_m1152876165 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterBehaviors::Awake()
extern "C"  void CharacterBehaviors_Awake_m3037885346 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterBehaviors::Start()
extern "C"  void CharacterBehaviors_Start_m1108797477 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterBehaviors::OnEnable()
extern "C"  void CharacterBehaviors_OnEnable_m3487677605 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterBehaviors::OnDisable()
extern "C"  void CharacterBehaviors_OnDisable_m1321403450 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterBehaviors::ResetMe()
extern "C"  void CharacterBehaviors_ResetMe_m3493009400 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterBehaviors::Update()
extern "C"  void CharacterBehaviors_Update_m1593411844 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterBehaviors::TouchTracker()
extern "C"  void CharacterBehaviors_TouchTracker_m4253395824 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterBehaviors::PopUp()
extern "C"  void CharacterBehaviors_PopUp_m2295277643 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterBehaviors::StartPopUp()
extern "C"  void CharacterBehaviors_StartPopUp_m3438804919 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterBehaviors::StopPopUp()
extern "C"  void CharacterBehaviors_StopPopUp_m2532184579 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterBehaviors::GlovesRetract()
extern "C"  void CharacterBehaviors_GlovesRetract_m3584609958 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterBehaviors::SetUpPunch()
extern "C"  void CharacterBehaviors_SetUpPunch_m426768826 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterBehaviors::Punch()
extern "C"  void CharacterBehaviors_Punch_m1922326469 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterBehaviors::Hover()
extern "C"  void CharacterBehaviors_Hover_m2887359879 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterBehaviors::ResetCheck()
extern "C"  void CharacterBehaviors_ResetCheck_m2350668060 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterBehaviors::CalculateDamageToOpponent()
extern "C"  void CharacterBehaviors_CalculateDamageToOpponent_m1939745900 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterBehaviors::ReceiveDamage(System.Single)
extern "C"  void CharacterBehaviors_ReceiveDamage_m2006513562 (CharacterBehaviors_t674834278 * __this, float ___howMuchDamage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterBehaviors::SetHealth(System.Single)
extern "C"  void CharacterBehaviors_SetHealth_m3563463624 (CharacterBehaviors_t674834278 * __this, float ___newHealthPoints0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterBehaviors::SetHealthScaleFactor()
extern "C"  void CharacterBehaviors_SetHealthScaleFactor_m2141324510 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterBehaviors::CheckForGameOver()
extern "C"  void CharacterBehaviors_CheckForGameOver_m3978069034 (CharacterBehaviors_t674834278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterBehaviors::.cctor()
extern "C"  void CharacterBehaviors__cctor_m4292261400 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
