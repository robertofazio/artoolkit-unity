﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Xml.XmlReader
struct XmlReader_t3675626668;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JsonManager
struct  JsonManager_t1028810929  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct JsonManager_t1028810929_StaticFields
{
public:
	// System.Xml.XmlReader JsonManager::xml
	XmlReader_t3675626668 * ___xml_2;
	// System.String JsonManager::filename
	String_t* ___filename_3;
	// System.Int32 JsonManager::countPlay
	int32_t ___countPlay_4;
	// System.String[] JsonManager::numString
	StringU5BU5D_t1642385972* ___numString_5;

public:
	inline static int32_t get_offset_of_xml_2() { return static_cast<int32_t>(offsetof(JsonManager_t1028810929_StaticFields, ___xml_2)); }
	inline XmlReader_t3675626668 * get_xml_2() const { return ___xml_2; }
	inline XmlReader_t3675626668 ** get_address_of_xml_2() { return &___xml_2; }
	inline void set_xml_2(XmlReader_t3675626668 * value)
	{
		___xml_2 = value;
		Il2CppCodeGenWriteBarrier(&___xml_2, value);
	}

	inline static int32_t get_offset_of_filename_3() { return static_cast<int32_t>(offsetof(JsonManager_t1028810929_StaticFields, ___filename_3)); }
	inline String_t* get_filename_3() const { return ___filename_3; }
	inline String_t** get_address_of_filename_3() { return &___filename_3; }
	inline void set_filename_3(String_t* value)
	{
		___filename_3 = value;
		Il2CppCodeGenWriteBarrier(&___filename_3, value);
	}

	inline static int32_t get_offset_of_countPlay_4() { return static_cast<int32_t>(offsetof(JsonManager_t1028810929_StaticFields, ___countPlay_4)); }
	inline int32_t get_countPlay_4() const { return ___countPlay_4; }
	inline int32_t* get_address_of_countPlay_4() { return &___countPlay_4; }
	inline void set_countPlay_4(int32_t value)
	{
		___countPlay_4 = value;
	}

	inline static int32_t get_offset_of_numString_5() { return static_cast<int32_t>(offsetof(JsonManager_t1028810929_StaticFields, ___numString_5)); }
	inline StringU5BU5D_t1642385972* get_numString_5() const { return ___numString_5; }
	inline StringU5BU5D_t1642385972** get_address_of_numString_5() { return &___numString_5; }
	inline void set_numString_5(StringU5BU5D_t1642385972* value)
	{
		___numString_5 = value;
		Il2CppCodeGenWriteBarrier(&___numString_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
