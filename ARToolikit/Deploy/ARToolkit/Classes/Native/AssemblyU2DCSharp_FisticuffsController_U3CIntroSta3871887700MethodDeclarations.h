﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FisticuffsController/<IntroStart>c__Iterator0
struct U3CIntroStartU3Ec__Iterator0_t3871887700;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FisticuffsController/<IntroStart>c__Iterator0::.ctor()
extern "C"  void U3CIntroStartU3Ec__Iterator0__ctor_m3149942269 (U3CIntroStartU3Ec__Iterator0_t3871887700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FisticuffsController/<IntroStart>c__Iterator0::MoveNext()
extern "C"  bool U3CIntroStartU3Ec__Iterator0_MoveNext_m503637435 (U3CIntroStartU3Ec__Iterator0_t3871887700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FisticuffsController/<IntroStart>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CIntroStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2993597583 (U3CIntroStartU3Ec__Iterator0_t3871887700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FisticuffsController/<IntroStart>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CIntroStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1169169895 (U3CIntroStartU3Ec__Iterator0_t3871887700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FisticuffsController/<IntroStart>c__Iterator0::Dispose()
extern "C"  void U3CIntroStartU3Ec__Iterator0_Dispose_m1828718102 (U3CIntroStartU3Ec__Iterator0_t3871887700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FisticuffsController/<IntroStart>c__Iterator0::Reset()
extern "C"  void U3CIntroStartU3Ec__Iterator0_Reset_m1306905816 (U3CIntroStartU3Ec__Iterator0_t3871887700 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
