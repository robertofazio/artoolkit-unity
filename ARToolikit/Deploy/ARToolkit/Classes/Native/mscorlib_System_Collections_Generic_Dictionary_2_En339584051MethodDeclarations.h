﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En999813113MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ARController/ARToolKitThresholdMode,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3317191858(__this, ___dictionary0, method) ((  void (*) (Enumerator_t339584051 *, Dictionary_2_t3314526645 *, const MethodInfo*))Enumerator__ctor_m550298620_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ARController/ARToolKitThresholdMode,System.String>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2854943757(__this, method) ((  Il2CppObject * (*) (Enumerator_t339584051 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3448596329_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ARController/ARToolKitThresholdMode,System.String>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2613553437(__this, method) ((  void (*) (Enumerator_t339584051 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m456649017_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<ARController/ARToolKitThresholdMode,System.String>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3636787036(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t339584051 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3932033974_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ARController/ARToolKitThresholdMode,System.String>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m759177811(__this, method) ((  Il2CppObject * (*) (Enumerator_t339584051 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2380732015_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ARController/ARToolKitThresholdMode,System.String>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2188392555(__this, method) ((  Il2CppObject * (*) (Enumerator_t339584051 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3673366751_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<ARController/ARToolKitThresholdMode,System.String>::MoveNext()
#define Enumerator_MoveNext_m2407501669(__this, method) ((  bool (*) (Enumerator_t339584051 *, const MethodInfo*))Enumerator_MoveNext_m916534521_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<ARController/ARToolKitThresholdMode,System.String>::get_Current()
#define Enumerator_get_Current_m1282262193(__this, method) ((  KeyValuePair_2_t1071871867  (*) (Enumerator_t339584051 *, const MethodInfo*))Enumerator_get_Current_m400230469_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<ARController/ARToolKitThresholdMode,System.String>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m345777618(__this, method) ((  int32_t (*) (Enumerator_t339584051 *, const MethodInfo*))Enumerator_get_CurrentKey_m3586788524_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<ARController/ARToolKitThresholdMode,System.String>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2248259090(__this, method) ((  String_t* (*) (Enumerator_t339584051 *, const MethodInfo*))Enumerator_get_CurrentValue_m1406815020_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ARController/ARToolKitThresholdMode,System.String>::Reset()
#define Enumerator_Reset_m1104329492(__this, method) ((  void (*) (Enumerator_t339584051 *, const MethodInfo*))Enumerator_Reset_m246264030_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ARController/ARToolKitThresholdMode,System.String>::VerifyState()
#define Enumerator_VerifyState_m687699531(__this, method) ((  void (*) (Enumerator_t339584051 *, const MethodInfo*))Enumerator_VerifyState_m2906625695_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ARController/ARToolKitThresholdMode,System.String>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m519923437(__this, method) ((  void (*) (Enumerator_t339584051 *, const MethodInfo*))Enumerator_VerifyCurrent_m3943498473_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ARController/ARToolKitThresholdMode,System.String>::Dispose()
#define Enumerator_Dispose_m2344969242(__this, method) ((  void (*) (Enumerator_t339584051 *, const MethodInfo*))Enumerator_Dispose_m2489208708_gshared)(__this, method)
