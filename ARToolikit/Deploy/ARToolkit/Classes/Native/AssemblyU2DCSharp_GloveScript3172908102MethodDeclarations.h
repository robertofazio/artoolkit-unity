﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GloveScript
struct GloveScript_t3172908102;
// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"

// System.Void GloveScript::.ctor()
extern "C"  void GloveScript__ctor_m450649515 (GloveScript_t3172908102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GloveScript::Start()
extern "C"  void GloveScript_Start_m2534267511 (GloveScript_t3172908102 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GloveScript::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void GloveScript_OnTriggerEnter_m3362502079 (GloveScript_t3172908102 * __this, Collider_t3497673348 * ___hit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GloveScript::FinishPunch(UnityEngine.GameObject,System.Boolean)
extern "C"  void GloveScript_FinishPunch_m1990033543 (GloveScript_t3172908102 * __this, GameObject_t1756533147 * ___otherCharacter0, bool ___hit1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
