﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<ContentMode,System.Object>
struct Dictionary_2_t123108344;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1443133046.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22175420862.h"
#include "AssemblyU2DCSharp_ContentMode2920913530.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ContentMode,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1162151167_gshared (Enumerator_t1443133046 * __this, Dictionary_2_t123108344 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m1162151167(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1443133046 *, Dictionary_2_t123108344 *, const MethodInfo*))Enumerator__ctor_m1162151167_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ContentMode,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1977822800_gshared (Enumerator_t1443133046 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1977822800(__this, method) ((  Il2CppObject * (*) (Enumerator_t1443133046 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1977822800_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ContentMode,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2525508092_gshared (Enumerator_t1443133046 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2525508092(__this, method) ((  void (*) (Enumerator_t1443133046 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2525508092_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<ContentMode,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3849555629_gshared (Enumerator_t1443133046 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3849555629(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1443133046 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3849555629_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ContentMode,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2921324274_gshared (Enumerator_t1443133046 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2921324274(__this, method) ((  Il2CppObject * (*) (Enumerator_t1443133046 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2921324274_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ContentMode,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1926327352_gshared (Enumerator_t1443133046 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1926327352(__this, method) ((  Il2CppObject * (*) (Enumerator_t1443133046 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1926327352_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<ContentMode,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1771997540_gshared (Enumerator_t1443133046 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1771997540(__this, method) ((  bool (*) (Enumerator_t1443133046 *, const MethodInfo*))Enumerator_MoveNext_m1771997540_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<ContentMode,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t2175420862  Enumerator_get_Current_m512375100_gshared (Enumerator_t1443133046 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m512375100(__this, method) ((  KeyValuePair_2_t2175420862  (*) (Enumerator_t1443133046 *, const MethodInfo*))Enumerator_get_Current_m512375100_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<ContentMode,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m1412294563_gshared (Enumerator_t1443133046 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1412294563(__this, method) ((  int32_t (*) (Enumerator_t1443133046 *, const MethodInfo*))Enumerator_get_CurrentKey_m1412294563_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<ContentMode,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m2497194819_gshared (Enumerator_t1443133046 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m2497194819(__this, method) ((  Il2CppObject * (*) (Enumerator_t1443133046 *, const MethodInfo*))Enumerator_get_CurrentValue_m2497194819_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ContentMode,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m1467096549_gshared (Enumerator_t1443133046 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1467096549(__this, method) ((  void (*) (Enumerator_t1443133046 *, const MethodInfo*))Enumerator_Reset_m1467096549_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ContentMode,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m804409340_gshared (Enumerator_t1443133046 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m804409340(__this, method) ((  void (*) (Enumerator_t1443133046 *, const MethodInfo*))Enumerator_VerifyState_m804409340_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ContentMode,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m788903340_gshared (Enumerator_t1443133046 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m788903340(__this, method) ((  void (*) (Enumerator_t1443133046 *, const MethodInfo*))Enumerator_VerifyCurrent_m788903340_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ContentMode,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2793040299_gshared (Enumerator_t1443133046 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2793040299(__this, method) ((  void (*) (Enumerator_t1443133046 *, const MethodInfo*))Enumerator_Dispose_m2793040299_gshared)(__this, method)
