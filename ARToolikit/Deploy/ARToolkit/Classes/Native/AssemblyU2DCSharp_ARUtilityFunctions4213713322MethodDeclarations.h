﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Camera
struct Camera_t189460977;
// System.String
struct String_t;
// System.Single[]
struct SingleU5BU5D_t577127397;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Matrix4x42933234003.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// UnityEngine.Camera ARUtilityFunctions::FindCameraByName(System.String)
extern "C"  Camera_t189460977 * ARUtilityFunctions_FindCameraByName_m1687395814 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 ARUtilityFunctions::MatrixFromFloatArray(System.Single[])
extern "C"  Matrix4x4_t2933234003  ARUtilityFunctions_MatrixFromFloatArray_m3542072061 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___values0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion ARUtilityFunctions::QuaternionFromMatrix(UnityEngine.Matrix4x4)
extern "C"  Quaternion_t4030073918  ARUtilityFunctions_QuaternionFromMatrix_m2564935062 (Il2CppObject * __this /* static, unused */, Matrix4x4_t2933234003  ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 ARUtilityFunctions::PositionFromMatrix(UnityEngine.Matrix4x4)
extern "C"  Vector3_t2243707580  ARUtilityFunctions_PositionFromMatrix_m2391904295 (Il2CppObject * __this /* static, unused */, Matrix4x4_t2933234003  ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 ARUtilityFunctions::LHMatrixFromRHMatrix(UnityEngine.Matrix4x4)
extern "C"  Matrix4x4_t2933234003  ARUtilityFunctions_LHMatrixFromRHMatrix_m2988276788 (Il2CppObject * __this /* static, unused */, Matrix4x4_t2933234003  ___rhm0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
