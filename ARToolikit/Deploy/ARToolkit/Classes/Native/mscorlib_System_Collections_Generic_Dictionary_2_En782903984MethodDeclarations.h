﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1443133046MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ContentMode,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2002147499(__this, ___dictionary0, method) ((  void (*) (Enumerator_t782903984 *, Dictionary_2_t3757846578 *, const MethodInfo*))Enumerator__ctor_m1162151167_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ContentMode,System.String>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4034508970(__this, method) ((  Il2CppObject * (*) (Enumerator_t782903984 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1977822800_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ContentMode,System.String>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3434005462(__this, method) ((  void (*) (Enumerator_t782903984 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2525508092_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<ContentMode,System.String>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m522244049(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t782903984 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3849555629_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ContentMode,System.String>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2180374476(__this, method) ((  Il2CppObject * (*) (Enumerator_t782903984 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2921324274_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<ContentMode,System.String>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m4249957006(__this, method) ((  Il2CppObject * (*) (Enumerator_t782903984 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1926327352_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<ContentMode,System.String>::MoveNext()
#define Enumerator_MoveNext_m3986771854(__this, method) ((  bool (*) (Enumerator_t782903984 *, const MethodInfo*))Enumerator_MoveNext_m1771997540_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<ContentMode,System.String>::get_Current()
#define Enumerator_get_Current_m1657034662(__this, method) ((  KeyValuePair_2_t1515191800  (*) (Enumerator_t782903984 *, const MethodInfo*))Enumerator_get_Current_m512375100_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<ContentMode,System.String>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m220458823(__this, method) ((  int32_t (*) (Enumerator_t782903984 *, const MethodInfo*))Enumerator_get_CurrentKey_m1412294563_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<ContentMode,System.String>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m992764391(__this, method) ((  String_t* (*) (Enumerator_t782903984 *, const MethodInfo*))Enumerator_get_CurrentValue_m2497194819_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ContentMode,System.String>::Reset()
#define Enumerator_Reset_m3495863569(__this, method) ((  void (*) (Enumerator_t782903984 *, const MethodInfo*))Enumerator_Reset_m1467096549_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ContentMode,System.String>::VerifyState()
#define Enumerator_VerifyState_m413713010(__this, method) ((  void (*) (Enumerator_t782903984 *, const MethodInfo*))Enumerator_VerifyState_m804409340_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ContentMode,System.String>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m2568947494(__this, method) ((  void (*) (Enumerator_t782903984 *, const MethodInfo*))Enumerator_VerifyCurrent_m788903340_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<ContentMode,System.String>::Dispose()
#define Enumerator_Dispose_m999072119(__this, method) ((  void (*) (Enumerator_t782903984 *, const MethodInfo*))Enumerator_Dispose_m2793040299_gshared)(__this, method)
