﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2590853191.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21732100929.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ARController/ARToolKitThresholdMode,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3965592029_gshared (InternalEnumerator_1_t2590853191 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3965592029(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2590853191 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3965592029_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ARController/ARToolKitThresholdMode,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m635613913_gshared (InternalEnumerator_1_t2590853191 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m635613913(__this, method) ((  void (*) (InternalEnumerator_1_t2590853191 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m635613913_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ARController/ARToolKitThresholdMode,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m726442681_gshared (InternalEnumerator_1_t2590853191 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m726442681(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2590853191 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m726442681_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ARController/ARToolKitThresholdMode,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1508720110_gshared (InternalEnumerator_1_t2590853191 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1508720110(__this, method) ((  void (*) (InternalEnumerator_1_t2590853191 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1508720110_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ARController/ARToolKitThresholdMode,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m254276505_gshared (InternalEnumerator_1_t2590853191 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m254276505(__this, method) ((  bool (*) (InternalEnumerator_1_t2590853191 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m254276505_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ARController/ARToolKitThresholdMode,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t1732100929  InternalEnumerator_1_get_Current_m580551924_gshared (InternalEnumerator_1_t2590853191 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m580551924(__this, method) ((  KeyValuePair_2_t1732100929  (*) (InternalEnumerator_1_t2590853191 *, const MethodInfo*))InternalEnumerator_1_get_Current_m580551924_gshared)(__this, method)
