﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<MarkerType>
struct EqualityComparer_1_t3830567761;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.EqualityComparer`1<MarkerType>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m3396162651_gshared (EqualityComparer_1_t3830567761 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m3396162651(__this, method) ((  void (*) (EqualityComparer_1_t3830567761 *, const MethodInfo*))EqualityComparer_1__ctor_m3396162651_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<MarkerType>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m2079548716_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m2079548716(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m2079548716_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<MarkerType>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1940286942_gshared (EqualityComparer_1_t3830567761 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1940286942(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t3830567761 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1940286942_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<MarkerType>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2149302820_gshared (EqualityComparer_1_t3830567761 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2149302820(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t3830567761 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2149302820_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<MarkerType>::get_Default()
extern "C"  EqualityComparer_1_t3830567761 * EqualityComparer_1_get_Default_m3852711435_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m3852711435(__this /* static, unused */, method) ((  EqualityComparer_1_t3830567761 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m3852711435_gshared)(__this /* static, unused */, method)
