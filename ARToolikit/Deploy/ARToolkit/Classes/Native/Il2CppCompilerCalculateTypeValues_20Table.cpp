﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3343836395.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3928470916.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2260664863.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3435657708.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2213949596.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper385374196.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect2504093552.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect1728560551.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline1417504278.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV11102546563.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow4269599528.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_1568637717.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_AnimationBehaviors2387498671.h"
#include "AssemblyU2DCSharp_CharacterBehaviors674834278.h"
#include "AssemblyU2DCSharp_CharacterBehaviors_PopupMode1897899772.h"
#include "AssemblyU2DCSharp_FisticuffsController308090938.h"
#include "AssemblyU2DCSharp_FisticuffsController_U3CIntroSta3871887700.h"
#include "AssemblyU2DCSharp_FisticuffsController_U3CEndBellU3868562403.h"
#include "AssemblyU2DCSharp_FollowCameraPrecise1971515421.h"
#include "AssemblyU2DCSharp_GloveScript3172908102.h"
#include "AssemblyU2DCSharp_LookAtCameraFisticuffs2130711525.h"
#include "AssemblyU2DCSharp_TimedSelfDeactivate3836150707.h"
#include "AssemblyU2DCSharp_TimedSelfDestruct4043215813.h"
#include "AssemblyU2DCSharp_ARCamera431610428.h"
#include "AssemblyU2DCSharp_ARCamera_ViewEye928886047.h"
#include "AssemblyU2DCSharp_ContentMode2920913530.h"
#include "AssemblyU2DCSharp_ContentAlign2517740362.h"
#include "AssemblyU2DCSharp_ARController593870669.h"
#include "AssemblyU2DCSharp_ARController_ARToolKitThresholdM2161580787.h"
#include "AssemblyU2DCSharp_ARController_ARToolKitLabelingMod925999490.h"
#include "AssemblyU2DCSharp_ARController_ARToolKitPatternDet1396884775.h"
#include "AssemblyU2DCSharp_ARController_ARToolKitMatrixCode3871848761.h"
#include "AssemblyU2DCSharp_ARController_ARToolKitImageProcM1927279569.h"
#include "AssemblyU2DCSharp_ARController_ARW_UNITY_RENDER_EV3085043805.h"
#include "AssemblyU2DCSharp_ARController_ARW_ERROR1424361661.h"
#include "AssemblyU2DCSharp_ARController_AR_LOG_LEVEL1976762171.h"
#include "AssemblyU2DCSharp_MarkerType961965194.h"
#include "AssemblyU2DCSharp_ARWMarkerOption4121286531.h"
#include "AssemblyU2DCSharp_ARMarker1554260723.h"
#include "AssemblyU2DCSharp_ARNativePlugin494404617.h"
#include "AssemblyU2DCSharp_ARNativePluginStatic4005840597.h"
#include "AssemblyU2DCSharp_AROrigin3335349585.h"
#include "AssemblyU2DCSharp_AROrigin_FindMode1452273230.h"
#include "AssemblyU2DCSharp_ARPattern3906840165.h"
#include "AssemblyU2DCSharp_ARTrackedCamera3950879424.h"
#include "AssemblyU2DCSharp_ARTrackedObject1684152848.h"
#include "AssemblyU2DCSharp_ARTransitionalCamera3582277450.h"
#include "AssemblyU2DCSharp_ARTransitionalCamera_U3CDoTransit340230560.h"
#include "AssemblyU2DCSharp_ARUtilityFunctions4213713322.h"
#include "AssemblyU2DCSharp_PluginFunctions2887339240.h"
#include "AssemblyU2DCSharp_PluginFunctions_LogCallback2143553514.h"
#include "AssemblyU2DCSharp_Animazioni3270717623.h"
#include "AssemblyU2DCSharp_JsonManager1028810929.h"
#include "AssemblyU2DCSharp_Logo2205197457.h"
#include "AssemblyU2DCSharp_Main2809994845.h"
#include "AssemblyU2DCSharp_Main_AppState1441107354.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3762068664.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2001[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2002[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2003[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (ReflectionMethodsCache_t3343836395), -1, sizeof(ReflectionMethodsCache_t3343836395_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2004[5] = 
{
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t3343836395::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t3343836395_StaticFields::get_offset_of_s_ReflectionMethodsCache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (Raycast3DCallback_t3928470916), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (Raycast2DCallback_t2260664863), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (RaycastAllCallback_t3435657708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (GetRayIntersectionAllCallback_t2213949596), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (VertexHelper_t385374196), -1, sizeof(VertexHelper_t385374196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2009[9] = 
{
	VertexHelper_t385374196::get_offset_of_m_Positions_0(),
	VertexHelper_t385374196::get_offset_of_m_Colors_1(),
	VertexHelper_t385374196::get_offset_of_m_Uv0S_2(),
	VertexHelper_t385374196::get_offset_of_m_Uv1S_3(),
	VertexHelper_t385374196::get_offset_of_m_Normals_4(),
	VertexHelper_t385374196::get_offset_of_m_Tangents_5(),
	VertexHelper_t385374196::get_offset_of_m_Indices_6(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultTangent_7(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultNormal_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (BaseVertexEffect_t2504093552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (BaseMeshEffect_t1728560551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2011[1] = 
{
	BaseMeshEffect_t1728560551::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (Outline_t1417504278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { sizeof (PositionAsUV1_t1102546563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2016[4] = 
{
	Shadow_t4269599528::get_offset_of_m_EffectColor_3(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_4(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305142), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2017[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637717 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (U3CModuleU3E_t3783534221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (AnimationBehaviors_t2387498671), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2020[4] = 
{
	AnimationBehaviors_t2387498671::get_offset_of_ding_2(),
	AnimationBehaviors_t2387498671::get_offset_of_beepCount_3(),
	AnimationBehaviors_t2387498671::get_offset_of_fanfare_4(),
	AnimationBehaviors_t2387498671::get_offset_of_audioSource_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (CharacterBehaviors_t674834278), -1, sizeof(CharacterBehaviors_t674834278_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2021[50] = 
{
	CharacterBehaviors_t674834278_StaticFields::get_offset_of_arCamera_2(),
	CharacterBehaviors_t674834278::get_offset_of_originalTouchPosY_3(),
	CharacterBehaviors_t674834278::get_offset_of_popTargetHeight_4(),
	CharacterBehaviors_t674834278::get_offset_of_popUpMode_5(),
	CharacterBehaviors_t674834278::get_offset_of_imTouched_6(),
	CharacterBehaviors_t674834278::get_offset_of_okToPlaySounds_7(),
	CharacterBehaviors_t674834278::get_offset_of_iLost_8(),
	CharacterBehaviors_t674834278::get_offset_of_punchTimer_9(),
	CharacterBehaviors_t674834278::get_offset_of_punchStartTime_10(),
	CharacterBehaviors_t674834278::get_offset_of_maxPunchTime_11(),
	CharacterBehaviors_t674834278::get_offset_of_punchTimerHasStarted_12(),
	CharacterBehaviors_t674834278::get_offset_of_glovesShouldRetract_13(),
	CharacterBehaviors_t674834278::get_offset_of_punchingGlove_14(),
	CharacterBehaviors_t674834278::get_offset_of_punchingGloveStartPosition_15(),
	CharacterBehaviors_t674834278::get_offset_of_healthScaleFactor_16(),
	CharacterBehaviors_t674834278::get_offset_of_healthOriginalScale_17(),
	CharacterBehaviors_t674834278::get_offset_of_totalHealthPoints_18(),
	CharacterBehaviors_t674834278::get_offset_of_myFace_19(),
	CharacterBehaviors_t674834278::get_offset_of_leftShoulderAttachPoint_20(),
	CharacterBehaviors_t674834278::get_offset_of_rightShoulderAttachPoint_21(),
	CharacterBehaviors_t674834278::get_offset_of_leftGloveAttachPoint_22(),
	CharacterBehaviors_t674834278::get_offset_of_rightGloveAttachPoint_23(),
	CharacterBehaviors_t674834278::get_offset_of_myPositionInControllerList_24(),
	CharacterBehaviors_t674834278::get_offset_of_myAttributes_25(),
	CharacterBehaviors_t674834278::get_offset_of_myHealthHolder_26(),
	CharacterBehaviors_t674834278::get_offset_of_myHealthScaler_27(),
	CharacterBehaviors_t674834278::get_offset_of_targetPointColliderObj_28(),
	CharacterBehaviors_t674834278::get_offset_of_myTempTarget_29(),
	CharacterBehaviors_t674834278::get_offset_of_opponentTargetPoint_30(),
	CharacterBehaviors_t674834278::get_offset_of_myLeftGloveHolder_31(),
	CharacterBehaviors_t674834278::get_offset_of_myRightGloveHolder_32(),
	CharacterBehaviors_t674834278::get_offset_of_leftGlove_33(),
	CharacterBehaviors_t674834278::get_offset_of_rightGlove_34(),
	CharacterBehaviors_t674834278::get_offset_of_origLeftGloveLocalPos_35(),
	CharacterBehaviors_t674834278::get_offset_of_origRightGloveLocalPos_36(),
	CharacterBehaviors_t674834278::get_offset_of_startLocalPosY_37(),
	CharacterBehaviors_t674834278::get_offset_of_punchPhase_38(),
	CharacterBehaviors_t674834278::get_offset_of_okToAnimate_39(),
	CharacterBehaviors_t674834278::get_offset_of_damageGivenPerPunch_40(),
	CharacterBehaviors_t674834278::get_offset_of_attackTimeIncrement_41(),
	CharacterBehaviors_t674834278::get_offset_of_punchSpeedModifier_42(),
	CharacterBehaviors_t674834278::get_offset_of_defenseModifier_43(),
	CharacterBehaviors_t674834278::get_offset_of_chanceOfMassiveHit_44(),
	CharacterBehaviors_t674834278::get_offset_of_modifiedAttackTimeIncrement_45(),
	CharacterBehaviors_t674834278::get_offset_of_allowTouch_46(),
	CharacterBehaviors_t674834278::get_offset_of_loseAnimPlayed_47(),
	CharacterBehaviors_t674834278::get_offset_of_lastTouchPos_48(),
	CharacterBehaviors_t674834278::get_offset_of_lastTouchValid_49(),
	CharacterBehaviors_t674834278::get_offset_of_characterAnimation_50(),
	CharacterBehaviors_t674834278::get_offset_of_faceAnimation_51(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (PopupMode_t1897899772)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2022[5] = 
{
	PopupMode_t1897899772::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (FisticuffsController_t308090938), -1, sizeof(FisticuffsController_t308090938_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2023[25] = 
{
	FisticuffsController_t308090938_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_2(),
	FisticuffsController_t308090938::get_offset_of_maxNumberOfCardsInPlay_3(),
	FisticuffsController_t308090938::get_offset_of_cardsNeededForGameToStart_4(),
	FisticuffsController_t308090938::get_offset_of_bell_5(),
	FisticuffsController_t308090938::get_offset_of_megaHit_6(),
	FisticuffsController_t308090938::get_offset_of_megaHitParticles_7(),
	FisticuffsController_t308090938::get_offset_of_pop_8(),
	FisticuffsController_t308090938::get_offset_of_punchHit_9(),
	FisticuffsController_t308090938::get_offset_of_punchMiss_10(),
	FisticuffsController_t308090938::get_offset_of_tada_11(),
	FisticuffsController_t308090938::get_offset_of_crash_12(),
	FisticuffsController_t308090938::get_offset_of_victory_13(),
	FisticuffsController_t308090938::get_offset_of_hitExplosion_14(),
	FisticuffsController_t308090938::get_offset_of_cardsInPlay_15(),
	FisticuffsController_t308090938::get_offset_of_ready_16(),
	FisticuffsController_t308090938::get_offset_of_timeAllowedBeforeReset_17(),
	FisticuffsController_t308090938::get_offset_of_oneShotAudio_18(),
	FisticuffsController_t308090938::get_offset_of_crowdAudio_19(),
	FisticuffsController_t308090938::get_offset_of_megaDamageAmount_20(),
	FisticuffsController_t308090938::get_offset_of_gameHasStarted_21(),
	FisticuffsController_t308090938::get_offset_of_gameIsDone_22(),
	FisticuffsController_t308090938::get_offset_of_character1_23(),
	FisticuffsController_t308090938::get_offset_of_character2_24(),
	FisticuffsController_t308090938::get_offset_of_char1behaviors_25(),
	FisticuffsController_t308090938::get_offset_of_char2behaviors_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (U3CIntroStartU3Ec__Iterator0_t3871887700), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2024[4] = 
{
	U3CIntroStartU3Ec__Iterator0_t3871887700::get_offset_of_U24this_0(),
	U3CIntroStartU3Ec__Iterator0_t3871887700::get_offset_of_U24current_1(),
	U3CIntroStartU3Ec__Iterator0_t3871887700::get_offset_of_U24disposing_2(),
	U3CIntroStartU3Ec__Iterator0_t3871887700::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (U3CEndBellU3Ec__Iterator1_t868562403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2025[4] = 
{
	U3CEndBellU3Ec__Iterator1_t868562403::get_offset_of_U24this_0(),
	U3CEndBellU3Ec__Iterator1_t868562403::get_offset_of_U24current_1(),
	U3CEndBellU3Ec__Iterator1_t868562403::get_offset_of_U24disposing_2(),
	U3CEndBellU3Ec__Iterator1_t868562403::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (FollowCameraPrecise_t1971515421), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (GloveScript_t3172908102), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2027[5] = 
{
	GloveScript_t3172908102::get_offset_of_hitPoof_2(),
	GloveScript_t3172908102::get_offset_of_characterBehvaiors_3(),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (LookAtCameraFisticuffs_t2130711525), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2028[1] = 
{
	LookAtCameraFisticuffs_t2130711525::get_offset_of_theCamera_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (TimedSelfDeactivate_t3836150707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2029[2] = 
{
	TimedSelfDeactivate_t3836150707::get_offset_of_storedTime_2(),
	TimedSelfDeactivate_t3836150707::get_offset_of_timeBeforeDeactivate_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (TimedSelfDestruct_t4043215813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2030[2] = 
{
	TimedSelfDestruct_t4043215813::get_offset_of_storedTime_2(),
	TimedSelfDestruct_t4043215813::get_offset_of_timeBeforeDestroy_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (ARCamera_t431610428), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2031[18] = 
{
	0,
	ARCamera_t431610428::get_offset_of__origin_3(),
	ARCamera_t431610428::get_offset_of__marker_4(),
	ARCamera_t431610428::get_offset_of_arPosition_5(),
	ARCamera_t431610428::get_offset_of_arRotation_6(),
	ARCamera_t431610428::get_offset_of_arVisible_7(),
	ARCamera_t431610428::get_offset_of_timeLastUpdate_8(),
	ARCamera_t431610428::get_offset_of_timeTrackingLost_9(),
	ARCamera_t431610428::get_offset_of_eventReceiver_10(),
	ARCamera_t431610428::get_offset_of_Stereo_11(),
	ARCamera_t431610428::get_offset_of_StereoEye_12(),
	ARCamera_t431610428::get_offset_of_Optical_13(),
	ARCamera_t431610428::get_offset_of_opticalSetupOK_14(),
	ARCamera_t431610428::get_offset_of_OpticalParamsFilenameIndex_15(),
	ARCamera_t431610428::get_offset_of_OpticalParamsFilename_16(),
	ARCamera_t431610428::get_offset_of_OpticalParamsFileContents_17(),
	ARCamera_t431610428::get_offset_of_OpticalEyeLateralOffsetRight_18(),
	ARCamera_t431610428::get_offset_of_opticalViewMatrix_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (ViewEye_t928886047)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2032[3] = 
{
	ViewEye_t928886047::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (ContentMode_t2920913530)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2033[5] = 
{
	ContentMode_t2920913530::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (ContentAlign_t2517740362)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2034[10] = 
{
	ContentAlign_t2517740362::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (ARController_t593870669), -1, sizeof(ARController_t593870669_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2035[95] = 
{
	ARController_t593870669::get_offset_of_bFound_2(),
	ARController_t593870669_StaticFields::get_offset_of_U3ClogCallbackU3Ek__BackingField_3(),
	ARController_t593870669_StaticFields::get_offset_of_logMessages_4(),
	0,
	0,
	ARController_t593870669::get_offset_of_UseNativeGLTexturingIfAvailable_7(),
	ARController_t593870669::get_offset_of_AllowNonRGBVideo_8(),
	ARController_t593870669::get_offset_of_QuitOnEscOrBack_9(),
	ARController_t593870669::get_offset_of_AutoStartAR_10(),
	ARController_t593870669::get_offset_of__version_11(),
	ARController_t593870669::get_offset_of__running_12(),
	ARController_t593870669::get_offset_of__runOnUnpause_13(),
	ARController_t593870669::get_offset_of__sceneConfiguredForVideo_14(),
	ARController_t593870669::get_offset_of__sceneConfiguredForVideoWaitingMessageLogged_15(),
	ARController_t593870669::get_offset_of__useNativeGLTexturing_16(),
	ARController_t593870669::get_offset_of__useColor32_17(),
	ARController_t593870669::get_offset_of_videoCParamName0_18(),
	ARController_t593870669::get_offset_of_videoConfigurationWindows0_19(),
	ARController_t593870669::get_offset_of_videoConfigurationMacOSX0_20(),
	ARController_t593870669::get_offset_of_videoConfigurationiOS0_21(),
	ARController_t593870669::get_offset_of_videoConfigurationAndroid0_22(),
	ARController_t593870669::get_offset_of_videoConfigurationWindowsStore0_23(),
	ARController_t593870669::get_offset_of_videoConfigurationLinux0_24(),
	ARController_t593870669::get_offset_of_BackgroundLayer0_25(),
	ARController_t593870669::get_offset_of__videoWidth0_26(),
	ARController_t593870669::get_offset_of__videoHeight0_27(),
	ARController_t593870669::get_offset_of__videoPixelSize0_28(),
	ARController_t593870669::get_offset_of__videoPixelFormatString0_29(),
	ARController_t593870669::get_offset_of__videoProjectionMatrix0_30(),
	ARController_t593870669::get_offset_of__videoBackgroundMeshGO0_31(),
	ARController_t593870669::get_offset_of__videoColorArray0_32(),
	ARController_t593870669::get_offset_of__videoColor32Array0_33(),
	ARController_t593870669::get_offset_of__videoTexture0_34(),
	ARController_t593870669::get_offset_of__videoMaterial0_35(),
	ARController_t593870669::get_offset_of_VideoIsStereo_36(),
	ARController_t593870669::get_offset_of_transL2RName_37(),
	ARController_t593870669::get_offset_of_videoCParamName1_38(),
	ARController_t593870669::get_offset_of_videoConfigurationWindows1_39(),
	ARController_t593870669::get_offset_of_videoConfigurationMacOSX1_40(),
	ARController_t593870669::get_offset_of_videoConfigurationiOS1_41(),
	ARController_t593870669::get_offset_of_videoConfigurationAndroid1_42(),
	ARController_t593870669::get_offset_of_videoConfigurationWindowsStore1_43(),
	ARController_t593870669::get_offset_of_videoConfigurationLinux1_44(),
	ARController_t593870669::get_offset_of_BackgroundLayer1_45(),
	ARController_t593870669::get_offset_of__videoWidth1_46(),
	ARController_t593870669::get_offset_of__videoHeight1_47(),
	ARController_t593870669::get_offset_of__videoPixelSize1_48(),
	ARController_t593870669::get_offset_of__videoPixelFormatString1_49(),
	ARController_t593870669::get_offset_of__videoProjectionMatrix1_50(),
	ARController_t593870669::get_offset_of__videoBackgroundMeshGO1_51(),
	ARController_t593870669::get_offset_of__videoColorArray1_52(),
	ARController_t593870669::get_offset_of__videoColor32Array1_53(),
	ARController_t593870669::get_offset_of__videoTexture1_54(),
	ARController_t593870669::get_offset_of__videoMaterial1_55(),
	ARController_t593870669::get_offset_of_clearCamera_56(),
	ARController_t593870669::get_offset_of__videoBackgroundCameraGO0_57(),
	ARController_t593870669::get_offset_of__videoBackgroundCamera0_58(),
	ARController_t593870669::get_offset_of__videoBackgroundCameraGO1_59(),
	ARController_t593870669::get_offset_of__videoBackgroundCamera1_60(),
	ARController_t593870669::get_offset_of_NearPlane_61(),
	ARController_t593870669::get_offset_of_FarPlane_62(),
	ARController_t593870669::get_offset_of_ContentRotate90_63(),
	ARController_t593870669::get_offset_of_ContentFlipH_64(),
	ARController_t593870669::get_offset_of_ContentFlipV_65(),
	ARController_t593870669::get_offset_of_ContentAlign_66(),
	ARController_t593870669_StaticFields::get_offset_of_ContentModeNames_67(),
	ARController_t593870669::get_offset_of_frameCounter_68(),
	ARController_t593870669::get_offset_of_timeCounter_69(),
	ARController_t593870669::get_offset_of_lastFramerate_70(),
	ARController_t593870669::get_offset_of_refreshTime_71(),
	ARController_t593870669_StaticFields::get_offset_of_ThresholdModeDescriptions_72(),
	ARController_t593870669::get_offset_of_currentContentMode_73(),
	ARController_t593870669::get_offset_of_currentThresholdMode_74(),
	ARController_t593870669::get_offset_of_currentThreshold_75(),
	ARController_t593870669::get_offset_of_currentLabelingMode_76(),
	ARController_t593870669::get_offset_of_currentTemplateSize_77(),
	ARController_t593870669::get_offset_of_currentTemplateCountMax_78(),
	ARController_t593870669::get_offset_of_currentBorderSize_79(),
	ARController_t593870669::get_offset_of_currentPatternDetectionMode_80(),
	ARController_t593870669::get_offset_of_currentMatrixCodeType_81(),
	ARController_t593870669::get_offset_of_currentImageProcMode_82(),
	ARController_t593870669::get_offset_of_currentUseVideoBackground_83(),
	ARController_t593870669::get_offset_of_currentNFTMultiMode_84(),
	ARController_t593870669::get_offset_of_currentLogLevel_85(),
	ARController_t593870669::get_offset_of_style_86(),
	ARController_t593870669::get_offset_of_guiSetup_87(),
	ARController_t593870669::get_offset_of_showGUIErrorDialog_88(),
	ARController_t593870669::get_offset_of_showGUIErrorDialogContent_89(),
	ARController_t593870669::get_offset_of_showGUIErrorDialogWinRect_90(),
	ARController_t593870669::get_offset_of_showGUIDebug_91(),
	ARController_t593870669::get_offset_of_showGUIDebugInfo_92(),
	ARController_t593870669::get_offset_of_showGUIDebugLogConsole_93(),
	ARController_t593870669::get_offset_of_scrollPosition_94(),
	ARController_t593870669_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_95(),
	ARController_t593870669_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_96(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (ARToolKitThresholdMode_t2161580787)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2036[6] = 
{
	ARToolKitThresholdMode_t2161580787::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (ARToolKitLabelingMode_t925999490)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2037[3] = 
{
	ARToolKitLabelingMode_t925999490::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (ARToolKitPatternDetectionMode_t1396884775)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2038[6] = 
{
	ARToolKitPatternDetectionMode_t1396884775::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (ARToolKitMatrixCodeType_t3871848761)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2039[7] = 
{
	ARToolKitMatrixCodeType_t3871848761::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (ARToolKitImageProcMode_t1927279569)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2040[3] = 
{
	ARToolKitImageProcMode_t1927279569::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (ARW_UNITY_RENDER_EVENTID_t3085043805)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2041[4] = 
{
	ARW_UNITY_RENDER_EVENTID_t3085043805::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (ARW_ERROR_t1424361661)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2042[15] = 
{
	ARW_ERROR_t1424361661::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (AR_LOG_LEVEL_t1976762171)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2043[6] = 
{
	AR_LOG_LEVEL_t1976762171::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (MarkerType_t961965194)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2044[5] = 
{
	MarkerType_t961965194::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (ARWMarkerOption_t4121286531)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2045[8] = 
{
	ARWMarkerOption_t4121286531::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { sizeof (ARMarker_t1554260723), -1, sizeof(ARMarker_t1554260723_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2046[24] = 
{
	ARMarker_t1554260723_StaticFields::get_offset_of_MarkerTypeNames_2(),
	0,
	0,
	ARMarker_t1554260723::get_offset_of_UID_5(),
	ARMarker_t1554260723::get_offset_of_MarkerType_6(),
	ARMarker_t1554260723::get_offset_of_Tag_7(),
	ARMarker_t1554260723::get_offset_of_PatternFilenameIndex_8(),
	ARMarker_t1554260723::get_offset_of_PatternFilename_9(),
	ARMarker_t1554260723::get_offset_of_PatternContents_10(),
	ARMarker_t1554260723::get_offset_of_PatternWidth_11(),
	ARMarker_t1554260723::get_offset_of_BarcodeID_12(),
	ARMarker_t1554260723::get_offset_of_MultiConfigFile_13(),
	ARMarker_t1554260723::get_offset_of_NFTDataName_14(),
	ARMarker_t1554260723::get_offset_of_NFTDataExts_15(),
	ARMarker_t1554260723::get_offset_of_NFTWidth_16(),
	ARMarker_t1554260723::get_offset_of_NFTHeight_17(),
	ARMarker_t1554260723::get_offset_of_patterns_18(),
	ARMarker_t1554260723::get_offset_of_currentUseContPoseEstimation_19(),
	ARMarker_t1554260723::get_offset_of_currentFiltered_20(),
	ARMarker_t1554260723::get_offset_of_currentFilterSampleRate_21(),
	ARMarker_t1554260723::get_offset_of_currentFilterCutoffFreq_22(),
	ARMarker_t1554260723::get_offset_of_currentNFTScale_23(),
	ARMarker_t1554260723::get_offset_of_visible_24(),
	ARMarker_t1554260723::get_offset_of_transformationMatrix_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (ARNativePlugin_t494404617), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2047[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (ARNativePluginStatic_t4005840597), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (AROrigin_t3335349585), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2049[5] = 
{
	0,
	AROrigin_t3335349585::get_offset_of_findMarkerTags_3(),
	AROrigin_t3335349585::get_offset_of_baseMarker_4(),
	AROrigin_t3335349585::get_offset_of_markersEligibleForBaseMarker_5(),
	AROrigin_t3335349585::get_offset_of__findMarkerMode_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { sizeof (FindMode_t1452273230)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2050[4] = 
{
	FindMode_t1452273230::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { sizeof (ARPattern_t3906840165), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2051[6] = 
{
	ARPattern_t3906840165::get_offset_of_texture_0(),
	ARPattern_t3906840165::get_offset_of_matrix_1(),
	ARPattern_t3906840165::get_offset_of_width_2(),
	ARPattern_t3906840165::get_offset_of_height_3(),
	ARPattern_t3906840165::get_offset_of_imageSizeX_4(),
	ARPattern_t3906840165::get_offset_of_imageSizeY_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (ARTrackedCamera_t3950879424), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2052[5] = 
{
	0,
	ARTrackedCamera_t3950879424::get_offset_of_secondsToRemainVisible_21(),
	ARTrackedCamera_t3950879424::get_offset_of_cullingMask_22(),
	ARTrackedCamera_t3950879424::get_offset_of_lastArVisible_23(),
	ARTrackedCamera_t3950879424::get_offset_of__markerTag_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { sizeof (ARTrackedObject_t1684152848), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2053[9] = 
{
	0,
	ARTrackedObject_t1684152848::get_offset_of__origin_3(),
	ARTrackedObject_t1684152848::get_offset_of__marker_4(),
	ARTrackedObject_t1684152848::get_offset_of_visible_5(),
	ARTrackedObject_t1684152848::get_offset_of_timeTrackingLost_6(),
	ARTrackedObject_t1684152848::get_offset_of_secondsToRemainVisible_7(),
	ARTrackedObject_t1684152848::get_offset_of_visibleOrRemain_8(),
	ARTrackedObject_t1684152848::get_offset_of_eventReceiver_9(),
	ARTrackedObject_t1684152848::get_offset_of__markerTag_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { sizeof (ARTransitionalCamera_t3582277450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2054[10] = 
{
	ARTransitionalCamera_t3582277450::get_offset_of_vrTargetPosition_25(),
	ARTransitionalCamera_t3582277450::get_offset_of_vrTargetRotation_26(),
	ARTransitionalCamera_t3582277450::get_offset_of_targetObject_27(),
	ARTransitionalCamera_t3582277450::get_offset_of_transitionAmount_28(),
	ARTransitionalCamera_t3582277450::get_offset_of_movementRate_29(),
	ARTransitionalCamera_t3582277450::get_offset_of_vrObserverAzimuth_30(),
	ARTransitionalCamera_t3582277450::get_offset_of_vrObserverElevation_31(),
	ARTransitionalCamera_t3582277450::get_offset_of_vrObserverOffset_32(),
	ARTransitionalCamera_t3582277450::get_offset_of_automaticTransition_33(),
	ARTransitionalCamera_t3582277450::get_offset_of_automaticTransitionDistance_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { sizeof (U3CDoTransitionU3Ec__Iterator0_t340230560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2055[8] = 
{
	U3CDoTransitionU3Ec__Iterator0_t340230560::get_offset_of_U3CartoolkitU3E__0_0(),
	U3CDoTransitionU3Ec__Iterator0_t340230560::get_offset_of_flyIn_1(),
	U3CDoTransitionU3Ec__Iterator0_t340230560::get_offset_of_U3CtransitionSpeedU3E__1_2(),
	U3CDoTransitionU3Ec__Iterator0_t340230560::get_offset_of_U3CtransitioningU3E__2_3(),
	U3CDoTransitionU3Ec__Iterator0_t340230560::get_offset_of_U24this_4(),
	U3CDoTransitionU3Ec__Iterator0_t340230560::get_offset_of_U24current_5(),
	U3CDoTransitionU3Ec__Iterator0_t340230560::get_offset_of_U24disposing_6(),
	U3CDoTransitionU3Ec__Iterator0_t340230560::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { sizeof (ARUtilityFunctions_t4213713322), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { sizeof (PluginFunctions_t2887339240), -1, sizeof(PluginFunctions_t2887339240_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2057[3] = 
{
	PluginFunctions_t2887339240_StaticFields::get_offset_of_inited_0(),
	PluginFunctions_t2887339240_StaticFields::get_offset_of_logCallback_1(),
	PluginFunctions_t2887339240_StaticFields::get_offset_of_logCallbackGCH_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { sizeof (LogCallback_t2143553514), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { sizeof (Animazioni_t3270717623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2059[6] = 
{
	Animazioni_t3270717623::get_offset_of_animationWin_2(),
	Animazioni_t3270717623::get_offset_of_animationNotWin_3(),
	Animazioni_t3270717623::get_offset_of_fireworks_4(),
	Animazioni_t3270717623::get_offset_of_win_5(),
	Animazioni_t3270717623::get_offset_of_notWin_6(),
	Animazioni_t3270717623::get_offset_of_info_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { sizeof (JsonManager_t1028810929), -1, sizeof(JsonManager_t1028810929_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2060[4] = 
{
	JsonManager_t1028810929_StaticFields::get_offset_of_xml_2(),
	JsonManager_t1028810929_StaticFields::get_offset_of_filename_3(),
	JsonManager_t1028810929_StaticFields::get_offset_of_countPlay_4(),
	JsonManager_t1028810929_StaticFields::get_offset_of_numString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { sizeof (Logo_t2205197457), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2061[7] = 
{
	Logo_t2205197457::get_offset_of_speedRotate_2(),
	Logo_t2205197457::get_offset_of_startScale_3(),
	Logo_t2205197457::get_offset_of_desireScale_4(),
	Logo_t2205197457::get_offset_of_desirePos_5(),
	Logo_t2205197457::get_offset_of_lerpScale_6(),
	Logo_t2205197457::get_offset_of_lerpPos_7(),
	Logo_t2205197457::get_offset_of_t_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { sizeof (Main_t2809994845), -1, sizeof(Main_t2809994845_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2062[21] = 
{
	Main_t2809994845::get_offset_of_curState_2(),
	Main_t2809994845::get_offset_of_web_3(),
	0,
	Main_t2809994845::get_offset_of_stored_5(),
	Main_t2809994845_StaticFields::get_offset_of_reply_6(),
	Main_t2809994845::get_offset_of_codice_7(),
	Main_t2809994845::get_offset_of_totale_8(),
	Main_t2809994845::get_offset_of_panel_9(),
	Main_t2809994845::get_offset_of_panelPostPlay_10(),
	Main_t2809994845::get_offset_of_panelRecap_11(),
	Main_t2809994845::get_offset_of_txtResult_12(),
	Main_t2809994845::get_offset_of_txtPremi_13(),
	Main_t2809994845::get_offset_of_playerID_14(),
	Main_t2809994845_StaticFields::get_offset_of_bWin_15(),
	Main_t2809994845::get_offset_of_curPlay_16(),
	Main_t2809994845::get_offset_of_numPlays_17(),
	Main_t2809994845::get_offset_of_gameResults_18(),
	Main_t2809994845_StaticFields::get_offset_of_bFound_19(),
	Main_t2809994845::get_offset_of_myARController_20(),
	Main_t2809994845::get_offset_of_myARTtrackedObject_21(),
	Main_t2809994845_StaticFields::get_offset_of_U3CmainU3Ek__BackingField_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { sizeof (AppState_t1441107354)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2063[4] = 
{
	AppState_t1441107354::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305143), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2064[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D7CDC43E92004F6C21BFB0E695E5C42C85155527E_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { sizeof (U24ArrayTypeU3D24_t762068664)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D24_t762068664 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
