﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<MarkerType,System.Object>
struct Dictionary_2_t2230079400;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3550104102.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24282391918.h"
#include "AssemblyU2DCSharp_MarkerType961965194.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MarkerType,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2973548479_gshared (Enumerator_t3550104102 * __this, Dictionary_2_t2230079400 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m2973548479(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3550104102 *, Dictionary_2_t2230079400 *, const MethodInfo*))Enumerator__ctor_m2973548479_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<MarkerType,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1191993120_gshared (Enumerator_t3550104102 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1191993120(__this, method) ((  Il2CppObject * (*) (Enumerator_t3550104102 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1191993120_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MarkerType,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2975504638_gshared (Enumerator_t3550104102 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2975504638(__this, method) ((  void (*) (Enumerator_t3550104102 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2975504638_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<MarkerType,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4253026541_gshared (Enumerator_t3550104102 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4253026541(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t3550104102 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4253026541_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<MarkerType,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3652883970_gshared (Enumerator_t3550104102 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3652883970(__this, method) ((  Il2CppObject * (*) (Enumerator_t3550104102 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3652883970_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<MarkerType,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3676760432_gshared (Enumerator_t3550104102 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3676760432(__this, method) ((  Il2CppObject * (*) (Enumerator_t3550104102 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3676760432_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<MarkerType,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1917570470_gshared (Enumerator_t3550104102 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1917570470(__this, method) ((  bool (*) (Enumerator_t3550104102 *, const MethodInfo*))Enumerator_MoveNext_m1917570470_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<MarkerType,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t4282391918  Enumerator_get_Current_m3452829462_gshared (Enumerator_t3550104102 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3452829462(__this, method) ((  KeyValuePair_2_t4282391918  (*) (Enumerator_t3550104102 *, const MethodInfo*))Enumerator_get_Current_m3452829462_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<MarkerType,System.Object>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m4228999067_gshared (Enumerator_t3550104102 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m4228999067(__this, method) ((  int32_t (*) (Enumerator_t3550104102 *, const MethodInfo*))Enumerator_get_CurrentKey_m4228999067_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<MarkerType,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m1599100579_gshared (Enumerator_t3550104102 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1599100579(__this, method) ((  Il2CppObject * (*) (Enumerator_t3550104102 *, const MethodInfo*))Enumerator_get_CurrentValue_m1599100579_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MarkerType,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m428455253_gshared (Enumerator_t3550104102 * __this, const MethodInfo* method);
#define Enumerator_Reset_m428455253(__this, method) ((  void (*) (Enumerator_t3550104102 *, const MethodInfo*))Enumerator_Reset_m428455253_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MarkerType,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1352483306_gshared (Enumerator_t3550104102 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1352483306(__this, method) ((  void (*) (Enumerator_t3550104102 *, const MethodInfo*))Enumerator_VerifyState_m1352483306_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MarkerType,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m1035604846_gshared (Enumerator_t3550104102 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m1035604846(__this, method) ((  void (*) (Enumerator_t3550104102 *, const MethodInfo*))Enumerator_VerifyCurrent_m1035604846_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<MarkerType,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m325224547_gshared (Enumerator_t3550104102 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m325224547(__this, method) ((  void (*) (Enumerator_t3550104102 *, const MethodInfo*))Enumerator_Dispose_m325224547_gshared)(__this, method)
