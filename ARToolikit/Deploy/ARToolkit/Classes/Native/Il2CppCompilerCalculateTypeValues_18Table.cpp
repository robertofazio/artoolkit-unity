﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlParserInput_XmlParserInputS25800784.h"
#include "System_Xml_System_Xml_XmlProcessingInstruction431557540.h"
#include "System_Xml_System_Xml_XmlQualifiedName1944712516.h"
#include "System_Xml_System_Xml_XmlReader3675626668.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport1548133672.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport_Comma1644897369.h"
#include "System_Xml_System_Xml_XmlReaderBinarySupport_CharG1955031820.h"
#include "System_Xml_System_Xml_XmlReaderSettings1578612233.h"
#include "System_Xml_System_Xml_XmlResolver2024571559.h"
#include "System_Xml_System_Xml_XmlSignificantWhitespace1224054391.h"
#include "System_Xml_System_Xml_XmlSpace2880376877.h"
#include "System_Xml_System_Xml_XmlText4111601336.h"
#include "System_Xml_Mono_Xml2_XmlTextReader511376973.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_XmlTokenInfo254587324.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_XmlAttributeTok3353594030.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_TagName2340974457.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_DtdInputState3313602765.h"
#include "System_Xml_Mono_Xml2_XmlTextReader_DtdInputStateSt3023928423.h"
#include "System_Xml_System_Xml_XmlTextReader3514170725.h"
#include "System_Xml_System_Xml_XmlTextWriter2527250655.h"
#include "System_Xml_System_Xml_XmlTextWriter_XmlNodeInfo3709371029.h"
#include "System_Xml_System_Xml_XmlTextWriter_StringUtil2068578019.h"
#include "System_Xml_System_Xml_XmlTextWriter_XmlDeclState3530111136.h"
#include "System_Xml_System_Xml_XmlTokenizedType1619571710.h"
#include "System_Xml_System_Xml_XmlUrlResolver896669594.h"
#include "System_Xml_System_Xml_XmlWhitespace2557770518.h"
#include "System_Xml_System_Xml_XmlWriter1048088568.h"
#include "System_Xml_System_Xml_Schema_ValidationEventHandle1580700381.h"
#include "System_Xml_System_Xml_XmlNodeChangedEventHandler2964483403.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A3672778802.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A1957337327.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24A2038352954.h"
#include "System_Xml_U3CPrivateImplementationDetailsU3E_U24Ar628910058.h"
#include "UnityEngine_UI_U3CModuleU3E3783534214.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventHandle942672932.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste3466835263.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg1967201810.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3959312622.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3365010046.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg2524067914.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve1693084770.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirect1406276862.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycasterM3179336627.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviou3960014691.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AxisEventD1524870173.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AbstractEv1333959294.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD2681005625.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve2981963041.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1414739712.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInput621514313.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInputM1295781545.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp1441575871.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp2688375492.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp3572864619.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp3709210170.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_StandaloneIn70867863.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Standalone2680906638.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_TouchInput2561058385.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseRaycas2336171397.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Physics2DR3236822917.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PhysicsRayc249603239.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Color3438117476.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Color1328781136.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Color3293839588.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Float2986189219.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Float2824271922.h"
#include "UnityEngine_UI_UnityEngine_UI_AnimationTriggers3244928895.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2872111280.h"
#include "UnityEngine_UI_UnityEngine_UI_Button_ButtonClicked2455055323.h"
#include "UnityEngine_UI_UnityEngine_UI_Button_U3COnFinishSubm99135383.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdate1528800019.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdateRegistry1780385998.h"
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock2652774230.h"
#include "UnityEngine_UI_UnityEngine_UI_DefaultControls1409660779.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (XmlParserInputSource_t25800784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1800[6] = 
{
	XmlParserInputSource_t25800784::get_offset_of_BaseURI_0(),
	XmlParserInputSource_t25800784::get_offset_of_reader_1(),
	XmlParserInputSource_t25800784::get_offset_of_state_2(),
	XmlParserInputSource_t25800784::get_offset_of_isPE_3(),
	XmlParserInputSource_t25800784::get_offset_of_line_4(),
	XmlParserInputSource_t25800784::get_offset_of_column_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (XmlProcessingInstruction_t431557540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1801[2] = 
{
	XmlProcessingInstruction_t431557540::get_offset_of_target_6(),
	XmlProcessingInstruction_t431557540::get_offset_of_data_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (XmlQualifiedName_t1944712516), -1, sizeof(XmlQualifiedName_t1944712516_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1802[4] = 
{
	XmlQualifiedName_t1944712516_StaticFields::get_offset_of_Empty_0(),
	XmlQualifiedName_t1944712516::get_offset_of_name_1(),
	XmlQualifiedName_t1944712516::get_offset_of_ns_2(),
	XmlQualifiedName_t1944712516::get_offset_of_hash_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (XmlReader_t3675626668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1803[3] = 
{
	XmlReader_t3675626668::get_offset_of_readStringBuffer_0(),
	XmlReader_t3675626668::get_offset_of_binary_1(),
	XmlReader_t3675626668::get_offset_of_settings_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (XmlReaderBinarySupport_t1548133672), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1804[8] = 
{
	XmlReaderBinarySupport_t1548133672::get_offset_of_reader_0(),
	XmlReaderBinarySupport_t1548133672::get_offset_of_getter_1(),
	XmlReaderBinarySupport_t1548133672::get_offset_of_base64Cache_2(),
	XmlReaderBinarySupport_t1548133672::get_offset_of_base64CacheStartsAt_3(),
	XmlReaderBinarySupport_t1548133672::get_offset_of_state_4(),
	XmlReaderBinarySupport_t1548133672::get_offset_of_textCache_5(),
	XmlReaderBinarySupport_t1548133672::get_offset_of_hasCache_6(),
	XmlReaderBinarySupport_t1548133672::get_offset_of_dontReset_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (CommandState_t1644897369)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1805[6] = 
{
	CommandState_t1644897369::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (CharGetter_t1955031820), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (XmlReaderSettings_t1578612233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1807[16] = 
{
	XmlReaderSettings_t1578612233::get_offset_of_checkCharacters_0(),
	XmlReaderSettings_t1578612233::get_offset_of_closeInput_1(),
	XmlReaderSettings_t1578612233::get_offset_of_conformance_2(),
	XmlReaderSettings_t1578612233::get_offset_of_ignoreComments_3(),
	XmlReaderSettings_t1578612233::get_offset_of_ignoreProcessingInstructions_4(),
	XmlReaderSettings_t1578612233::get_offset_of_ignoreWhitespace_5(),
	XmlReaderSettings_t1578612233::get_offset_of_lineNumberOffset_6(),
	XmlReaderSettings_t1578612233::get_offset_of_linePositionOffset_7(),
	XmlReaderSettings_t1578612233::get_offset_of_prohibitDtd_8(),
	XmlReaderSettings_t1578612233::get_offset_of_nameTable_9(),
	XmlReaderSettings_t1578612233::get_offset_of_schemas_10(),
	XmlReaderSettings_t1578612233::get_offset_of_schemasNeedsInitialization_11(),
	XmlReaderSettings_t1578612233::get_offset_of_validationFlags_12(),
	XmlReaderSettings_t1578612233::get_offset_of_validationType_13(),
	XmlReaderSettings_t1578612233::get_offset_of_xmlResolver_14(),
	XmlReaderSettings_t1578612233::get_offset_of_ValidationEventHandler_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (XmlResolver_t2024571559), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (XmlSignificantWhitespace_t1224054391), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (XmlSpace_t2880376877)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1810[4] = 
{
	XmlSpace_t2880376877::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (XmlText_t4111601336), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (XmlTextReader_t511376973), -1, sizeof(XmlTextReader_t511376973_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1812[54] = 
{
	XmlTextReader_t511376973::get_offset_of_cursorToken_3(),
	XmlTextReader_t511376973::get_offset_of_currentToken_4(),
	XmlTextReader_t511376973::get_offset_of_currentAttributeToken_5(),
	XmlTextReader_t511376973::get_offset_of_currentAttributeValueToken_6(),
	XmlTextReader_t511376973::get_offset_of_attributeTokens_7(),
	XmlTextReader_t511376973::get_offset_of_attributeValueTokens_8(),
	XmlTextReader_t511376973::get_offset_of_currentAttribute_9(),
	XmlTextReader_t511376973::get_offset_of_currentAttributeValue_10(),
	XmlTextReader_t511376973::get_offset_of_attributeCount_11(),
	XmlTextReader_t511376973::get_offset_of_parserContext_12(),
	XmlTextReader_t511376973::get_offset_of_nameTable_13(),
	XmlTextReader_t511376973::get_offset_of_nsmgr_14(),
	XmlTextReader_t511376973::get_offset_of_readState_15(),
	XmlTextReader_t511376973::get_offset_of_disallowReset_16(),
	XmlTextReader_t511376973::get_offset_of_depth_17(),
	XmlTextReader_t511376973::get_offset_of_elementDepth_18(),
	XmlTextReader_t511376973::get_offset_of_depthUp_19(),
	XmlTextReader_t511376973::get_offset_of_popScope_20(),
	XmlTextReader_t511376973::get_offset_of_elementNames_21(),
	XmlTextReader_t511376973::get_offset_of_elementNameStackPos_22(),
	XmlTextReader_t511376973::get_offset_of_allowMultipleRoot_23(),
	XmlTextReader_t511376973::get_offset_of_isStandalone_24(),
	XmlTextReader_t511376973::get_offset_of_returnEntityReference_25(),
	XmlTextReader_t511376973::get_offset_of_entityReferenceName_26(),
	XmlTextReader_t511376973::get_offset_of_valueBuffer_27(),
	XmlTextReader_t511376973::get_offset_of_reader_28(),
	XmlTextReader_t511376973::get_offset_of_peekChars_29(),
	XmlTextReader_t511376973::get_offset_of_peekCharsIndex_30(),
	XmlTextReader_t511376973::get_offset_of_peekCharsLength_31(),
	XmlTextReader_t511376973::get_offset_of_curNodePeekIndex_32(),
	XmlTextReader_t511376973::get_offset_of_preserveCurrentTag_33(),
	XmlTextReader_t511376973::get_offset_of_line_34(),
	XmlTextReader_t511376973::get_offset_of_column_35(),
	XmlTextReader_t511376973::get_offset_of_currentLinkedNodeLineNumber_36(),
	XmlTextReader_t511376973::get_offset_of_currentLinkedNodeLinePosition_37(),
	XmlTextReader_t511376973::get_offset_of_useProceedingLineInfo_38(),
	XmlTextReader_t511376973::get_offset_of_startNodeType_39(),
	XmlTextReader_t511376973::get_offset_of_currentState_40(),
	XmlTextReader_t511376973::get_offset_of_nestLevel_41(),
	XmlTextReader_t511376973::get_offset_of_readCharsInProgress_42(),
	XmlTextReader_t511376973::get_offset_of_binaryCharGetter_43(),
	XmlTextReader_t511376973::get_offset_of_namespaces_44(),
	XmlTextReader_t511376973::get_offset_of_whitespaceHandling_45(),
	XmlTextReader_t511376973::get_offset_of_resolver_46(),
	XmlTextReader_t511376973::get_offset_of_normalization_47(),
	XmlTextReader_t511376973::get_offset_of_checkCharacters_48(),
	XmlTextReader_t511376973::get_offset_of_prohibitDtd_49(),
	XmlTextReader_t511376973::get_offset_of_closeInput_50(),
	XmlTextReader_t511376973::get_offset_of_entityHandling_51(),
	XmlTextReader_t511376973::get_offset_of_whitespacePool_52(),
	XmlTextReader_t511376973::get_offset_of_whitespaceCache_53(),
	XmlTextReader_t511376973::get_offset_of_stateStack_54(),
	XmlTextReader_t511376973_StaticFields::get_offset_of_U3CU3Ef__switchU24map51_55(),
	XmlTextReader_t511376973_StaticFields::get_offset_of_U3CU3Ef__switchU24map52_56(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (XmlTokenInfo_t254587324), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1813[13] = 
{
	XmlTokenInfo_t254587324::get_offset_of_valueCache_0(),
	XmlTokenInfo_t254587324::get_offset_of_Reader_1(),
	XmlTokenInfo_t254587324::get_offset_of_Name_2(),
	XmlTokenInfo_t254587324::get_offset_of_LocalName_3(),
	XmlTokenInfo_t254587324::get_offset_of_Prefix_4(),
	XmlTokenInfo_t254587324::get_offset_of_NamespaceURI_5(),
	XmlTokenInfo_t254587324::get_offset_of_IsEmptyElement_6(),
	XmlTokenInfo_t254587324::get_offset_of_QuoteChar_7(),
	XmlTokenInfo_t254587324::get_offset_of_LineNumber_8(),
	XmlTokenInfo_t254587324::get_offset_of_LinePosition_9(),
	XmlTokenInfo_t254587324::get_offset_of_ValueBufferStart_10(),
	XmlTokenInfo_t254587324::get_offset_of_ValueBufferEnd_11(),
	XmlTokenInfo_t254587324::get_offset_of_NodeType_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (XmlAttributeTokenInfo_t3353594030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1814[4] = 
{
	XmlAttributeTokenInfo_t3353594030::get_offset_of_ValueTokenStartIndex_13(),
	XmlAttributeTokenInfo_t3353594030::get_offset_of_ValueTokenEndIndex_14(),
	XmlAttributeTokenInfo_t3353594030::get_offset_of_valueCache_15(),
	XmlAttributeTokenInfo_t3353594030::get_offset_of_tmpBuilder_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (TagName_t2340974457)+ sizeof (Il2CppObject), sizeof(TagName_t2340974457_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1815[3] = 
{
	TagName_t2340974457::get_offset_of_Name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TagName_t2340974457::get_offset_of_LocalName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TagName_t2340974457::get_offset_of_Prefix_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (DtdInputState_t3313602765)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1816[10] = 
{
	DtdInputState_t3313602765::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (DtdInputStateStack_t3023928423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1817[1] = 
{
	DtdInputStateStack_t3023928423::get_offset_of_intern_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (XmlTextReader_t3514170725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1818[5] = 
{
	XmlTextReader_t3514170725::get_offset_of_entity_3(),
	XmlTextReader_t3514170725::get_offset_of_source_4(),
	XmlTextReader_t3514170725::get_offset_of_entityInsideAttribute_5(),
	XmlTextReader_t3514170725::get_offset_of_insideAttribute_6(),
	XmlTextReader_t3514170725::get_offset_of_entityNameStack_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (XmlTextWriter_t2527250655), -1, sizeof(XmlTextWriter_t2527250655_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1819[35] = 
{
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_unmarked_utf8encoding_0(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_escaped_text_chars_1(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_escaped_attr_chars_2(),
	XmlTextWriter_t2527250655::get_offset_of_base_stream_3(),
	XmlTextWriter_t2527250655::get_offset_of_source_4(),
	XmlTextWriter_t2527250655::get_offset_of_writer_5(),
	XmlTextWriter_t2527250655::get_offset_of_preserver_6(),
	XmlTextWriter_t2527250655::get_offset_of_preserved_name_7(),
	XmlTextWriter_t2527250655::get_offset_of_is_preserved_xmlns_8(),
	XmlTextWriter_t2527250655::get_offset_of_allow_doc_fragment_9(),
	XmlTextWriter_t2527250655::get_offset_of_close_output_stream_10(),
	XmlTextWriter_t2527250655::get_offset_of_ignore_encoding_11(),
	XmlTextWriter_t2527250655::get_offset_of_namespaces_12(),
	XmlTextWriter_t2527250655::get_offset_of_xmldecl_state_13(),
	XmlTextWriter_t2527250655::get_offset_of_check_character_validity_14(),
	XmlTextWriter_t2527250655::get_offset_of_newline_handling_15(),
	XmlTextWriter_t2527250655::get_offset_of_is_document_entity_16(),
	XmlTextWriter_t2527250655::get_offset_of_state_17(),
	XmlTextWriter_t2527250655::get_offset_of_node_state_18(),
	XmlTextWriter_t2527250655::get_offset_of_nsmanager_19(),
	XmlTextWriter_t2527250655::get_offset_of_open_count_20(),
	XmlTextWriter_t2527250655::get_offset_of_elements_21(),
	XmlTextWriter_t2527250655::get_offset_of_new_local_namespaces_22(),
	XmlTextWriter_t2527250655::get_offset_of_explicit_nsdecls_23(),
	XmlTextWriter_t2527250655::get_offset_of_namespace_handling_24(),
	XmlTextWriter_t2527250655::get_offset_of_indent_25(),
	XmlTextWriter_t2527250655::get_offset_of_indent_count_26(),
	XmlTextWriter_t2527250655::get_offset_of_indent_char_27(),
	XmlTextWriter_t2527250655::get_offset_of_indent_string_28(),
	XmlTextWriter_t2527250655::get_offset_of_newline_29(),
	XmlTextWriter_t2527250655::get_offset_of_indent_attributes_30(),
	XmlTextWriter_t2527250655::get_offset_of_quote_char_31(),
	XmlTextWriter_t2527250655::get_offset_of_v2_32(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_U3CU3Ef__switchU24map53_33(),
	XmlTextWriter_t2527250655_StaticFields::get_offset_of_U3CU3Ef__switchU24map54_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (XmlNodeInfo_t3709371029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1820[7] = 
{
	XmlNodeInfo_t3709371029::get_offset_of_Prefix_0(),
	XmlNodeInfo_t3709371029::get_offset_of_LocalName_1(),
	XmlNodeInfo_t3709371029::get_offset_of_NS_2(),
	XmlNodeInfo_t3709371029::get_offset_of_HasSimple_3(),
	XmlNodeInfo_t3709371029::get_offset_of_HasElements_4(),
	XmlNodeInfo_t3709371029::get_offset_of_XmlLang_5(),
	XmlNodeInfo_t3709371029::get_offset_of_XmlSpace_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (StringUtil_t2068578019), -1, sizeof(StringUtil_t2068578019_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1821[2] = 
{
	StringUtil_t2068578019_StaticFields::get_offset_of_cul_0(),
	StringUtil_t2068578019_StaticFields::get_offset_of_cmp_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (XmlDeclState_t3530111136)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1822[5] = 
{
	XmlDeclState_t3530111136::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (XmlTokenizedType_t1619571710)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1823[14] = 
{
	XmlTokenizedType_t1619571710::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (XmlUrlResolver_t896669594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1824[1] = 
{
	XmlUrlResolver_t896669594::get_offset_of_credential_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (XmlWhitespace_t2557770518), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (XmlWriter_t1048088568), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (ValidationEventHandler_t1580700381), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (XmlNodeChangedEventHandler_t2964483403), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305141), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1829[7] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24U24fieldU2D23_0(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24U24fieldU2D26_1(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24U24fieldU2D27_2(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24U24fieldU2D28_3(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24U24fieldU2D29_4(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24U24fieldU2D43_5(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24U24fieldU2D44_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (U24ArrayTypeU2412_t3672778807)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3672778807 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (U24ArrayTypeU248_t1957337328)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU248_t1957337328 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (U24ArrayTypeU24256_t2038352957)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24256_t2038352957 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (U24ArrayTypeU241280_t628910058)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU241280_t628910058 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (U3CModuleU3E_t3783534220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (EventHandle_t942672932)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1835[3] = 
{
	EventHandle_t942672932::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (EventSystem_t3466835263), -1, sizeof(EventSystem_t3466835263_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1854[12] = 
{
	EventSystem_t3466835263::get_offset_of_m_SystemInputModules_2(),
	EventSystem_t3466835263::get_offset_of_m_CurrentInputModule_3(),
	EventSystem_t3466835263_StaticFields::get_offset_of_U3CcurrentU3Ek__BackingField_4(),
	EventSystem_t3466835263::get_offset_of_m_FirstSelected_5(),
	EventSystem_t3466835263::get_offset_of_m_sendNavigationEvents_6(),
	EventSystem_t3466835263::get_offset_of_m_DragThreshold_7(),
	EventSystem_t3466835263::get_offset_of_m_CurrentSelected_8(),
	EventSystem_t3466835263::get_offset_of_m_Paused_9(),
	EventSystem_t3466835263::get_offset_of_m_SelectionGuard_10(),
	EventSystem_t3466835263::get_offset_of_m_DummyData_11(),
	EventSystem_t3466835263_StaticFields::get_offset_of_s_RaycastComparer_12(),
	EventSystem_t3466835263_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (EventTrigger_t1967201810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1855[2] = 
{
	EventTrigger_t1967201810::get_offset_of_m_Delegates_2(),
	EventTrigger_t1967201810::get_offset_of_delegates_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (TriggerEvent_t3959312622), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (Entry_t3365010046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1857[2] = 
{
	Entry_t3365010046::get_offset_of_eventID_0(),
	Entry_t3365010046::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (EventTriggerType_t2524067914)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1858[18] = 
{
	EventTriggerType_t2524067914::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (ExecuteEvents_t1693084770), -1, sizeof(ExecuteEvents_t1693084770_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1859[36] = 
{
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerEnterHandler_0(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerExitHandler_1(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerDownHandler_2(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerUpHandler_3(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerClickHandler_4(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_InitializePotentialDragHandler_5(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_BeginDragHandler_6(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_DragHandler_7(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_EndDragHandler_8(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_DropHandler_9(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_ScrollHandler_10(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_UpdateSelectedHandler_11(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_SelectHandler_12(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_DeselectHandler_13(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_MoveHandler_14(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_SubmitHandler_15(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_CancelHandler_16(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_HandlerListPool_17(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_InternalTransformList_18(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_19(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_20(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_21(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_22(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_23(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_24(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_25(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_26(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_27(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_28(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_29(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_30(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheC_31(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheD_32(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheE_33(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheF_34(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache10_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (MoveDirection_t1406276862)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1861[6] = 
{
	MoveDirection_t1406276862::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (RaycasterManager_t3179336627), -1, sizeof(RaycasterManager_t3179336627_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1862[1] = 
{
	RaycasterManager_t3179336627_StaticFields::get_offset_of_s_Raycasters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (RaycastResult_t21186376)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1863[10] = 
{
	RaycastResult_t21186376::get_offset_of_m_GameObject_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_module_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_distance_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_index_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_depth_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_sortingLayer_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_sortingOrder_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_worldPosition_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_worldNormal_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_screenPosition_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (UIBehaviour_t3960014691), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (AxisEventData_t1524870173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1865[2] = 
{
	AxisEventData_t1524870173::get_offset_of_U3CmoveVectorU3Ek__BackingField_2(),
	AxisEventData_t1524870173::get_offset_of_U3CmoveDirU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (AbstractEventData_t1333959294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1866[1] = 
{
	AbstractEventData_t1333959294::get_offset_of_m_Used_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (BaseEventData_t2681005625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1867[1] = 
{
	BaseEventData_t2681005625::get_offset_of_m_EventSystem_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (PointerEventData_t1599784723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1868[21] = 
{
	PointerEventData_t1599784723::get_offset_of_U3CpointerEnterU3Ek__BackingField_2(),
	PointerEventData_t1599784723::get_offset_of_m_PointerPress_3(),
	PointerEventData_t1599784723::get_offset_of_U3ClastPressU3Ek__BackingField_4(),
	PointerEventData_t1599784723::get_offset_of_U3CrawPointerPressU3Ek__BackingField_5(),
	PointerEventData_t1599784723::get_offset_of_U3CpointerDragU3Ek__BackingField_6(),
	PointerEventData_t1599784723::get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_7(),
	PointerEventData_t1599784723::get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_8(),
	PointerEventData_t1599784723::get_offset_of_hovered_9(),
	PointerEventData_t1599784723::get_offset_of_U3CeligibleForClickU3Ek__BackingField_10(),
	PointerEventData_t1599784723::get_offset_of_U3CpointerIdU3Ek__BackingField_11(),
	PointerEventData_t1599784723::get_offset_of_U3CpositionU3Ek__BackingField_12(),
	PointerEventData_t1599784723::get_offset_of_U3CdeltaU3Ek__BackingField_13(),
	PointerEventData_t1599784723::get_offset_of_U3CpressPositionU3Ek__BackingField_14(),
	PointerEventData_t1599784723::get_offset_of_U3CworldPositionU3Ek__BackingField_15(),
	PointerEventData_t1599784723::get_offset_of_U3CworldNormalU3Ek__BackingField_16(),
	PointerEventData_t1599784723::get_offset_of_U3CclickTimeU3Ek__BackingField_17(),
	PointerEventData_t1599784723::get_offset_of_U3CclickCountU3Ek__BackingField_18(),
	PointerEventData_t1599784723::get_offset_of_U3CscrollDeltaU3Ek__BackingField_19(),
	PointerEventData_t1599784723::get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20(),
	PointerEventData_t1599784723::get_offset_of_U3CdraggingU3Ek__BackingField_21(),
	PointerEventData_t1599784723::get_offset_of_U3CbuttonU3Ek__BackingField_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (InputButton_t2981963041)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1869[4] = 
{
	InputButton_t2981963041::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (FramePressState_t1414739712)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1870[5] = 
{
	FramePressState_t1414739712::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (BaseInput_t621514313), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (BaseInputModule_t1295781545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1872[6] = 
{
	BaseInputModule_t1295781545::get_offset_of_m_RaycastResultCache_2(),
	BaseInputModule_t1295781545::get_offset_of_m_AxisEventData_3(),
	BaseInputModule_t1295781545::get_offset_of_m_EventSystem_4(),
	BaseInputModule_t1295781545::get_offset_of_m_BaseEventData_5(),
	BaseInputModule_t1295781545::get_offset_of_m_InputOverride_6(),
	BaseInputModule_t1295781545::get_offset_of_m_DefaultInput_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (PointerInputModule_t1441575871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1873[6] = 
{
	0,
	0,
	0,
	0,
	PointerInputModule_t1441575871::get_offset_of_m_PointerData_12(),
	PointerInputModule_t1441575871::get_offset_of_m_MouseState_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (ButtonState_t2688375492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1874[2] = 
{
	ButtonState_t2688375492::get_offset_of_m_Button_0(),
	ButtonState_t2688375492::get_offset_of_m_EventData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (MouseState_t3572864619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1875[1] = 
{
	MouseState_t3572864619::get_offset_of_m_TrackedButtons_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (MouseButtonEventData_t3709210170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1876[2] = 
{
	MouseButtonEventData_t3709210170::get_offset_of_buttonState_0(),
	MouseButtonEventData_t3709210170::get_offset_of_buttonData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (StandaloneInputModule_t70867863), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1877[13] = 
{
	StandaloneInputModule_t70867863::get_offset_of_m_PrevActionTime_14(),
	StandaloneInputModule_t70867863::get_offset_of_m_LastMoveVector_15(),
	StandaloneInputModule_t70867863::get_offset_of_m_ConsecutiveMoveCount_16(),
	StandaloneInputModule_t70867863::get_offset_of_m_LastMousePosition_17(),
	StandaloneInputModule_t70867863::get_offset_of_m_MousePosition_18(),
	StandaloneInputModule_t70867863::get_offset_of_m_CurrentFocusedGameObject_19(),
	StandaloneInputModule_t70867863::get_offset_of_m_HorizontalAxis_20(),
	StandaloneInputModule_t70867863::get_offset_of_m_VerticalAxis_21(),
	StandaloneInputModule_t70867863::get_offset_of_m_SubmitButton_22(),
	StandaloneInputModule_t70867863::get_offset_of_m_CancelButton_23(),
	StandaloneInputModule_t70867863::get_offset_of_m_InputActionsPerSecond_24(),
	StandaloneInputModule_t70867863::get_offset_of_m_RepeatDelay_25(),
	StandaloneInputModule_t70867863::get_offset_of_m_ForceModuleActive_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (InputMode_t2680906638)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1878[3] = 
{
	InputMode_t2680906638::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (TouchInputModule_t2561058385), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1879[3] = 
{
	TouchInputModule_t2561058385::get_offset_of_m_LastMousePosition_14(),
	TouchInputModule_t2561058385::get_offset_of_m_MousePosition_15(),
	TouchInputModule_t2561058385::get_offset_of_m_ForceModuleActive_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (BaseRaycaster_t2336171397), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (Physics2DRaycaster_t3236822917), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (PhysicsRaycaster_t249603239), -1, sizeof(PhysicsRaycaster_t249603239_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1882[4] = 
{
	0,
	PhysicsRaycaster_t249603239::get_offset_of_m_EventCamera_3(),
	PhysicsRaycaster_t249603239::get_offset_of_m_EventMask_4(),
	PhysicsRaycaster_t249603239_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (ColorTween_t3438117476)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1884[6] = 
{
	ColorTween_t3438117476::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t3438117476::get_offset_of_m_StartColor_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t3438117476::get_offset_of_m_TargetColor_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t3438117476::get_offset_of_m_TweenMode_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t3438117476::get_offset_of_m_Duration_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t3438117476::get_offset_of_m_IgnoreTimeScale_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (ColorTweenMode_t1328781136)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1885[4] = 
{
	ColorTweenMode_t1328781136::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (ColorTweenCallback_t3293839588), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (FloatTween_t2986189219)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1887[5] = 
{
	FloatTween_t2986189219::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t2986189219::get_offset_of_m_StartValue_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t2986189219::get_offset_of_m_TargetValue_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t2986189219::get_offset_of_m_Duration_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t2986189219::get_offset_of_m_IgnoreTimeScale_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (FloatTweenCallback_t2824271922), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1889[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1890[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (AnimationTriggers_t3244928895), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1891[8] = 
{
	0,
	0,
	0,
	0,
	AnimationTriggers_t3244928895::get_offset_of_m_NormalTrigger_4(),
	AnimationTriggers_t3244928895::get_offset_of_m_HighlightedTrigger_5(),
	AnimationTriggers_t3244928895::get_offset_of_m_PressedTrigger_6(),
	AnimationTriggers_t3244928895::get_offset_of_m_DisabledTrigger_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (Button_t2872111280), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1892[1] = 
{
	Button_t2872111280::get_offset_of_m_OnClick_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (ButtonClickedEvent_t2455055323), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (U3COnFinishSubmitU3Ec__Iterator0_t99135383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1894[6] = 
{
	U3COnFinishSubmitU3Ec__Iterator0_t99135383::get_offset_of_U3CfadeTimeU3E__0_0(),
	U3COnFinishSubmitU3Ec__Iterator0_t99135383::get_offset_of_U3CelapsedTimeU3E__1_1(),
	U3COnFinishSubmitU3Ec__Iterator0_t99135383::get_offset_of_U24this_2(),
	U3COnFinishSubmitU3Ec__Iterator0_t99135383::get_offset_of_U24current_3(),
	U3COnFinishSubmitU3Ec__Iterator0_t99135383::get_offset_of_U24disposing_4(),
	U3COnFinishSubmitU3Ec__Iterator0_t99135383::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (CanvasUpdate_t1528800019)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1895[7] = 
{
	CanvasUpdate_t1528800019::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (CanvasUpdateRegistry_t1780385998), -1, sizeof(CanvasUpdateRegistry_t1780385998_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1897[7] = 
{
	CanvasUpdateRegistry_t1780385998_StaticFields::get_offset_of_s_Instance_0(),
	CanvasUpdateRegistry_t1780385998::get_offset_of_m_PerformingLayoutUpdate_1(),
	CanvasUpdateRegistry_t1780385998::get_offset_of_m_PerformingGraphicUpdate_2(),
	CanvasUpdateRegistry_t1780385998::get_offset_of_m_LayoutRebuildQueue_3(),
	CanvasUpdateRegistry_t1780385998::get_offset_of_m_GraphicRebuildQueue_4(),
	CanvasUpdateRegistry_t1780385998_StaticFields::get_offset_of_s_SortLayoutFunction_5(),
	CanvasUpdateRegistry_t1780385998_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (ColorBlock_t2652774230)+ sizeof (Il2CppObject), sizeof(ColorBlock_t2652774230 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1898[6] = 
{
	ColorBlock_t2652774230::get_offset_of_m_NormalColor_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t2652774230::get_offset_of_m_HighlightedColor_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t2652774230::get_offset_of_m_PressedColor_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t2652774230::get_offset_of_m_DisabledColor_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t2652774230::get_offset_of_m_ColorMultiplier_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t2652774230::get_offset_of_m_FadeDuration_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (DefaultControls_t1409660779), -1, sizeof(DefaultControls_t1409660779_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1899[9] = 
{
	0,
	0,
	0,
	DefaultControls_t1409660779_StaticFields::get_offset_of_s_ThickElementSize_3(),
	DefaultControls_t1409660779_StaticFields::get_offset_of_s_ThinElementSize_4(),
	DefaultControls_t1409660779_StaticFields::get_offset_of_s_ImageElementSize_5(),
	DefaultControls_t1409660779_StaticFields::get_offset_of_s_DefaultSelectableColor_6(),
	DefaultControls_t1409660779_StaticFields::get_offset_of_s_PanelColor_7(),
	DefaultControls_t1409660779_StaticFields::get_offset_of_s_TextColor_8(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
