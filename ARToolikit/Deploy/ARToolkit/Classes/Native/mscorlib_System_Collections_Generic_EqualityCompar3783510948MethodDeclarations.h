﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<ARController/ARToolKitThresholdMode>
struct DefaultComparer_t3783510948;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ARController_ARToolKitThresholdM2161580787.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<ARController/ARToolKitThresholdMode>::.ctor()
extern "C"  void DefaultComparer__ctor_m4212143785_gshared (DefaultComparer_t3783510948 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m4212143785(__this, method) ((  void (*) (DefaultComparer_t3783510948 *, const MethodInfo*))DefaultComparer__ctor_m4212143785_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<ARController/ARToolKitThresholdMode>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m187613412_gshared (DefaultComparer_t3783510948 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m187613412(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3783510948 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m187613412_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<ARController/ARToolKitThresholdMode>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m32591036_gshared (DefaultComparer_t3783510948 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m32591036(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3783510948 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m32591036_gshared)(__this, ___x0, ___y1, method)
